import gc
import network
gc.collect()
import machine
gc.collect()
import ubinascii
gc.collect()
import ujson
gc.collect()
import uos
gc.collect()
import utime
gc.collect()
import socket
gc.collect()
import uselect
gc.collect()
import urequests
gc.collect()
from uhashlib import sha256
gc.collect()
import ustruct
gc.collect()

html="""<!DOCTYPE html>
<html>
	<head> <title>Ouroboros IoT Login</title> </head>
	<body>
		<form action="http://192.168.0.1/configure.html" method="post">
			Username : <input type="text"  name="username"><br>
			Password: <input type="password" name="password" ><br>
		    	<input type="submit" value="submit" name="submit">
		</form>
	</body>
</html>
"""

login_fail_html="""<!DOCTYPE html>
<html>
	<head> <title>Ouroboros IoT Login</title> </head>
	<body>
		<h2>Incorrect Credentials!</h2><br>Please login<br>
		<form action="configure.html" method="post">
			Username : <input type="text"  name="username"><br>
			Password: <input type="password" name="password" ><br>
		    	<input type="submit" value="submit" name="submit">
		</form>
	</body>
</html>
"""
l_k_s=None
i_d_c=False
d_s_m={'device_0':{'curr_status':0, 'on_time':None, 'off_time': None}, 'device_2':{'curr_status':0, 'on_time':None, 'off_time': None}}
def fn_u_d_s(device_num, device_status, on_time=None, off_time=None):
	d_n_k='device_'+str(device_num)
	if d_n_k in d_s_m:
		d_s_o=d_s_m[d_n_k]
		d_s_o['curr_status']=device_status
		d_s_o['on_time']=on_time
		d_s_o['off_time']=off_time
		if device_status==1:
			turn_gpio_on(device_num)
		elif device_status==0:
			turn_gpio_off(device_num)

def perform_cron_task(sock):
	curr_time=utime.time()
	for d_n_k in d_s_m:
		device_num=int(d_n_k[len('device_')])
		d_s_o=d_s_m[d_n_k]
		on_time=d_s_o['on_time']
		off_time=d_s_o['off_time']
		status=d_s_o['curr_status']
		if (on_time is not None) and (status==0) and (curr_time >= on_time):
			fn_u_d_s(device_num, 1, on_time=None, off_time=None)
		elif (off_time is not None) and (status==1) and (curr_time >= off_time):
			fn_u_d_s(device_num, 0, on_time=None, off_time=None)
	if (l_k_s is not None) and ((curr_time - l_k_s) > 60):
		fn_s_s_p(sock)
def fn_s_s_p(sock):
	version=1
	pkt_type=10
	num_dev=len(d_s_m)
	dataToSend=ustruct.pack('>BHB', version, pkt_type, num_dev)
	for d_n_k in d_s_m:
		device_num=int(d_n_k[len('device_')])
		d_s_o=d_s_m[d_n_k]
		device_status=d_s_o['curr_status']
		extra_info=0
		dataToSend=dataToSend+ustruct.pack('>BBL', device_num, device_status, extra_info)
	sock.send(dataToSend)
	global l_k_s
	l_k_s=utime.time()
def update_rtc(epoch):
	cal_datetime=utime.localtime(epoch)
	machine.RTC().datetime(cal_datetime)

def http_get(url):
	 return urequests.get(url)
def fileExists(fileName):
	try:
		uos.stat(fileName)
		return True
	except OSError:
		return False
def turn_wifi_on():
	ap_if=network.WLAN(network.AP_IF)
	ap_if.active(False)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	ap_if.active(True)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	mac=ubinascii.hexlify(ap_if.config('mac'),':').decode()
	mac=mac.replace(':','')
	ap_if.config(essid="OUB1_"+mac, password="12345678")
	ap_if.ifconfig(('192.168.0.1', '255.255.255.0', '192.168.0.1', '192.168.0.1'))

def turn_wifi_off():
	ap_if=network.WLAN(network.AP_IF)
	ap_if.active(False)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
	utime.sleep_ms(500)
def get_iot_secret():
	fileName='alpha.txt'
	if fileExists(fileName):
		f=open(fileName)
		content_str=f.read()
		f.close()
		return content_str
	else:
		return 'asdasrefwefefergf9rerf3n4r23irn1n32f'

def get_wifi_config():
	fileName='wifi.conf'
	if fileExists(fileName):
		f=open(fileName)
		content_str=f.read()
		f.close()
		content=ujson.loads(content_str)
		return content
	else:
		return None
def save_wifi_config(essid, passphrase):
	f=open('wifi.conf', 'w')
	config={'essid':essid, 'passphrase':passphrase}
	config_str=ujson.dumps(config)
	f.write(config_str)
	f.close()
def get_login_config():
	fileName='login.conf'
	if fileExists(fileName):
		f=open(fileName)
		content_str=f.read()
		f.close()
		content=ujson.loads(content_str)
		return content
	else:
		return {'user':'admin','password':'admin'}

def save_login_config(user, password):
	f=open('login.conf', 'w')
	config={'user':user, 'password':password}
	config_str=ujson.dumps(config)
	f.write(config_str)
	f.close()

def turn_gpio_on(device_num):
	if device_num==0:
		pin_num=0
	elif device_num==1:
		pin_num=2
	pin=machine.Pin(pin_num) 
	if pin.value()==0:
		pin.on()
def turn_gpio_off(device_num):
	if device_num==0:
		pin_num=0
	elif device_num==1:
		pin_num=2
	pin=machine.Pin(pin_num) 
	if pin.value()==1:
		pin.off()
def init_pin(device_num):
	if device_num==0:
		pin_num=0
	elif device_num==1:
		pin_num=2
	pin=machine.Pin(pin_num, machine.Pin.OUT) 
	turn_gpio_off(device_num)
def fn_s_ka(sock, epoch, md5hash):
	cl_if=network.WLAN(network.STA_IF)
	mac=ubinascii.hexlify(cl_if.config('mac'),':').decode()
	mac=mac.replace(':','')
	version=1
	pkt_type=1
	dataToSend=ustruct.pack('>BH', version, pkt_type)+ubinascii.unhexlify(mac)+ustruct.pack('>L', epoch)+md5hash  
	sock.send(dataToSend)
def process_keepalive_ack(sock, payload):
	status_pkt=payload[0:1]
	status=ustruct.unpack('>B', status_pkt)[0]
	if status==1:
		global i_d_c
		i_d_c=True
		fn_s_s_p(sock)
def fn_p_c_p(payload):
	if len(payload) >= 4:
		cmd_subtype_pkt=payload[0:2]
		c_s_t=ustruct.unpack('>H', cmd_subtype_pkt)[0]
		if c_s_t==1:
			d_i_pkt=payload[2:4]
			d_i=ustruct.unpack('>H', d_i_pkt)[0]
			fn_u_d_s(d_i, 1, on_time=None, off_time=None)
		elif c_s_t==2:
			d_i_pkt=payload[2:4]
			d_i=ustruct.unpack('>H', d_i_pkt)[0]
			fn_u_d_s(d_i, 0, on_time=None, off_time=None)
		elif (c_s_t==3) and len(payload) >= 6:
			d_i_pkt=payload[2:4]
			d_i=ustruct.unpack('>H', d_i_pkt)[0]
			sec_time_on_pkt=payload[4:6]
			sec_time_on=ustruct.unpack('>H', sec_time_on_pkt)[0]
			off_time=utime.time()+sec_time_on
			fn_u_d_s(d_i, 1, on_time=None, off_time=off_time)
def process_server_packet(data,sock):
	if len(data)>6:
		pkt_type_pkt=data[0:2]
		e_pk=data[2:6]
		pkt_type=ustruct.unpack('>H',pkt_type_pkt)[0]
		epoch=ustruct.unpack('>L',e_pk)[0]
		if(pkt_type==2)or(pkt_type==3):
			update_rtc(epoch)
			if pkt_type==2:
				process_keepalive_ack(sock,data[6:])
			elif pkt_type==3:
				fn_p_c_p(data[6:])
				fn_s_s_p(sock)
def fn_r_t(the_socket,timeout=2):
	the_socket.setblocking(0)
	t_d='';
	begin=utime.time()
	while 1:
		now=utime.time()
		if (len(t_d)>0) and (now-begin>timeout):
			break
		elif (now-begin)>(timeout*2):
			break
		try:
			data=the_socket.recv(100)
			data=data.decode()
			if data:
				t_d=t_d+data
				begin=utime.time()
			else:
				utime.sleep_ms(200)
		except Exception as e:
			utime.sleep_ms(200)
			pass
	return t_d

def get_p_par(req):
	p_par=req.split('\r\n')[-1:][0]
	par=p_par.split('&')
	post_dict={}
	for param in par:
		key_val=param.split('=')
		key=key_val[0]
		val=key_val[1]
		post_dict[key]=val
	return post_dict
def connect_server():
	r=http_get('http://manukahoneyindia.com/v13/gettime')
	to_ret={'code': r.status_code, 'reply': r.text}
	output=ujson.loads(to_ret['reply'])
	if output['error']==False:
		epoch=output['data']
		try:
			sockaddr=socket.getaddrinfo('manukahoneyindia.com', 80)[0][-1][0]
			sock=socket.socket()
			sock.connect((sockaddr, 8000))
			sock.setblocking(0)
			secret=get_iot_secret()
			hash_bytes=sha256(secret+str(epoch)).digest()
			fn_s_ka(sock, epoch, hash_bytes)
			poller=uselect.poll()
			poller.register(sock, uselect.POLLERR | uselect.POLLIN | uselect.POLLHUP)
			loop_break=0
			while True and (loop_break==0):
				events=poller.poll(500)
				if events:
					for fd, flag in events:
						print(flag)
						if flag & uselect.POLLIN:
							data=sock.recv(250)
							if len(data)==0:
								poller.unregister(sock)
								loop_break=1
								break
							else:	
								process_server_packet(data, sock)
						if (flag & uselect.POLLHUP) or (flag & uselect.POLLERR):
							poller.unregister(sock)
							loop_break=1
							break
				perform_cron_task(sock)
				utime.sleep_ms(500)
				utime.sleep_ms(500)
				utime.sleep_ms(500)

		except OSError:
			if 'poller' in locals():
				poller.unregister(sock)
		finally:
			sock.close()
			global i_d_c
			i_d_c=False

def do_connect():
	wifi_config=get_wifi_config()
	if wifi_config is not None:
		home_wifi_ssid=wifi_config['essid']
		home_wifi_pwd=wifi_config['passphrase']
		sta_if=network.WLAN(network.STA_IF)
		sta_if.active(False)
		utime.sleep_ms(500)
		utime.sleep_ms(500)
		utime.sleep_ms(500)
		sta_if.active(True)
		sta_if.connect(home_wifi_ssid, home_wifi_pwd)
		while True:
			while not sta_if.isconnected():
				machine.idle()
			try:
				connect_server()
			except OSError:
				print("Got OS Error!")
			utime.sleep_ms(500)
	else:
		start_web_server()

def web_server(max_run_sec=None):
	turn_wifi_on()
	addr=socket.getaddrinfo('0.0.0.0', 80)[0][-1]
	s=socket.socket()
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind(addr)
	s.listen(1)
	wdt=machine.WDT()
	poller=uselect.poll()
	poller.register(s, uselect.POLLIN)
	startTimeEpoch=utime.time()
	while True:
		if max_run_sec is not None:
			elapsedTime=utime.time() - startTimeEpoch
			if elapsedTime >  max_run_sec:
				break
		all_events=poller.poll(400)
		read_avl=0
		if len(all_events) > 0:
			try:
				gc.collect()
				res=s.accept()
				client_s=res[0]
				client_addr=res[1]
				req=''
				req=fn_r_t(client_s)
		  		req=str(req)
				startTimeEpoch=utime.time()
				if req.find('conf_wifi.html') != -1:
					login_config=get_login_config()
					username=login_config['user']
					pwd=login_config['password']
					post_dict=get_p_par(req)
					u_pt=post_dict['username']
					p_pt=post_dict['password']
					if (u_pt==username) and (p_pt==pwd):
						new_wifi_ssid=post_dict['essid']
						new_wifi_passphrase=post_dict['passphrase']
						save_wifi_config(new_wifi_ssid, new_wifi_passphrase)
						client_s.send('<!DOCTYPE html><html><head> <title>Ouroboros IoT WiFi Configuration Success</title> </head><body>Configuration successful!<br>Device would go into reboot now!</body></html>')
						client_s.close()
						utime.sleep(4)
						machine.reset()
					else:
						client_s.send(login_fail_html)
				elif req.find('configure.html') != -1:
					login_config=get_login_config()
					username=login_config['user']
					pwd=login_config['password']
					post_dict=get_p_par(req)
					u_pt=post_dict['username']
					p_pt=post_dict['password']
					if (u_pt==username) and (p_pt==pwd):
						hidden_input='<input type="hidden" name="username" value="'+username+'"><input type="hidden" name="password" value="'+pwd+'">'
						configure_html='<!DOCTYPE html><html><head> <title>Ouroboros IoT WiFi Configuration Page</title> </head><body><form action="http://192.168.0.1/conf_wifi.html" method="post">WiFi SSID : <input type="text" name="essid"><br>WiFi Password: <input type="password" name="passphrase" ><br>'+hidden_input+'<input type="submit" value="submit" name="submit"></form></body></html>'
						client_s.send(configure_html)	
					else:
						client_s.send(login_fail_html)
				elif req.find('index.html') != -1:
					client_s.send(html)
				else :
					client_s.send(html)
	
			except OSError:
				print("Timed-out, no request received!\r\n")
			except Exception as e:
				print(str(e))
			finally:
				client_s.close()
				machine.idle()
				
		utime.sleep_ms(50)
		machine.idle()
	poller.unregister(s)
	s.close()
	turn_wifi_off()
def start_web_server(max_run_sec=None):
	web_server(max_run_sec)
init_pin(0)
init_pin(1)
try:
	if get_wifi_config() is None:
		start_web_server()
	else:
		start_web_server(30)
		do_connect()
finally:
	init_pin(0)
	init_pin(1)
	#machine.reset()

