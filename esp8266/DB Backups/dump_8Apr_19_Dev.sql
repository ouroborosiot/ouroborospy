-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: jf
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `jf`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `jf` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `jf`;

--
-- Table structure for table `IOT_DeviceInfo`
--

DROP TABLE IF EXISTS `IOT_DeviceInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOT_DeviceInfo` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AccessKey` varchar(512) NOT NULL,
  `DeviceIdentifier` varchar(190) NOT NULL,
  `DeviceTitle` varchar(512) NOT NULL,
  `NumOutputs` int(10) unsigned NOT NULL,
  `LastKnownStatus` varchar(512) DEFAULT NULL,
  `LastKnownStatusRecvTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `UserIDRegistered` int(10) unsigned DEFAULT NULL,
  `DeviceType` enum('SWITCH','WATER_TIMER') DEFAULT 'WATER_TIMER',
  `DeviceModel` varchar(512) DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `SSID` varchar(512) DEFAULT NULL,
  `DeviceSchedule` mediumtext DEFAULT NULL,
  `DeviceScheduleSynced` enum('YES','NO') NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UQ_IOT_DI_DevIdent` (`DeviceIdentifier`),
  KEY `FK_IOT_DI_UID_UL_UID` (`UserIDRegistered`),
  CONSTRAINT `FK_IOT_DI_UID_UL_UID` FOREIGN KEY (`UserIDRegistered`) REFERENCES `IOT_UserList` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IOT_DeviceInfo`
--

LOCK TABLES `IOT_DeviceInfo` WRITE;
/*!40000 ALTER TABLE `IOT_DeviceInfo` DISABLE KEYS */;
INSERT INTO `IOT_DeviceInfo` VALUES (1,'asdasrefwefefergf9rerf3n4r23irn1n32f','6001947F404F','Water Timer',1,'OFF','2018-05-22 12:48:14',NULL,'WATER_TIMER','WT-01','2018-05-05 10:19:57','2018-05-24 21:20:52',NULL,NULL,'NO'),(2,'gQ01c5BdS1LM0dqYRcAOLPwJmt5uIDeZuf0GkCUdDFZDSqCKCdzn256pzcTiQ7hk','6001947F8DED','Water Timer',1,'{\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-23 15:16:24',1,'WATER_TIMER','WT-01','2018-05-22 07:08:03','2019-03-23 09:46:24','NETGEAR-zestl','{\"5\":[{\"start\":\"0:45:00\",\"when\":\"Monday\",\"duration\":7200},{\"start\":\"22:12:00\",\"when\":\"Tuesday\",\"duration\":18000}]}','NO'),(3,'o0mRY89TvaPScTlC0beCJQDtAuJ2GfY5gkWft59YgYRsSd5Spju8a8CLClNjBLoR','6001947F3DA8','Water Timer',1,'{\"0\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2018-12-29 04:45:05',1,'WATER_TIMER','WT-01','2018-05-25 19:46:26','2018-12-29 04:45:05','TP-LINK_A870',NULL,'NO'),(4,'qzmU2PBzyDP1AZpIAoEX8qEjisecswWS6jN88pIH2yICx8k7xZ5GqJZIceVELSxR','6001947F409E','Water Timer',1,'{\"4\":\"OFF\",\"5\":\"OFF\"}','2018-12-28 04:18:04',1,'WATER_TIMER','WT-01','2018-05-26 20:40:20','2019-02-27 18:52:53','NETGEAR-zestl','{\"5\":[{\"start\":\"0:20:00\",\"when\":\"Monday\",\"duration\":4800},{\"start\":\"12:19:00\",\"when\":\"Friday\",\"duration\":7200},{\"start\":\"18:22:00\",\"when\":\"Sunday\",\"duration\":18000},{\"start\":\"16:28:00\",\"when\":\"Saturday\",\"duration\":6600}]}','NO'),(5,'MscZn3bA3zSvXc18DkBjglsjfQgWzPVlh7lFaxge78K5lMeY6PinaKGqAXnaMiw4','6001947FAE43','Switch',1,'OFF','2018-06-21 18:34:48',1,'SWITCH','SW-MINI-1W2N','2018-06-19 20:16:06','2019-02-26 03:42:37',NULL,'{\"5\":[{\"start\":\"9:10:00\",\"when\":\"Monday\",\"duration\":3600},{\"start\":\"11:10:00\",\"when\":\"Tuesday\",\"duration\":3600},{\"start\":\"13:15:00\",\"when\":\"Wednesday\",\"duration\":3600},{\"start\":\"15:15:00\",\"when\":\"Thursday\",\"duration\":3600},{\"start\":\"16:15:00\",\"when\":\"Friday\",\"duration\":3600},{\"start\":\"5:15:00\",\"when\":\"Monday\",\"duration\":2700},{\"start\":\"6:15:00\",\"when\":\"Saturday\",\"duration\":2700},{\"start\":\"9:5:00\",\"when\":\"Sunday\",\"duration\":4200}]}','NO'),(6,'Oah9KNLNbMq9kf1I5gE2e3gwm6PEbpDZzV9jIU7UGy30N5JTmnVAqb7MhWrsm5sW','B4E62D236EF5','Water Timer Demo',1,'{\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-09 12:25:34',1,'WATER_TIMER','WT-01','2018-07-31 19:03:30','2019-03-09 06:55:34','NETGEAR-zestl','{\"5\":[{\"start\":\"4:37:00\",\"when\":\"Tuesday\",\"duration\":3600},{\"start\":\"23:23:00\",\"when\":\"Thursday\",\"duration\":2700},{\"start\":\"23:50:00\",\"when\":\"Monday\",\"duration\":1200},{\"start\":\"10:20:00\",\"when\":\"Monday\",\"duration\":1200},{\"start\":\"10:20:00\",\"when\":\"Friday\",\"duration\":1200}],\"4\":[{\"start\":\"2:37:00\",\"when\":\"Wednesday\",\"duration\":3660}]}','NO'),(7,'dphzZ2D4tVxdycnSzCzoiceqQyPDgM6uco3cqGgTBO7a0v2A7CYpPcQFLGi1tovG','68C63AC778D5','Smart Switch Master Bedroom',1,NULL,'2018-12-25 10:47:41',NULL,'WATER_TIMER',NULL,'2018-12-25 10:47:41','2019-01-26 10:09:27',NULL,NULL,'NO'),(8,'8neKwIMi5dVXG6UDQGu7rUXRkHhMVGP333NAMASSNOPuUJ7KpBRRwPIQxZCtFrwI','68C63AC77B7D','Smart Switch Kids Bedroom',1,'{\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-30 10:19:29',11,'WATER_TIMER',NULL,'2018-12-25 10:48:24','2019-03-30 04:49:29','PATIL','{\"5\":[{\"start\":\"19:25:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"7:23:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"7:25:00\",\"when\":\"Monday\",\"duration\":300},{\"start\":\"7:25:00\",\"when\":\"Wednesday\",\"duration\":300},{\"start\":\"7:25:00\",\"when\":\"Tuesday\",\"duration\":300},{\"start\":\"10:1:00\",\"when\":\"Saturday\",\"duration\":300},{\"start\":\"7:25:00\",\"when\":\"Thursday\",\"duration\":300},{\"start\":\"9:53:00\",\"when\":\"Sunday\",\"duration\":300}]}','NO'),(9,'KPT9g3j0ZfmFYeHuh6AHOsp5zZl6ApykesuvwNvv3Rb16TwnZ65OzvU8ufe4FNpU','B4E62D2373DD','Smart Switch TV Panel',4,'{\"4\":\"OFF\",\"5\":\"ON\",\"12\":\"OFF\",\"14\":\"OFF\"}','2019-04-03 23:36:24',1,'WATER_TIMER',NULL,'2018-12-25 10:48:55','2019-04-03 18:06:24','NETGEAR-zestl','{\"5\":[{\"start\":\"17:30:00\",\"when\":\"Monday\",\"duration\":3600},{\"start\":\"21:30:00\",\"when\":\"Wednesday\",\"duration\":12000},{\"start\":\"17:50:00\",\"when\":\"Tuesday\",\"duration\":900},{\"start\":\"20:15:00\",\"when\":\"Tuesday\",\"duration\":900},{\"start\":\"21:15:00\",\"when\":\"Tuesday\",\"duration\":600},{\"start\":\"21:50:00\",\"when\":\"Monday\",\"duration\":1200},{\"start\":\"21:35:00\",\"when\":\"Tuesday\",\"duration\":1500},{\"start\":\"22:4:00\",\"when\":\"Tuesday\",\"duration\":120},{\"start\":\"22:0:00\",\"when\":\"Thursday\",\"duration\":1500},{\"start\":\"15:25:00\",\"when\":\"Saturday\",\"duration\":600}],\"4\":[{\"start\":\"7:30:00\",\"when\":\"Tuesday\",\"duration\":1200},{\"start\":\"5:55:00\",\"when\":\"Tuesday\",\"duration\":900},{\"start\":\"17:55:00\",\"when\":\"Tuesday\",\"duration\":2700},{\"start\":\"23:30:00\",\"when\":\"Tuesday\",\"duration\":9000},{\"start\":\"21:20:00\",\"when\":\"Tuesday\",\"duration\":900},{\"start\":\"21:40:00\",\"when\":\"Tuesday\",\"duration\":1500}],\"12\":[{\"start\":\"1:30:00\",\"when\":\"Monday\",\"duration\":14400},{\"start\":\"22:45:00\",\"when\":\"Saturday\",\"duration\":3000}],\"14\":[{\"start\":\"17:30:00\",\"when\":\"Friday\",\"duration\":2700},{\"start\":\"5:30:00\",\"when\":\"Sunday\",\"duration\":5400}]}','NO'),(10,'lXZQQs3LCVb7vU7LjUB8vktMEL0Xtw4Ou4FkwI68EhgacnVvixENS7zwSzul6zaA','B4E62D236F55','Smart Water Timer',1,'{\"4\":\"OFF\",\"5\":\"OFF\",\"12\":\"OFF\",\"14\":\"OFF\"}','2019-03-21 15:04:02',1,'WATER_TIMER',NULL,'2018-12-25 15:32:32','2019-03-21 09:34:18','NETGEAR-zestl','{\"5\":[{\"start\":\"11:55:00\",\"when\":\"Thursday\",\"duration\":300},{\"start\":\"12:15:00\",\"when\":\"Thursday\",\"duration\":300},{\"start\":\"14:40:00\",\"when\":\"Thursday\",\"duration\":600},{\"start\":\"15:6:00\",\"when\":\"Tuesday\",\"duration\":150}]}','YES'),(11,'0l1HE9FyvGEak8vvUrpfNgnMtczBpB0qX28BbNaHuOSOXnjSPJ7C0vptIY58A6yx','2C3AE8176AD4','Water Timer',1,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-04-03 23:35:52',1,'WATER_TIMER','WT-01','2019-03-02 19:13:06','2019-04-04 12:25:35','NETGEAR-zestl','[]','YES'),(12,'RMpVwJttz4rgC8NHLwdGiYXNl9zIUvLLibHOUbiugKLSSzAE5NkoLic7rLQmgB8y','B4E62D23691A','Smart Switch - Kapil',3,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-12 09:57:19',1,'WATER_TIMER','WT-01','2019-03-03 17:10:14','2019-03-12 04:27:19','NETGEAR-zestl',NULL,'NO'),(13,'1gkg677rps2q0RZIXcqC391hkolfKYRJOq6QZZmATIKS0AoLNH9WM0cniZ9pEOp7','B4E62D236FEB','A-Z Water Timer Demo 1',1,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-21 22:54:00',1,'WATER_TIMER','WT-01','2019-03-15 16:52:15','2019-03-21 17:24:00','NETGEAR-zestl','{\"5\":[{\"start\":\"0:20:00\",\"when\":\"Sunday\",\"duration\":300},{\"start\":\"1:45:00\",\"when\":\"Monday\",\"duration\":300},{\"start\":\"1:45:00\",\"when\":\"Sunday\",\"duration\":300}]}','YES'),(14,'WuOFn3WXbNCjv5hSvHOGn4HuClqYuo5vkZPERTbHazpN8v4pazCNEWwOWp7TOjm8','68C63AC77EAC','Water Timer Demo A',1,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-04-08 23:15:43',11,'WATER_TIMER','WT-01','2019-03-21 10:00:30','2019-04-08 17:45:43','Patil_2.4G','{\"5\":[{\"start\":\"7:25:00\",\"when\":\"Monday\",\"duration\":420},{\"start\":\"7:25:00\",\"when\":\"Tuesday\",\"duration\":420},{\"start\":\"7:25:00\",\"when\":\"Wednesday\",\"duration\":420},{\"start\":\"7:25:00\",\"when\":\"Thursday\",\"duration\":420},{\"start\":\"7:25:00\",\"when\":\"Monday\",\"duration\":420},{\"start\":\"7:25:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"8:25:00\",\"when\":\"Saturday\",\"duration\":420},{\"start\":\"8:25:00\",\"when\":\"Sunday\",\"duration\":420}]}','YES'),(15,'qm9ExsYbwttfdpRDHY1Xi50FxnbjJx9H8wxxhv1DU3SDrZ816JSovLcVk2FBBSMj','68C63AC69AE8','Water Timer Demo B',1,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-04-08 23:16:22',1,'WATER_TIMER','WT-01','2019-03-21 10:01:14','2019-04-08 17:46:22','NETGEAR-zestl','{\"5\":[{\"start\":\"22:21:00\",\"when\":\"Sunday\",\"duration\":180},{\"start\":\"22:26:00\",\"when\":\"Sunday\",\"duration\":180},{\"start\":\"22:31:00\",\"when\":\"Sunday\",\"duration\":180},{\"start\":\"22:36:00\",\"when\":\"Sunday\",\"duration\":180},{\"start\":\"22:45:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"22:50:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"22:55:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"23:0:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"23:15:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"23:20:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"23:30:00\",\"when\":\"Sunday\",\"duration\":240},{\"start\":\"12:30:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"12:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"12:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"13:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"13:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"13:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"14:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"14:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"15:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"16:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"18:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"19:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"20:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"23:0:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"18:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"19:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"20:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"19:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"15:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"17:40:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"21:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"22:20:00\",\"when\":\"Sunday\",\"duration\":600},{\"start\":\"23:40:00\",\"when\":\"Thursday\",\"duration\":600},{\"start\":\"23:20:00\",\"when\":\"Thursday\",\"duration\":600},{\"start\":\"0:0:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"0:20:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"12:40:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"1:20:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"7:30:00\",\"when\":\"Friday\",\"duration\":1200},{\"start\":\"15:20:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"18:40:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"20:20:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"22:0:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"21:0:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"17:0:00\",\"when\":\"Saturday\",\"duration\":600},{\"start\":\"16:40:00\",\"when\":\"Saturday\",\"duration\":600},{\"start\":\"14:0:00\",\"when\":\"Saturday\",\"duration\":600},{\"start\":\"8:0:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"9:0:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"10:0:00\",\"when\":\"Friday\",\"duration\":600},{\"start\":\"12:0:00\",\"when\":\"Saturday\",\"duration\":600}]}','YES'),(16,'rKTAmJiWUEAyjMKmeR3a8b3G0bXhKjz7k5OJpwO8WhiWSDxIhjTOKrT9eZR3Uive','68C63AC77621','Water Timer Demo C',1,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-24 22:01:16',1,'WATER_TIMER','WT-01','2019-03-21 10:01:48','2019-03-24 16:31:16','NETGEAR-zestl',NULL,'NO'),(17,'Np0073iW0aCBywzoibprUHrBh2kTvJyYqgVOTXQnQm1FTENbPfjhXfglC61Fvvjh','2C3AE8178EBB','Water Timer Demo D',1,'{\"12\":\"OFF\",\"14\":\"OFF\",\"4\":\"OFF\",\"5\":\"OFF\"}','2019-03-31 21:07:30',1,'WATER_TIMER','WT-01','2019-03-21 10:02:33','2019-03-31 15:37:30','NETGEAR-zestl',NULL,'NO');
/*!40000 ALTER TABLE `IOT_DeviceInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IOT_UserList`
--

DROP TABLE IF EXISTS `IOT_UserList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOT_UserList` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserEmail` varchar(256) NOT NULL,
  `UserPhone` varchar(16) DEFAULT NULL,
  `UserPassword` varchar(1024) DEFAULT '',
  `UserName` varchar(256) DEFAULT '',
  `UserPhoto` varchar(256) DEFAULT NULL,
  `UserAddr` varchar(512) DEFAULT '',
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `EmailVerified` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `EmailOTPInfo` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `UQ_IOT_UL_UserEmail` (`UserEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IOT_UserList`
--

LOCK TABLES `IOT_UserList` WRITE;
/*!40000 ALTER TABLE `IOT_UserList` DISABLE KEYS */;
INSERT INTO `IOT_UserList` VALUES (1,'pushkarprasad007@gmail.com',NULL,'b9dd04f6bfaaa257ba6bcfdf242a6afb93ccd2a2aba2263515e83ee5ea7926121bb8196f861324bc4d1a4277b857adf4298811a58f122ca80de1183f2a8b4d8d','pushkarprasad007@gmail.com',NULL,'','2018-05-05 10:28:50','2019-02-14 12:22:43','YES','{\"LOGIN_OTP\":{\"otp\":\"347455\",\"valid_upto\":1550147263,\"latest_otp_sent_time\":1550146963},\"stats\":{\"2019-02-14\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(2,'pushkar@zestl.com',NULL,'','',NULL,'','2018-05-24 21:02:50','2018-05-24 21:06:32','NO','{\"LOGIN_OTP\":{\"otp\":\"572517\",\"valid_upto\":1527196292,\"latest_otp_sent_time\":1527195992},\"stats\":{\"2018-05-25\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(3,'ouroborosiot@gmail.com',NULL,'ouroborosiot123','Ouroboros',NULL,'','2018-05-24 21:06:44','2018-05-24 21:08:30','YES','{\"LOGIN_OTP\":{\"otp\":\"981811\",\"valid_upto\":1527196399,\"latest_otp_sent_time\":1527196099},\"stats\":{\"2018-05-25\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(4,'anandakumar.mani@gmail.com',NULL,'xzsony18a','anandakumar',NULL,'','2018-06-26 07:49:30','2018-06-26 07:50:02','YES','{\"LOGIN_OTP\":{\"otp\":\"650030\",\"valid_upto\":1529999670,\"latest_otp_sent_time\":1529999370},\"stats\":{\"2018-06-26\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(5,'kapilbhadke@gmail.com',NULL,'3b9ba1aed7b5e97b01c43cd61769e6525d6e353bc29def7f072c61929b6f92b5eb5dc41a1597e44abb2af1dbe037837047aaccb1a4fa49892a196cb490b4e44a','kapil',NULL,'','2018-07-30 07:33:50','2018-12-04 14:55:57','YES','{\"LOGIN_OTP\":{\"otp\":\"556909\",\"valid_upto\":1543935631,\"latest_otp_sent_time\":1543935331},\"stats\":{\"2018-12-04\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(6,'abc@abc.com',NULL,'','',NULL,'','2018-11-29 16:57:09','2018-11-29 16:57:09','NO','{\"LOGIN_OTP\":{\"otp\":\"312685\",\"valid_upto\":1543510929,\"latest_otp_sent_time\":1543510629},\"stats\":{\"2018-11-29\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(7,'atoto05@gmail.com',NULL,'b9dd04f6bfaaa257ba6bcfdf242a6afb93ccd2a2aba2263515e83ee5ea7926121bb8196f861324bc4d1a4277b857adf4298811a58f122ca80de1183f2a8b4d8d','atoto05@gmail.com',NULL,'','2018-12-29 09:24:15','2019-03-31 14:21:50','YES','{\"LOGIN_OTP\":{\"otp\":\"704796\",\"valid_upto\":1554042410,\"latest_otp_sent_time\":1554042110},\"stats\":{\"2019-03-31\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(8,'rprasad56@gmail.com',NULL,'d9641c9e4aa7c1c0ff4da69ce6d641d52838c07db85287082dfd716983fca5903ad78c084fc472c591bd88c8297fc96548d6e5942947799e4d97c3dead83944b','rprasad56@gmail.com',NULL,'','2018-12-29 10:35:40','2018-12-29 10:55:42','YES','{\"LOGIN_OTP\":{\"otp\":\"141520\",\"valid_upto\":1546081193,\"latest_otp_sent_time\":1546080893},\"stats\":{\"2018-12-29\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(9,'singh.prb@gmail.com',NULL,'a0a9238d98b7c88a9c2e2bc0fa3d656f275d337232f8dc00f2564ff4abbe8d154942b3d8bdf2fa005bd7be58b59673605b4ad489c696f8204a9048f0f4d4d597','singh.prb@gmail.com',NULL,'','2019-02-14 13:21:13','2019-02-14 13:23:01','YES','{\"LOGIN_OTP\":{\"otp\":\"064634\",\"valid_upto\":1550150841,\"latest_otp_sent_time\":1550150541},\"stats\":{\"2019-02-14\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(10,'ashish.miba@gmail.com',NULL,'','',NULL,'','2019-02-14 14:24:35','2019-02-14 14:24:35','NO','{\"LOGIN_OTP\":{\"otp\":\"761726\",\"valid_upto\":1550154575,\"latest_otp_sent_time\":1550154275},\"stats\":{\"2019-02-14\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(11,'patilree@gmail.com',NULL,'b1c3db2424af02172edc49f5e3c878c331230b950dce19fec22b52066d9c93fae05a0a962edaeb800e65b1f65af262a3d851e9ae783c6d9a4543958b4ec28970','patilree@gmail.com',NULL,'','2019-02-16 04:27:33','2019-03-31 13:31:13','YES','{\"LOGIN_OTP\":{\"otp\":\"863396\",\"valid_upto\":1554039349,\"latest_otp_sent_time\":1554039049},\"stats\":{\"2019-03-31\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(12,'pallavimmu89@gmail.com',NULL,'3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79','pallavimmu89@gmail.com',NULL,'','2019-02-26 15:50:12','2019-02-26 15:51:42','YES','{\"LOGIN_OTP\":{\"otp\":\"205817\",\"valid_upto\":1551196586,\"latest_otp_sent_time\":1551196286},\"stats\":{\"2019-02-26\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(13,'sk.das1987@gmail.com',NULL,'a8e19e71362c9cac02dc9b0a07f1e1542d11bf5d4bd5eca7848c74de95b4326f96c65d642f3051a3cd3958804ea118a56ab21e6e5adb0b52f1cc2e5e9464bf98','sk.das1987@gmail.com',NULL,'','2019-02-26 17:25:34','2019-02-26 17:26:01','YES','{\"LOGIN_OTP\":{\"otp\":\"504666\",\"valid_upto\":1551202234,\"latest_otp_sent_time\":1551201934},\"stats\":{\"2019-02-26\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(14,'knnayak@gmail.com',NULL,'ab161cef484cfd06c871ae5b53b617bebac70fe8d6bc8fd32fec2e12095a6bc298fb3e922d52fa5d1094194774442f17aba880dd80d899b2a1d7a6389140ba0c','knnayak@gmail.com',NULL,'','2019-02-27 16:21:42','2019-02-27 16:22:24','YES','{\"LOGIN_OTP\":{\"otp\":\"573542\",\"valid_upto\":1551284802,\"latest_otp_sent_time\":1551284502},\"stats\":{\"2019-02-27\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(15,'amit_aks_007@rediffmail.com',NULL,'01da54138a3ccb299d1e1872e99b525a22505dec2d6aefade31cbeecaeae87b509d80fd18d50a3ced1ad24e775a0b556bef45c1a901e69d76bf28ad9bd5ded9e','amit_aks_007@rediffmail.com',NULL,'','2019-03-09 05:48:50','2019-03-09 05:49:27','YES','{\"LOGIN_OTP\":{\"otp\":\"422890\",\"valid_upto\":1552110830,\"latest_otp_sent_time\":1552110530},\"stats\":{\"2019-03-09\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(16,'rajeevrnpathak@gmail.com',NULL,'606acc2ced280429c58a900952212c4197e31ab6878fab5892315d5015b07295b2f1382b5d1cb23fe24a6d765e302113ef01a658ca16a98a4567492a21006b64','rajeevrnpathak@gmail.com',NULL,'','2019-03-23 15:59:06','2019-03-23 16:00:09','YES','{\"LOGIN_OTP\":{\"otp\":\"314308\",\"valid_upto\":1553357046,\"latest_otp_sent_time\":1553356746},\"stats\":{\"2019-03-23\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(17,'rajeevnayan.kei@gmail.com',NULL,'606acc2ced280429c58a900952212c4197e31ab6878fab5892315d5015b07295b2f1382b5d1cb23fe24a6d765e302113ef01a658ca16a98a4567492a21006b64','rajeevnayan.kei@gmail.com',NULL,'','2019-03-23 16:04:11','2019-03-23 16:05:16','YES','{\"LOGIN_OTP\":{\"otp\":\"702959\",\"valid_upto\":1553357351,\"latest_otp_sent_time\":1553357051},\"stats\":{\"2019-03-23\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(18,'bhoite.vaishali@gmail.com',NULL,'','',NULL,'','2019-03-24 08:23:30','2019-03-24 08:25:41','NO','{\"LOGIN_OTP\":{\"otp\":\"801989\",\"valid_upto\":1553416241,\"latest_otp_sent_time\":1553415941},\"stats\":{\"2019-03-24\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}'),(19,'prashaantpatil@hotmail.com',NULL,'8065bdd13176308c3ad79ce5c24f50e223a6fa31fa8fa44e49a8d71c9e2488ce944810a522bc6e56c25d82af560b6fd17a2441771f316ac624025a39905bf7f4','prashaantpatil@hotmail.com',NULL,'','2019-03-31 13:33:31','2019-03-31 13:35:17','YES','{\"LOGIN_OTP\":{\"otp\":\"565767\",\"valid_upto\":1554039511,\"latest_otp_sent_time\":1554039211},\"stats\":{\"2019-03-31\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":1}}');
/*!40000 ALTER TABLE `IOT_UserList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JF_ContactUs`
--

DROP TABLE IF EXISTS `JF_ContactUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JF_ContactUs` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` text DEFAULT NULL,
  `Mobile` text DEFAULT NULL,
  `EmailID` text DEFAULT NULL,
  `Message` text DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JF_ContactUs`
--

LOCK TABLES `JF_ContactUs` WRITE;
/*!40000 ALTER TABLE `JF_ContactUs` DISABLE KEYS */;
INSERT INTO `JF_ContactUs` VALUES (1,'Pushkar Prasad','9975385693','pushkarprasad007@gmail.com','Hi guys, how are you!','2018-01-01 08:07:49','2018-01-01 08:07:49'),(2,'Aparna Singh','7387826610','atoto05@gmail.com','Hi, I\'m looking to see if you have some Himalayan Honey too!','2018-01-01 08:10:51','2018-01-01 08:10:51'),(3,'Pushkar Prasad','9975385693','pushkarprasad007@gmail.com','Hi','2018-01-02 05:32:06','2018-01-02 05:32:06'),(4,'Pushkar Prasad','9975385693','pushkarprasad007@gmail.com','Hi this should work','2018-01-02 09:41:54','2018-01-02 09:41:54'),(5,'Sailesh Prasad','9900998877','sailesh1981@gmail.com','This is good website','2018-01-06 09:12:51','2018-01-06 09:12:51'),(6,'Aparna Singh','7387826610','atoto05@gmail.com','Please let me know about your website a little more','2018-01-11 18:55:53','2018-01-11 18:55:53'),(7,'Aparna','7387826610','atoto05@gmail.com','I want to enquire more about this water timer.','2018-12-29 10:25:06','2018-12-29 10:25:06');
/*!40000 ALTER TABLE `JF_ContactUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JF_Items`
--

DROP TABLE IF EXISTS `JF_Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JF_Items` (
  `ItemID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(512) NOT NULL,
  `Desc` varchar(8192) NOT NULL,
  `Price` decimal(15,2) DEFAULT NULL,
  `ImageUrl` varchar(2048) DEFAULT NULL,
  `WeightInGms` decimal(15,2) DEFAULT NULL,
  `ItemGSTPercent` decimal(15,2) DEFAULT NULL,
  `IsActive` enum('YES','NO') DEFAULT 'YES',
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ItemDescription` text NOT NULL,
  `URLID` varchar(190) NOT NULL,
  PRIMARY KEY (`ItemID`),
  UNIQUE KEY `UQ_JF_It_URLID` (`URLID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JF_Items`
--

LOCK TABLES `JF_Items` WRITE;
/*!40000 ALTER TABLE `JF_Items` DISABLE KEYS */;
INSERT INTO `JF_Items` VALUES (6,'Manuka Honey 250 MGO+ 500gm','',5400.00,'https://i.ebayimg.com/images/g/TVQAAOSw2tFad7~~/s-l300.jpg',0.00,5.00,'YES','2018-02-27 20:43:26','2018-02-27 20:50:02','Mt Adams 100% New Zealand Manuka Honey Active MGO 250+ 500g  Natural. Mt Adams Manuka HoneyMt Adams Manuka Honey is 100% New Zealand honey and each product is tested and certified to contain a minimum level of methylglyoxal.Why Manuka Honey is So Special?New Zealand Manuka Honey is renowned throughout the world as one of s most prized foods.Scientific evidence has confirmed methylglyoxal as one of the key compounds naturally occurring in New Zealand Manuka Honey.Methylglyoxal is only found in high levels in Manuka Honey. In contrast, other honeys tested worldwide have low levels of methylglyoxal, ranging between 0 to 5mg/kg.MGO 250+ Manuka Honey contains 250mg/kg of methylglyoxal.','Mt-Adams-250MGO-500GM'),(7,'Manuka Honey 30MGO+ 500gm','',4000.00,'https://www.kadac.com.au/retailer-zone/images/inventory/products/436218/front-large.jpg',0.00,5.00,'YES','2018-02-27 20:45:56','2018-02-27 20:50:02','Mt Adams 100% New Zealand Manuka Honey Active MGO 30+ 500g  Natural. Mt Adams Manuka HoneyMt Adams Manuka Honey is 100% New Zealand honey and each product is tested and certified to contain a minimum level of methylglyoxal.Why Manuka Honey is So Special?New Zealand Manuka Honey is renowned throughout the world as one of s most prized foods.Scientific evidence has confirmed methylglyoxal as one of the key compounds naturally occurring in New Zealand Manuka Honey.Methylglyoxal is only found in high levels in Manuka Honey. In contrast, other honeys tested worldwide have low levels of methylglyoxal, ranging between 0 to 5mg/kg. MGO 30+ Manuka Honey contains 30mg/kg of methylglyoxal.','Mt-Adams-30MGO-500GM');
/*!40000 ALTER TABLE `JF_Items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JF_UserCart`
--

DROP TABLE IF EXISTS `JF_UserCart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JF_UserCart` (
  `CartID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(10) unsigned NOT NULL,
  `PaymentStatus` enum('STARTED','PAID') DEFAULT 'STARTED',
  `CostIncTax` decimal(15,2) DEFAULT NULL,
  `ShippingCost` decimal(15,2) DEFAULT NULL,
  `OrderID` varchar(190) NOT NULL,
  `Address` text DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`CartID`),
  UNIQUE KEY `UQ_JF_UC_OrderID` (`OrderID`),
  KEY `FK_JF_UsrCrt_UL_UID` (`UserID`),
  CONSTRAINT `FK_JF_UsrCrt_UL_UID` FOREIGN KEY (`UserID`) REFERENCES `JF_UserList` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JF_UserCart`
--

LOCK TABLES `JF_UserCart` WRITE;
/*!40000 ALTER TABLE `JF_UserCart` DISABLE KEYS */;
INSERT INTO `JF_UserCart` VALUES (5,6,'PAID',1716.00,0.00,'order_9EVw93PI1HYCSp','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-17 19:03:32','2017-12-17 19:03:47'),(6,6,'PAID',1098.00,0.00,'order_9EVyFfubOm6Pt0','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-17 19:05:32','2017-12-17 19:06:10'),(7,7,'PAID',399.00,0.00,'order_9EWCvQNJeEqpG8','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkar@zestl.com\"}','2017-12-17 19:19:26','2017-12-17 19:19:55'),(8,7,'STARTED',399.00,0.00,'order_9EWMMAXHjPxyWG','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-17 19:28:21','2017-12-17 19:28:22'),(9,7,'PAID',399.00,0.00,'order_9EWhNp2bg9rFMI','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-17 19:48:16','2017-12-17 19:48:39'),(10,7,'PAID',1716.00,0.00,'order_9G3gnMQ6hMxBW9','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-21 16:43:35','2017-12-21 16:45:08'),(11,7,'PAID',639.00,0.00,'order_9GUBwWW7rAfQ6d','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-22 18:39:07','2017-12-22 18:48:44'),(12,7,'PAID',798.00,0.00,'order_9GVf3NDuzb2AwJ','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-22 20:05:22','2017-12-22 20:05:58'),(13,7,'PAID',918.00,0.00,'order_9GVrOPIK2aDPEc','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-22 20:17:03','2017-12-22 20:17:24'),(14,7,'PAID',1716.00,0.00,'order_9GWDM9MNa2I3LB','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-22 20:37:50','2017-12-22 20:38:03'),(15,7,'STARTED',949.00,0.00,'order_9GWIDDLS6dpi2r','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-22 20:42:26','2017-12-22 20:42:26'),(16,7,'PAID',2198.00,0.00,'order_9GmJ5SpnRHnnQ0','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-23 12:22:22','2017-12-23 12:26:02'),(17,7,'STARTED',1648.00,0.00,'order_9GmVwB58n0ACZi','{\"name\":\"Pushkar Prasad\",\"addressline2\":\"Nirman Exotica\",\"addressline1\":\"401-B\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2017-12-23 12:34:32','2017-12-23 12:34:32'),(19,7,'PAID',2566.00,0.00,'order_9KE9qwqr8peyYo','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"rprasad56@gmail.com\"}','2018-01-01 05:34:02','2018-01-01 05:34:36'),(20,7,'STARTED',858.00,0.00,'order_9KFqXQiIlQ9e9I','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-01 07:13:09','2018-01-01 07:13:09'),(21,7,'PAID',1648.00,0.00,'order_9KGq5T51MYICbx','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-01 08:11:25','2018-01-01 08:15:12'),(22,7,'PAID',9067.00,0.00,'order_9KpwZ4nUH2KppC','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:31:49','2018-01-02 18:45:10'),(23,7,'PAID',9928.00,0.00,'order_9KqLwff4rClVmi','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:55:51','2018-01-02 18:56:10'),(24,7,'PAID',9890.00,0.00,'order_9KqMnhaJax6EMW','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:56:40','2018-01-02 18:56:50'),(25,7,'PAID',9648.00,0.00,'order_9KqNSSRvCHyk0X','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:57:17','2018-01-02 18:57:26'),(26,7,'PAID',9888.00,0.00,'order_9KqO9xoK09bHQ7','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:57:57','2018-01-02 18:58:10'),(27,7,'PAID',9737.00,0.00,'order_9KqP2MRW79iOul','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"7387826610\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:58:46','2018-01-02 18:58:59'),(28,7,'PAID',9489.00,0.00,'order_9KqPo4Pg5AXueD','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 18:59:30','2018-01-02 18:59:44'),(29,7,'PAID',9739.00,0.00,'order_9KqQZ9fTMIQeWl','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 19:00:13','2018-01-02 19:00:26'),(30,7,'STARTED',858.00,0.00,'order_9KqTiDbjwTxnYr','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-02 19:03:12','2018-01-02 19:03:12'),(31,7,'PAID',4094.00,0.00,'order_9LFWzRoPfgeKSe','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-03 19:33:39','2018-01-03 19:35:56'),(32,7,'PAID',3296.00,0.00,'order_9LFs0LgZJ3tdzy','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"rprasad56@gmail.com\"}','2018-01-03 19:53:32','2018-01-03 19:54:10'),(33,7,'STARTED',3776.00,0.00,'order_9MGcXid67LV6Y1','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-06 09:16:36','2018-01-06 09:16:36'),(34,7,'PAID',1716.00,0.00,'order_9MheyATC7R9Iyx','{\"name\":\"Rajendra Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"7387826610\",\"email\":\"atoto05@gmail.com\"}','2018-01-07 11:43:37','2018-01-07 11:43:53'),(35,7,'PAID',3296.00,0.00,'order_9MhmcPDwZvGTYW','{\"name\":\"Rajendra Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-07 11:50:52','2018-01-07 11:51:10'),(36,6,'PAID',480.00,0.00,'order_9MkjgGlfDlp7W0','{\"name\":\"Rajendra Prasad\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-07 14:44:10','2018-01-07 14:44:23'),(37,6,'PAID',1648.00,0.00,'order_9OPEDTwI2c20Oa','{\"name\":\"Aparna Singh\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram Nagar, Bawdhan\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-11 18:59:49','2018-01-11 19:00:16'),(38,7,'PAID',1317.00,0.00,'order_9OPowtGnQhevEC','{\"name\":\"Sairabh\",\"addressline1\":\"401-B, Nirman Exotica, Ram Nagar Colony\",\"landmark\":\"MH\",\"city\":\"Pune\",\"addressline2\":\"Bawdhan\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-01-11 19:34:35','2018-01-11 19:34:52'),(39,7,'PAID',9870.00,100.00,'order_9h2Bgle89ZWHpS','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B, Nirman Exotica\",\"addressline2\":\"Ram Nagar Colony\",\"landmark\":\"Bawdhan\",\"city\":\"Pune\",\"state\":\"Maharashtra\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-02-27 20:48:30','2018-02-27 20:49:14'),(40,7,'PAID',9870.00,0.00,'order_9h2FMltRTStNon','{\"name\":\"Pushkar\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\",\"landmark\":\"Bawdhan\"}','2018-02-27 20:51:59','2018-02-27 20:52:22'),(41,7,'STARTED',5670.00,0.00,'order_9jHawxsdjptqRT','{\"name\":\"Pushkar\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman Exotica\",\"landmark\":\"Ram nagar\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-03-05 13:10:49','2018-03-05 13:10:49'),(42,7,'STARTED',5670.00,0.00,'order_9jHjaIr0GMwUGT','{\"name\":\"Pushkar\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman\",\"landmark\":\"Near Ram Nagar\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-03-05 13:18:59','2018-03-05 13:19:00'),(43,7,'STARTED',5670.00,0.00,'order_9k13RL1tmO1vzv','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B, Nirman Exotica, Ram Nagar Colony\",\"landmark\":\"Ram ngr\",\"city\":\"Pune\",\"addressline2\":\"Besides\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-03-07 09:39:00','2018-03-07 09:39:00'),(44,7,'STARTED',5670.00,0.00,'order_9mYWbaPZ3ZH774','{\"name\":\"Pushkar\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman\",\"landmark\":\"Near Rajkamal\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-03-13 19:41:30','2018-03-13 19:41:30'),(45,7,'STARTED',5670.00,0.00,'order_9mo9X98WvyJpLE','{\"name\":\"Pushkar\",\"addressline1\":\"401-B\",\"addressline2\":\"Nirman\",\"landmark\":\"Near Rajkamal\",\"city\":\"Pune\",\"state\":\"MH\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-03-14 10:58:45','2018-03-14 10:58:46'),(46,6,'STARTED',5670.00,0.00,'order_Bb7awNYgTlvHOR','{\"name\":\"Pushkar Prasad\",\"addressline1\":\"401-B, Nirman Exotica, Ram Nagar Colony\",\"landmark\":\"MH\",\"city\":\"Pune\",\"addressline2\":\"Bawdhan\",\"state\":\"Maharashtra\",\"pincode\":\"411021\",\"mobile\":\"9975385693\",\"email\":\"pushkarprasad007@gmail.com\"}','2018-12-23 06:48:05','2018-12-23 06:48:05');
/*!40000 ALTER TABLE `JF_UserCart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JF_UserCartItems`
--

DROP TABLE IF EXISTS `JF_UserCartItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JF_UserCartItems` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CartID` int(10) unsigned NOT NULL,
  `ItemID` int(10) unsigned NOT NULL,
  `ItemTitle` text DEFAULT NULL,
  `ItemQuantity` int(10) unsigned NOT NULL,
  `ItemPricePerUnit` decimal(15,2) DEFAULT NULL,
  `ItemGSTPercent` decimal(15,2) DEFAULT NULL,
  `ItemTaxPerUnit` decimal(15,2) DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`ID`),
  KEY `FK_JF_UsrCrtItm_CI_UsrCrt_CI` (`CartID`),
  KEY `FK_JF_UsrCrtItm_IID_Items_IID` (`ItemID`),
  CONSTRAINT `FK_JF_UsrCrtItm_CI_UsrCrt_CI` FOREIGN KEY (`CartID`) REFERENCES `JF_UserCart` (`CartID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_JF_UsrCrtItm_IID_Items_IID` FOREIGN KEY (`ItemID`) REFERENCES `JF_Items` (`ItemID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JF_UserCartItems`
--

LOCK TABLES `JF_UserCartItems` WRITE;
/*!40000 ALTER TABLE `JF_UserCartItems` DISABLE KEYS */;
INSERT INTO `JF_UserCartItems` VALUES (105,39,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-02-27 20:48:30','2018-02-27 20:48:30'),(106,39,7,'Manuka Honey 30MGO+ 500gm',1,4000.00,5.00,200.00,'2018-02-27 20:48:30','2018-02-27 20:48:30'),(107,40,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-02-27 20:51:59','2018-02-27 20:51:59'),(108,40,7,'Manuka Honey 30MGO+ 500gm',1,4000.00,5.00,200.00,'2018-02-27 20:51:59','2018-02-27 20:51:59'),(109,41,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-03-05 13:10:49','2018-03-05 13:10:49'),(110,42,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-03-05 13:19:00','2018-03-05 13:19:00'),(111,43,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-03-07 09:39:00','2018-03-07 09:39:00'),(112,44,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-03-13 19:41:30','2018-03-13 19:41:30'),(113,45,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-03-14 10:58:46','2018-03-14 10:58:46'),(114,46,6,'Manuka Honey 250 MGO+ 500gm',1,5400.00,5.00,270.00,'2018-12-23 06:48:05','2018-12-23 06:48:05');
/*!40000 ALTER TABLE `JF_UserCartItems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JF_UserList`
--

DROP TABLE IF EXISTS `JF_UserList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JF_UserList` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserPhone` varchar(16) DEFAULT NULL,
  `UserName` varchar(256) DEFAULT '',
  `UserPhoto` varchar(256) DEFAULT NULL,
  `UserEmail` varchar(256) DEFAULT NULL,
  `UserAddr` varchar(512) DEFAULT '',
  `CreatedOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModifiedOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `PhoneVerified` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `PhoneOTPInfo` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `UQ_JF_UL_UserPhone` (`UserPhone`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JF_UserList`
--

LOCK TABLES `JF_UserList` WRITE;
/*!40000 ALTER TABLE `JF_UserList` DISABLE KEYS */;
INSERT INTO `JF_UserList` VALUES (6,'8668805314','',NULL,NULL,'','2017-12-17 19:03:11','2018-12-23 06:47:53','NO','{\"LOGIN_OTP\":{\"otp\":\"540016\",\"valid_upto\":1545547973,\"latest_otp_sent_time\":1545547673},\"stats\":{\"2018-12-23\":{\"LOGIN_OTP\":{\"otp_count\":1}},\"total_otp_sent\":7}}'),(7,'9975385693','',NULL,NULL,'','2017-12-17 19:18:58','2018-03-14 11:03:00','NO','{\"LOGIN_OTP\":{\"otp\":\"142302\",\"valid_upto\":1521025508,\"latest_otp_sent_time\":1521025380},\"stats\":{\"2018-03-14\":{\"LOGIN_OTP\":{\"otp_count\":6}},\"total_otp_sent\":30}}');
/*!40000 ALTER TABLE `JF_UserList` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-08 17:46:26
