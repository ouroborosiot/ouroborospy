(function() {

    var app = angular.module("jargoViewer");

    // This controller would intercept the routes which are
    // /cart/payment/:checkoutid
    var PaymentController = function($scope, $http, $q, $location, $routeParams, CheckoutPaymentService) {

        // Used to show Loading bar
        // TODO : Move this to a common place
        $scope.projects_loading = true;

        // initialize productid
        $scope.checkoutid = null;
        $scope.products = null;

        if (('checkoutid' in $routeParams) && !isNaN($routeParams.checkoutid)) {
            $scope.checkoutid = parseInt($routeParams.checkoutid);
        }

        // Call the Cart Payment Service  to actually fetch the data
        CheckoutPaymentService.getCheckoutDetails($scope.checkoutid).then(function(result) {
            //      This is the result
            //      {
            //          'rzp_orderid' : 'rzp_A34RFhgs45',     // RazorPay OrderID - string
            //          'products' : [
            //              {
            //                  'id'    : 101,                // integer
            //                  'mrp'   : 1499.0,             // float
            //                  'units' : 3,                  // integer
            //                  'title' : 'Teddy Bear (M)',   // String
            //                  'tax_percentage' : 10.0,      // Float
            //                  'tax_per_unit' : 149.0,       // float
            //                  'total_tax' : 447.0           // float
            //                  'total_incl_tax' : 4944.0     // float
            //              }, ...
            //          ],
            //          'total_excl_tax' : 4497.0,            // float
            //          'tax' : 447.0,                        // float
            //          'total' : 4944.0                      // float
            //          'delivery_address' : {
            //              'name'  : '...',
            //              'line1' : '...',
            //              'line2' : '...',
            //              'city'  : '...',
            //              'mobile': '...',
            //              'email' : '...'
            //          }
            //      }
            $scope.products = result.products;
            $scope.total_excl_tax = result.total_excl_tax;
            $scope.tax = result.tax;
            $scope.total = result.total;
            // Also get the delivery address
            $scope.delivery_address = result.delivery_address;
            $scope.rzp_orderid = result.rzp_orderid;
            // REST API has returned the response. Stop the Loading
            $scope.projects_loading = false;
        });
        // Anything that you code below would execute immediately even if the
        // response of the getData(..) promise has not returned


        // RAZOR PAYMENT HANDLER

        // The transaction handler function
        $scope.transactionHandler = CheckoutPaymentService.razorPayTransactionComplete;

        // Handler on Button Click of the "Pay Now" button
        $scope.btnClick = function () {
            cartAmount = $scope.total * 100;
            var options = {
                'key': 'rzp_live_HmfDi0dXerLzgK',
                // Insert the amount here, in paise
                'amount': cartAmount,
                'name': '',
                'description': 'Pay for Order#' + $scope.checkoutid,
                'image': '',
                'handler': function (transaction) {
                    $scope.transactionHandler(transaction, cartAmount, $scope.checkoutid);
                },
                'prefill': {
                    'name': $scope.delivery_address.name,
                    'email': $scope.delivery_address.email,
                    'contact': $scope.delivery_address.mobile
                },
                'order_id' : $scope.rzp_orderid
            };
            var rzp1 = new Razorpay(options);
            rzp1.open();
        };
    };

    app.controller("PaymentController", PaymentController);

}());
