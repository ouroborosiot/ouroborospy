(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");

    // Create the factory
    app.factory('ProdSearchService', function($routeParams, $route, $http, $location, $q, UserCart, RestService) {

        var changeSearch = function(searchtext, pageNum) {
            console.log("Came inside ProdSearchService, searchtext :"+searchtext+", pageNum : "+pageNum);
            $location.path("products/search/page/"+pageNum).search({searchtext: encodeURIComponent(searchtext)});
        }

        // Search Products
        // Based on the Search Parameter and the Page Number, show the Results
        // Response :
        // {
        //      'error' : false,
        //      'data' : {
        //          'products' : [
        //              {
        //                  'id' : ...,
        //                  'urlid' : '...',
        //                  'title' : ...,
        //                  'desc' : ...,
        //                  'quantity' : ..,
        //                  'img' : ...,
        //                  'cost' : ...
        //              },
        //              {....}
        //          ],
        //          'more' : false / true           <- If more data is available
        //      }
        // }
        var searchProducts = function(searchtext, pageNum) {
            console.log("Came inside ProdSearchService, searchtext :"+searchtext+", pageNum : "+pageNum);
            // Now make the http API hit to fetch the products
            var request = RestService.invoke_api(
                {
                    method: "get",
                    url: "/products/search",
                    params: {
                        searchtext: searchtext,
                        page: pageNum
                    },
                }
            );
            return request;
        };

        // Get Product Details
        //
        // Response :
        // {
        //     'error' : false,
        //     'data' : {
        //         'products' {
        //             'id' : ...,
        //             'urlid' : '...','
        //             'label' : ...,
        //             'desc' : ...,
        //             'quantity' : ..,
        //             'img' : ...,
        //             'cost' : ...
        //         }
        //     }
        // }
        var getProduct = function(productID) {
            console.log("getProduct  productID :"+productID);
            // Now make the http API hit to fetch the products
            var request = RestService.invoke_api({
                method: "get",
                url: "/product/"+productID,
                // data: {
                //    id: id
                //}
            });
            return request;
        };

        return {
            searchProducts : searchProducts,
            getProduct : getProduct,
            changeSearch : changeSearch,
        };
    });
}());
