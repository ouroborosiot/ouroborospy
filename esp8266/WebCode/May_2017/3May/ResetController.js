(function() {

    var app = angular.module("jargoViewer");

    var ResetController = function($scope, $http, $q, $location, $routeParams, LoginService) {
        $scope.user = {};

        $scope.resetpwd = function () {
            email = $scope.user.email;
            pwd = $scope.user.password;
            otp = $scope.user.otp;

            LoginService.resetpwd(email, pwd, otp).then(function(successData) {
                // This is the correct place to change the location route to payments
                // If Success, change the Route, and point to the /mydevices
                $location.path("mydevices");
            });
      };

      $scope.generateOTP = function () {
          LoginService.generateOTP($scope.user.email).then(function(successData) {
              // Nothing to do
          });
    };
  };

  app.controller("ResetController", ResetController);

}());
