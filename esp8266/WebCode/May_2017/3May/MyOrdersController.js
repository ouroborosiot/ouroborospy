(function() {

    var app = angular.module("jargoViewer");

    // This controller would intercept the routes which are
    // /orders/:checkoutid
    var MyOrdersController = function($scope, $http, $q, $location, $routeParams, CheckoutPaymentService) {

        // Used to show Loading bar
        // TODO : Move this to a common place
        $scope.projects_loading = true;


        // Call the Cart Payment Service  to actually fetch the data
        CheckoutPaymentService.getAllOrderDetails().then(function(result) {
            //  This is the result
            //
            //  'orders' : [
            //      {
            //          'rzp_orderid' : 'rzp_A34RFhgs45',     // RazorPay OrderID - string
            //          'products' : [
            //              {
            //                  'id'    : 101,                // integer
            //                  'mrp'   : 1499.0,             // float
            //                  'units' : 3,                  // integer
            //                  'title' : 'Teddy Bear (M)',   // String
            //                  'tax_percentage' : 10.0,      // Float
            //                  'tax_per_unit' : 149.0,       // float
            //                  'total_tax' : 447.0           // float
            //                  'total_incl_tax' : 4944.0     // float
            //              }, ...
            //          ],
            //          'total_excl_tax' : 4497.0,            // float
            //          'tax' : 447.0,                        // float
            //          'total' : 4944.0                      // float
            //          'delivery_address' : {
            //              'name'  : '...',
            //              'line1' : '...',
            //              'line2' : '...',
            //              'city'  : '...',
            //              'mobile': '...',
            //              'email' : '...'
            //          },
            //          'date_created' : '2018-01-05 12:05:54'
            //      }
            //  ]
            $scope.orders = result.orders;
            // REST API has returned the response. Stop the Loading
            $scope.projects_loading = false;
        });
        // Anything that you code below would execute immediately even if the
        // response of the getData(..) promise has not returned
    };

    app.controller("MyOrdersController", MyOrdersController);

}());
