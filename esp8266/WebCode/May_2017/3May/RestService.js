/**
Filename : RestService.js
**/

(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");

    // Create the factory
    app.factory('RestService', function($http, $q, globalStore, CONFIG) {

        var invoke_api = function(params) {
            // check if params contains method, url, and json_data and params
            if (
                ! angular.isObject(params) ||
                ! params.url ||
                ! params.method
            ) {
                return($q.reject( "Invalid REST API Request!"));
            }

            globalStore.in_progress =  true;

            var method = params.method;
            var url = CONFIG.BASE_URL + params.url;
            var body = null;
            var url_params = null;

            if((method == 'POST') || (method == 'PUT') || (method == 'post') || (method == 'put')) {
                if(params.data) {
                    body = params.data;
                }
            }
            if(params.params) {
                url_params = params.params;
            }

            // Now create the object to Call
            var rest_obj = {
                method : method,
                url : url,
                data : body,
                params : url_params,
                timeout: 25000
            };

            console.log(rest_obj);

            return $http(rest_obj).then(handleSuccess, handleError).finally(function () {
                // Finally, irrespective of whether success or fail, stop the
                // progress bar
                globalStore.in_progress = false;
            });
        };

        // SUCCESS AND ERROR RESPONSE HANDLING

        // I transform the error response, unwrapping the application dta from
        // the API response payload.
        function handleError(response) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject(response.data) ||
                ! response.data.message
                ) {
                showToast("Please check your internet connection!");
                return($q.reject( "Please check your internet connection!"));
            }
            // Check if there is 401 error, in which case, we should log the User out, and then navigate to Login Page
            if (response.status == 401) {
                //LoginService.logout();
                //$location.absUrl("#/login");
                //$location.url("#/login");
                //showToast(response.data.message);
                //$location.reload();
            }
            // Otherwise, use expected error message.
            showToast(response.data.message);
            return($q.reject(response.data.message));
        }

        // I transform the successful response, unwrapping the application data
        // from the API response payload.
        function handleSuccess(response) {
            if(response.data.error == true) {
               showToast(response.data.message);
               return($q.reject(response.data.message));
            }
            // If show message is true, show the toast even if the
            // operation has succeeded
            if(response.data.hasOwnProperty("showmessage")) {
                if(response.data.showmessage == true) {
                    showToast(response.data.message);
                }
            }
            console.log(response.data.data);
            return(response.data.data);
        }

        function showToast(toastMessage) {
            Snackbar.show({text: toastMessage});
        }

        return {
            invoke_api : invoke_api,
        };
    });
}());
