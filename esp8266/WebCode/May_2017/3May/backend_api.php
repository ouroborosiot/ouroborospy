<?php

// 2/15/15 -- all zbot/zvice IDs need to be encrypted versions when info sent over.


/* REST API list
 - Without API key
 / get  				-- done ---
 /user/register post  	-- done ---
 /user/login post  		-- done ---
 /user/logout post  	-- done ---
 /user/password post  	-- done ---
 -- this only sends password retreival email. Password reset done via web after user clicks on retreival link.

 /comment post  																--done--
 /usertags/ID get -- user's mytags -- public, no authorization provided			--done--
 /ztag/publicnotes/ID get -- notes get -- public, no authorization provided		--done--
 /ztag/publicnote/ID get -- note get -- public, no authorization provided		--done--


 - With user API key
 /user/info get  (API key identifies user, so no ID required)	-- done ---
 /user/info post   (API key identifies user, so no ID required)	-- done ---
 /zbot/register/ID put (cant use post since user provides ID)	-- done ---
 /zbot/info/ID get												-- done ---
 /zbot/info/ID put												-- done ---
 /zvice/register/ID put (cant use post since user provides ID)	-- done --- (need to test all types of zvices, with ptag etc
 /zvice/info/ID get												-- done ---
 /zvice/info/ID put												-- done ---

 /media/upload post												-- done ---
 /media/info/ID put												-- done ---
 /media/info/ID get												-- done ---

 /content/upload post
 - think more about ad submission/update/scheduling etc

 /user/mygroups get 											-- done ---
 /user/group get - list of all user groups filtered				-- done ---
 /user/group/register post										-- done ---
 /user/group/subscribe/ID put									-- done ---
 /user/group/info/ID get										-- done ---
 /user/group/info/ID put										-- done ---

 /zbot/group/register post
 /zbot/group/subscribe/ID put
 /zbot/group/info/ID get
 /zbot/group/info/ID put

 /zvice/mygroups get 											-- done ---
 /zvice/group get - list of all  groups filtered				-- done ---
 /zvice/group/register post										-- done ---
 /zvice/group/subscribe/ID put									-- done ---
 /zvice/group/info/ID get										-- done ---
 /zvice/group/info/ID put										-- done ---

 /mytags get -- user's mytags -- private											--done--
 /activity get -- list activity of zvice or user or both 							--done--

 /ztag/interactions/ID get -- user's interactions									--done--
 /ztag/favourite/ID put -- user favourites status									--done--

 /ztag/notes/ID get -- notes get -- with authorization								--done-- still need to test note types and u type etc.
 /ztag/note/ID get -- note get -- with authorization								--done-- still need to test note types and u type etc.
 /ztag/notes/ID put -- notes upload													--done-- still need to test note types and u type etc.

 /ztag/notes/favourite/ID put -- notes favourite status								--done--
 /ztag/notes/delete/ID put -- notes delete											--done--

 /ztag/track/ID put -- track														--done--

 /ztag/ownership/ID get -- ownership/status 										--done--
 /ztag/ownership/ID put -- ownership/status 										--done--

 /ztag/access ID,useremail get -- access permissions for user						--done--
 /ztag/access ID,useremail put -- access permissions for user						--done--

 /ztag/link/ID get -- link															--done--
 /ztag/link/ID put -- link															--done--

 /zvice/stream/ID get -- get list of all streams subscribed to this zvice			--done--
 /zvice/stream/register/ID put -- register a stream									--done--
 /zvice/stream/info/ID get -- info for a stream (provide id)						--done--
 /zvice/stream/info/ID put -- info for a stream (provide id)						--done--

 /zvice/stream/data/ID get -- info for a stream (provide id)						--done--
 /zvice/stream/data/ID put -- info for a stream (provide id)						--done--

 /user/message get (only self access)												--done--
 /user/message put (provide user email)												--done--
 /user/group/message put (provide group id)											--done--

 /user/interactions get (provide user email)										--done--
 /user/interactions put (provide user email)										--done--

 /ztag/follow/ID get (list of all followers for the ztag)							--done--
 /ztag/follow/ID put (provide user email to follow)									--done--


/// /user/follow get (list of all followers for self, and all users and tags being followed) -- this is done via /user/interactions, and get/mytags
/// /user/follow put (provide user email to follow) -- this is done via /user/interactions


///// TEST ONLY /////// - delete fast
 /tagid/ID get -- enc/dec



*/

require_once DOCUMENT_ROOT . "/vendor/autoload.php";

use TwigMeServerApplication\users\login\GoogleLogin;
use TwigMeServerApplication\utility\Utils;

use TwigMeServerApplication\activitylog\ActivityLog;
use TwigMeServerApplication\zvice\ZviceListModelHelper;

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

// This will mandate all the APIs to match  version to what is rule here, unless an API overrides that
\Slim\Route::setDefaultConditions(array(
    //'version' => 'v[1,2,3,4,5,6,7,8,9,10]'
    'version' => 'v([1-9]|1[0-3])'
));

class APIVersionMiddleware extends \Slim\Middleware
{
    public function call()
    {

        $this->app->hook('slim.before.dispatch', array($this, 'onBeforeDispatch'));
        $this->next->call();
    }

    public function onBeforeDispatch()
    {
        $this->app->is_development_environment = IS_DEVELOMPENT_ENVIRONMENT;
        $route_params = $this->app->router()->getCurrentRoute()->getParams();
        $route_headers = $this->app->request->headers;

        if(isset($route_headers['version'])) {
            $version = $route_headers['version'];
            $this->app->api_version = $version;
        } else if(isset($route_params['version'])) {
            $version = $route_params['version'];
            $this->app->api_version = $version;
        }
        // Assumption that the Enc TagID of the User selected is what would be sent back
        // https://twig.me/vX/alpha/beta/gamma?users={\"selected\" : \"ASDGFREGER32S\"}
        // GET Params mean the query parameters. Post Parameters mean the Body
        $filterArr = $this->app->request()->get();
        $this->app->filter = $filterArr;
        // Would be set to NULL if the parameter is not found
        // Now check if the call is coming from iOS and the API it being hit from older deprecated version
        if(\TwigMeServerApplication\utility\HeaderUtils::isUserAgentIOS()) {
            if(isset($this->app->api_version)) {
                if(!apiVersionExceeds(5)) {
                    $response['error'] = true;
                    $response['message'] = 'Please upgrade the app from App Store to continue using';
                    echoResponse(200, $response);
                }
            }
        }
        // Set the language code
        $langCode = \TwigMeServerApplication\utility\HeaderUtils::getLanguageCode();
        if(!empty($langCode)) {
            Utils::setLocalization($langCode);
        }
    }
}

$app->TwigMeLogger = \TwigMeServerApplication\logger\Logger::getInstance();


// This middleware will intercept all the API calls, and store the Version number in $app->api_version
$app->add(new \APIVersionMiddleware());

/*
 * We fetch URL routes from individual files here
 */
$onDemandActionRoutes = TwigMeServerApplication\cards\customization\actions\OnDemandActionUrls::getUrls();
foreach ($onDemandActionRoutes as $onDemandActionRoute) {
    /*
     * Each $onDemandActionRoute is of following format:
     *      {
     *          "HttpMethod": "GET|POST|PUT|DELETE"
     *          "Uri" : "example_uri", // Something like this '/:version/genericcards/:ZviceID'
     *          "MiddleWares": ["middleware1", "middleware2"], //This field is optional
     *          "ClassMap": "\Namespaced\Classpath\To\Your\Controller\Class:handlerFunction"
     *      }
     */
    $parameters = [ $onDemandActionRoute["Uri"] ];
    if (!empty($onDemandActionRoute["MiddleWares"])) {
        $parameters = array_merge($parameters, $onDemandActionRoute["MiddleWares"]);
    }
    if (!empty($onDemandActionRoute["ClassMap"])) {
        $parameters = array_merge(
            $parameters,
            [ $onDemandActionRoute["ClassMap"] ]
        );
    }
    $methodName = strtolower($onDemandActionRoute["HttpMethod"]);
    call_user_func_array([$app, $methodName], $parameters);
}

// <editor-fold>
/**
 * We need to check if there is any route defined in Router class.
 * If yes then call that controller instead
 */
//AcknowledgeController
$app->get('/:version/timezones(/)', '\TwigMeServerApplication\timezones\TimezonesController:getAllSupportedTimezones');


//AcknowledgeController
$app->get('/:version/:OrgZviceID/:ZviceID/cards/:CardID/ack', 'authenticate', '\TwigMeServerApplication\acknowledgement\AcknowledgementController:getAck');

// Move Card API
$app->post('/:version/org/:OrgZviceID/move/card/:SourceCardID/to/tag/:DestinationTagID((/)card/:DestinationCardID)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\card\move\CardMoveController:moveCard');
// Generic Card APIs
$app->post('/:version/genericcards/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\GenericCardAPIHandler:getAllCards');
// Generic Card Customization APIs
$app->post('/:version/genericcards/customize/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\customization\GenericCardCustomization:setCustomCardList');
$app->get('/:version/genericcards/customize/:ZviceID/cardID/:ParentCardID', 'authenticate', '\TwigMeServerApplication\cards\customization\GenericCardCustomization:getConfigurationCards');
// Action customization
$app->post('/:version/customize/actions/:ZviceID',          'authenticate', '\TwigMeServerApplication\cards\customization\ActionCustomization:setCustomizeActions');
$app->post('/:version/resetcustomization/actions/:ZviceID', 'authenticate', '\TwigMeServerApplication\cards\customization\ActionCustomization:resetCustomizeActions');

// Gallery Card APIs
$app->get('/:version/:ZviceID/gallery/:GalleryID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:get');
$app->post('/:version/:ZviceID/gallery', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:post');
$app->put('/:version/:ZviceID/gallery/:GalleryID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:put');
//$app->delete('/:version/:ZviceID/gallery/:GalleryID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:deleteGallery');
$app->post('/:version/delete/:ZviceID/gallery/:GalleryID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:deleteGallery');
$app->post('/:version/archive/:ZviceID/gallery/:GalleryID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:archive');
$app->post('/:version/cleanup/:ZviceID/gallery(/)', 'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:cleanup');
$app->post('/:version/restore/:ZviceID/gallery/:GalleryID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:restore');
//$app->delete('/:version/:ZviceID/gallery/:GalleryID/image/:PicID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:deleteGalleryPic');
$app->post('/:version/delete/:ZviceID/gallery/:GalleryID/image/:PicID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:deleteGalleryPic');
$app->put('/:version/:ZviceID/gallery/:GalleryID/image/:PicID/caption', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:editGalleryPicCaption');
$app->post('/:version/:ZviceID/gallery/:GalleryID/image', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:addGalleryPic');

// Parent Forum Card APIs
$app->get('/:version/:ZviceID/parentforum/:ParentForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\parentforum\ParentForumCardController:get');
$app->post('/:version/:ZviceID/parentforum', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\parentforum\ParentForumCardController:post');
$app->put('/:version/:ZviceID/parentforum/:ParentForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\parentforum\ParentForumCardController:put');
$app->post('/:version/delete/:ZviceID/parentforum/:ParentForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\parentforum\ParentForumCardController:deleteParentForum');

//// User Group Configuration APIs
$app->get('/:version/usergroups/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:getUserGroups');
$app->post('/:version/usergroups/add/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:addUserGroup');
$app->post('/:version/usergroups/:GroupID/delete/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:deleteUserGroup');
$app->put('/:version/usergroups/:GroupID/modify/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:modifyUserGroup');

$app->get('/:version/usergroups/:GroupID/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:getUserGroupsDetails');

$app->post('/:version/usergroups/user/add/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:addUserToGroup');
$app->post('/:version/usergroups/:GroupID/user/:UserTagID/delete/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:delUserFromGroup');
$app->post('/:version/usergroups/:GroupID/usergroup/add/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:addUserGroupToGroup');
$app->post('/:version/usergroups/:GroupID/usergroup/:UserGroupID/delete/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:delUserGroupFromGroup');
$app->post('/:version/usergroups/:GroupID/message/:ZviceID', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:messageAllUsrGrp');
$app->post('/:version/usergroups/message/:ZviceID', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:messageAllUsrGrpName');
$app->post('/:version/usergroups/autosearch/:ZviceID', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:autoSearchUserGroups');
$app->post('/:version/usergroups/search/:ZviceID', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:searchUserGroups');
$app->get('/:version/usergroups/business/user/:UserTagID', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:getAllUserGroupsForBusinessUser');
//User Group User Search
$app->post('/:version/usergroups/:GroupID/search/users/org/:ZviceID((/)page/:PageNum)',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\usergroup\UserGroupController:getPaginatedUserCardsForUG');

// User Search
$app->post('/:version/org/:ZviceID/user/search', 'authenticate', '\TwigMeServerApplication\users\search\UserSearchController:searchOrgUsers');

// Org User relationships
$app->get('/:version/:OrgZviceID/users/:ChildZviceID/relationships(/)',                        'authenticate', '\TwigMeServerApplication\organisation\user_relationship\OrgRelationshipController:getRelationships');
$app->get('/:version/:OrgZviceID/users/:ChildZviceID/linked(/)',                               'authenticate', '\TwigMeServerApplication\organisation\user_relationship\OrgRelationshipController:getLinked');
$app->get('/:version/:OrgZviceID/users/:ParentZviceID/linkedto(/)',                            'authenticate', '\TwigMeServerApplication\organisation\user_relationship\OrgRelationshipController:getLinkedTo');
$app->post('/:version/:OrgZviceID/users/:ChildZviceID/relationships(/)',                       'authenticate', '\TwigMeServerApplication\organisation\user_relationship\OrgRelationshipController:post');
#$app->post('/:version/delete/:OrgZviceID/users/:ChildZviceID/relationships/:ParentZviceID(/)', 'authenticate', '\TwigMeServerApplication\organisation\user_relationship\OrgRelationshipController:delete');
#$app->delete('/:version/:OrgZviceID/users/:ChildZviceID/relationships/:ParentZviceID(/)',      'authenticate', '\TwigMeServerApplication\organisation\user_relationship\OrgRelationshipController:delete');

// Card Attachment APIs
$app->post('/:version/cards/:CardID/attachment/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:uploadAttachment');
$app->post('/:version/cards/:cardID/attachment/s3/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:uploadAttachmentS3');
//$app->delete('/:version/cards/:CardID/attachment/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:deleteAttachment');
$app->post('/:version/delete/cards/:CardID/attachment/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:deleteAttachment');
$app->get('/:version/cards/:CardID/attachment/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:getAttachmentsForCard');
$app->get('/:version/:ZviceID/cards/:cardID/attachments(/)',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:getAttachmentDetailssForCard');
$app->post('/:version/cards/:CardID/bgimage/:ZviceID',   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\AttachmentController:uploadBgImgCards');

// Card Permissions API
$app->post('/:version/card/get/permissions/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\permissions\CardPermissionsController:getCardUserPermissions');
$app->post('/:version/card/permissions/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\permissions\CardPermissionsController:cardSettingsOperation');
$app->put('/:version/cards/permsissions/extraperm/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\permissions\CardPermissionsController:cardExtraPerm');

// Card Comunication Permission API
$app->post('/:version/permissions/communication/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\permissions\CommunicationPermissionsController:showCommunicationSettings');
$app->post('/:version/communication/sms/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\permissions\CommunicationPermissionsController:smsCardCommunication');
$app->post('/:version/communication/mail/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\permissions\CommunicationPermissionsController:emailCardCommunication');
$app->post('/:version/cards/policy/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\PolicyController:post');

// Communication Group API
$app->post('/:version/communication/group/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupController:showCommunicationSettings');
$app->get('/:version/communication/:ZviceID/groups', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupController:getCommGroups');
$app->post('/:version/communication/:ZviceID/groups', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupController:postCommGroups');
$app->put('/:version/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupController:putCommGroups');
//$app->delete('/:version/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupController:deleteCommGroups');
$app->post('/:version/delete/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupController:deleteCommGroups');

// User Profile Communication Group API
$app->get('/:version/user/communication/:ZviceID/groups', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupUserController:getUserCommGroups');
$app->get('/:version/user/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupUserController:getUserCommGroupUserGroups');
$app->post('/:version/user/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupUserController:postUserCommGroups');
//$app->delete('/:version/user/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupUserController:deleteUserCommGroups');
$app->post('/:version/delete/user/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupUserController:deleteUserCommGroups');

// Per Card Communication Group API
$app->post('/:version/card/communication/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupCardController:getCardCommGroups');
$app->post('/:version/card/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupCardController:postCardCommGroups');
$app->post('/:version/card/communication/:ZviceID/inherit', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupCardController:inheritCardCommGroups');
$app->post('/:version/card/communication/:ZviceID/customize', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupCardController:customizeCardCommGroups');
//$app->delete('/:version/card/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupCardController:deleteCardCommGroup');
$app->post('/:version/delete/card/communication/:ZviceID/groups/:GroupID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\settings\CommGroupCardController:deleteCardCommGroup');

// Share Link
//$app->get('/test/decryptshareurl/:EncText(/)', '\TwigMeServerApplication\cards\share\ShareLinkController:getDecryptedShareLink');
$app->get('/:version/share/:EncStr', 'authenticate', '\TwigMeServerApplication\cards\share\ShareLinkController:get');

//Text Card APIs
$app->get('/:version/:ZviceID/textcard/:CardID/page(/(:PageNum))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:getPage');
$app->get('/:version/:ZviceID/textcard/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:getTextCard');
$app->post('/:version/cleanup/:ZviceID/textcard(/)',      'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:cleanup');
$app->post('/:version/archive/:ZviceID/textcard/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:archive');
$app->post('/:version/restore/:ZviceID/textcard/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:restore');

//new API for text card addition / editing with all
$app->post('/:version/textcard/add/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:addTextCard');
$app->post('/:version/textcard/edit/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:editTextCard');

//Link Card APIs
$app->get('/:version/:ZviceID/link/:CardID',          'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\link\LinkController:getLinkCard');
$app->post('/:version/archive/:ZviceID/link/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\link\LinkController:archive');
$app->post('/:version/cleanup/:ZviceID/link(/)',      'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\cards\link\LinkController:cleanup');
$app->post('/:version/restore/:ZviceID/link/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\link\LinkController:restore');

//Pre-defined Cards APIs
$app->get('/:version/:ZviceID/locations', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\location\LocationController:get');
$app->get('/:version/:ZviceID/productsearch', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\product\ProductController:get');
$app->post('/:version/:ZviceID/myuserprofiles/page', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\myuserprofiles\MyUserProfileController:getPage');
$app->get('/:version/:ZviceID/myuserprofiles',       'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\myuserprofiles\MyUserProfileController:get');
$app->get('/:version/:ZviceID/staticdetails',  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\staticdetails\StaticDetailsController:get');
$app->post('/:version/:ZviceID/genericnotes/page', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\genericnote\GenericNoteController:getPage');
$app->get('/:version/:ZviceID/genericnotes',      'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\genericnote\GenericNoteController:get');
$app->post('/:version/:ZviceID/topreviews/page', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\topreview\TopReviewController:getPage');
$app->get('/:version/:ZviceID/topreviews',       'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\topreview\TopReviewController:get');
$app->post('/:version/:ZviceID/contacts/page', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\contact\ContactController:getPage');
$app->get('/:version/:ZviceID/contacts',       'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\contact\ContactController:get');
$app->get('/:version/:ZviceID/healthcheck',    'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\healthcheck\HealthCheckController:get');

//Departments APIs
$app->get('/:version/:ZviceID/departments/:DepartmentZviceID/page', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\department\DepartmentController:getPage');
$app->get('/:version/:ZviceID/departments/:DepartmentZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\department\DepartmentController:get');

// Attendance Fast Scan APIs
$app->get('/:version/:ZviceID/fastscan/attendance/:FSCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:get');
$app->post('/:version/fastscan/attendance/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:post');
$app->put('/:version/:ZviceID/fastscan/attendance/:FSCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:put');
$app->put('/:version/:ZviceID/fastscan/attendance/:FSCardID/commentattrib/config', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:putCommentAttribConfig');
//$app->delete('/:version/:ZviceID/fastscan/attendance/:FSCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:delete');
$app->post('/:version/delete/:ZviceID/fastscan/attendance/:FSCardID',  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:delete');
$app->post('/:version/cleanup/:ZviceID/fastscan/attendance(/)', 'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:cleanup');
$app->post('/:version/archive/:ZviceID/fastscan/attendance/:FSCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:archive');
$app->post('/:version/restore/:ZviceID/fastscan/attendance/:FSCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:restore');
$app->get('/:version/fastscan/attendance/:ZviceID/users/:FSCardID', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:getAllowedUsers');
$app->post('/:version/:ZviceID/fastscan/attendance/:FSCardID/sync', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:postAttendance');
$app->post('/:version/:ZviceID/manual/attendance/:FSCardID/sync', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceManualScanController:postManualAttendance');
$app->get('/:version/ondemand/:ZviceID/manual/attendance/:FSCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceManualScanController:getManualAttendanceAction');
$app->post('/:version/:ZviceID/fastscan/attendance/:FSCardID/report', 'authenticate', '\TwigMeServerApplication\cards\attendance\reports\AttendanceFastScanCardReportController:getAttendanceReport');
$app->post('/:version/:ZviceID/fastscan/attendance/mailreport/usergroup/:UserGroupID', 'authenticate', '\TwigMeServerApplication\cards\attendance\reports\AttendanceUserGroupReportController:mailAttendanceReport');
$app->post('/:version/:ZviceID/fastscan/attendance/:FSCardID/mailreport', 'authenticate', '\TwigMeServerApplication\cards\attendance\reports\AttendanceFastScanCardReportController:mailAttendanceReport');
$app->get('/:version/fastscan/attendance/:ENCZVICEID/card/:FSCardID/activeusers/:DateOffset(/:Type)', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:getActiveUsersForDate');
$app->post('/:version/fastscan/attendance/:ENCZVICEID/card/:FSCardID/activeusers/:DateOffset/mail(/:Type)', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceFastScanController:mailActiveUsersForDate');
$app->get('/:version/fastscan/attendance/:ENCZVICEID/card/:FSCardID/tally', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceTallyController:tallyRecords');
$app->post('/:version/:ZviceID/slowscan/attendance/:FSCardID/sync', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceSlowScanController:postAttendance');
// Migration of data from old data to new
$app->get('/:version/attendance/migrate_data/:ZviceID', 'ENABLE_TRANSACTION', '\TwigMeServerApplication\cards\attendance\AttendanceSlowScanController:migrateAttendanceDataToDynamoDB');
// Attendance Handover APIs
$app->post('/:version/attendance/:ZviceID/handover/:CardID/scan', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceHandoverScanController:scanUser');
$app->post('/:version/attendance/:ZviceID/handover/:FSCardID/addRequest/:ParentUserTagID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceHandoverScanController:putHandoverRequests');
$app->post('/:version/attendance/:ZviceID/handover/:FSCardID/complete/:ChildUserTagID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceHandoverScanController:completeHandoverRequest');
$app->get('/:version/attendance/:ZviceID/handover/:FSCardID/pendingrequests', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceHandoverScanController:getPendingHandoverRequests');
$app->post('/:version/attendance/:ZviceID/handover/:FSCardID/delRequest/:ChildUserTagID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\AttendanceHandoverScanController:deleteHandoverRequests');
// Attendance Loyalty APIs
$app->post('/:version/attendance/:ZviceID/loyalty/:CardID/scan', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\loyalty\UserAttendanceLoyaltyScanController:scanUser');
$app->get('/:version/attendance/:ZviceID/loyalty/:FSCardID/records(/(:PageNum(/)))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\attendance\loyalty\UserAttendanceLoyaltyScanController:getLoyaltySessionRecords');

// Card Publish API
// $app->post('/:version/genericcards/:ZviceID/publish/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\common\CardPublishController:post');
$app->post('/:version/textcard/:ZviceID/publish/:CardID',       'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\textcard\TextCardHackController:publishHide');
$app->post('/:version/form/:ZviceID/publish/:CardID',           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:publishHide');
$app->post('/:version/formsubmission/:ZviceID/publish/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:publishHide');
$app->post('/:version/forum/:ZviceID/publish/:CardID',          'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:publishHide');
$app->post('/:version/forumcomments/:ZviceID/publish/:CardID',  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\comments\ForumCommentsController:publishHide');
$app->post('/:version/gallery/:ZviceID/publish/:CardID',        'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\gallery\GalleryCardController:publishHide');
$app->post('/:version/:ZviceID/calendar/:CardID/publish',       'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:publishHide');
$app->post('/:version/:ZviceID/events/:CardID/publish',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:publishHide');

//Form Configuration APIs
$app->get('/:version/add_action/:ZviceID/forms(/:ParentCardID)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:onDemandActionAddForm');
$app->get('/:version/:ZviceID/forms/:FormID(/)',    'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:getPage');
//Form Dependency APIs
$app->post('/:version/:OrgZviceID/fetchwidgets/:ZviceID/forms/:FormID/:FormMetaID(/)',  'authenticate', '\TwigMeServerApplication\forms\dependency\DynamicWidgetsController:fetchDependantWidgets');
$app->post('/:version/:OrgZviceID/dependencymap/:ZviceID/forms/:FormID(/)',  'authenticate', '\TwigMeServerApplication\forms\dependency\DynamicWidgetsController:buildDependancyMap');
//$app->get('/:version/:ZviceID/forms(/(:FormID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:get');
$app->get('/:version/:ZviceID/formdetails/:FormID(/)',  'authenticate', '\TwigMeServerApplication\forms\FormController:getFormDetails');
//$app->get('/:version/:ZviceID/formdetails/:FormID(/)',  '\TwigMeServerApplication\forms\FormController:getFormDetails');
$app->post('/:version/search/:ZviceID/forms(/)',  'authenticate', '\TwigMeServerApplication\forms\FormController:searchForms');
$app->post('/:version/delete/:ZviceID/forms(/(:FormID))',  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:delete');
$app->post('/:version/archive/:ZviceID/forms(/(:FormID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:archive');
$app->post('/:version/restore/:ZviceID/forms(/(:FormID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:restore');
$app->post('/:version/cleanup/:ZviceID/forms(/)',          'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\forms\FormController:cleanup');
$app->post('/:version/:ZviceID/forms(/)',                  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:post');
$app->put('/:version/:ZviceID/forms(/(:FormID))',          'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:put');
//$app->delete('/:version/:ZviceID/forms(/(:FormID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormController:delete');

//Form Submission APIs
$app->get('/:version/:ZviceID/forms/:FormID/submissions(/(:FormSubmissionID))',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:getSubmissions');
$app->post('/:version/:ZviceID/forms/:FormID/searchbyFormID',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:searchSubmissions');
$app->get('/:version/:ZviceID/forms/:FormID/submissions/:FormSubmissionID/history(/)',  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:getSubmissionHistory');
$app->post('/:version/ack/:ZviceID/forms/:FormID/submissions/:FormSubmissionID(/)',     'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:ackSubmissions');
$app->post('/:version/:ZviceID/forms/:FormID/submissions(/)',                           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:postSubmissions');
$app->put('/:version/:ZviceID/forms/:FormID/submissions(/:FormSubmissionID)',           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:putSubmissions');
$app->get('/:version/:ZviceID/forms/:FormID/submissions/:FormSubmissionIDSource/copy/:FormSubmissionIDDest',           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:copySubmissions');
$app->put('/:version/bulk/:ZviceID/formsubmissions(/)',           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:putBulkEditAPI');
$app->post('/:version/search/:OrgZviceID/forms/:FormID/submissions(/)',           'authenticate', '\TwigMeServerApplication\forms\FormSubmissionSearchController:search');
//Form Submisison REST
$app->get('/:version/rest/:OrgZviceID/formsubmissions/:FormSubmisisonID/history', 'authenticate', '\TwigMeServerApplication\forms\rest\FormSubmissionRestController:getRESTHistory');
//$app->post('/:version/search/:OrgZviceID/forms/:FormID/submissions(/)',           '\TwigMeServerApplication\forms\FormSubmissionSearchController:search');
//$app->delete('/:version/:ZviceID/forms/:FormID/submissions(/:FormSubmissionID)',        'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionController:deleteSubmissions');

//PDF for form submissions
$app->post('/:version/:ZviceID/forms/submissions/pdf/:FormID', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionsPDF:post');
$app->get('/:version/:ZviceID/forms/submission/download/:FormSubmissionID', '\TwigMeServerApplication\forms\FormSubmissionsPDF:downloadFormSubmission');

//PDF download for FG Dossier
$app->get('/:version/:ZviceID/dossier/view/:WFETag', 'authenticate', '\TwigMeServerApplication\forms\FormSubmissionsPDF:viewFGDossier');
$app->get('/:version/:ZviceID/dossier/download/:WFETag', '\TwigMeServerApplication\forms\FormSubmissionsPDF:downloadFGDossier');

// Report Generation APIs
$app->post('/:version/:ZviceID/reports/:CardID', 'authenticate', '\TwigMeServerApplication\reports\ReportController:post');
// Activity Logs Report Generation
$app->post('/:version/activitylogs', 'authenticate', '\TwigMeServerApplication\reports\ReportCloudWatchActivityLogs:post');

//Calendar APIs
$app->get('/:version/:ZviceID/calendars/:CalendarID(/)',          'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:get');
$app->post('/:version/delete/:ZviceID/calendars/:CalendarID(/)',  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:delete');
$app->post('/:version/cleanup/:ZviceID/calendars(/)',             'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\calendar\CalendarController:cleanup');
$app->post('/:version/archive/:ZviceID/calendars/:CalendarID(/)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:archive');
$app->post('/:version/restore/:ZviceID/calendars/:CalendarID(/)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:restore');
$app->post('/:version/:ZviceID/calendars(/)',                     'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:post');
$app->put('/:version/:ZviceID/calendars/:CalendarID(/)',          'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:put');
//$app->delete('/:version/:ZviceID/calendars/:CalendarID(/)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:delete');
$app->post('/:version/:ZviceID/calendar/:CalendarID/blockEventsfortheday(/)', 'ENABLE_TRANSACTION', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\CalendarController:blockEventsForTheDay');

//Calendar Events
$app->post('/:version/delete/:ZviceID/calendars/:CalendarID/events(/(:EventID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:deleteEvents');
$app->post('/:version/cleanup/:ZviceID/events(/)',                                'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:cleanup');
$app->get('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:getEvents');
$app->get('/:version/:ZviceID/calendars/:CalendarID/eventslist(/(:EventID))',     'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:getEventsList');
$app->get('/:version/:OrgZviceID/myeventslist(/)',                                                      'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:getMyScheduleForBiz');
$app->get('/:version/:OrgZivceID/pendingrequests(/)',                                                   'authenticate', '\TwigMeServerApplication\calendar\appointments\AppointmentsController:pendingRequests');
$app->get('/:version/:OrgZivceID/confirmedrequests(/)',                                                 'authenticate', '\TwigMeServerApplication\calendar\appointments\AppointmentsController:confirmedRequests');
$app->put('/:version/:ZviceID/calendars/:CalendarID/events/:EventID/confirmappointment(/)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\appointments\AppointmentsController:confirmAppointment');
$app->get('/:version/:ZivceID/nextavailableappointments/:CalendarID(/)',                                'authenticate', '\TwigMeServerApplication\calendar\appointments\AppointmentsController:getNextAvailableAppointments');
$app->post('/:version/:ZviceID/calendars/:CalendarID/events(/)',                  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:postEvents');
$app->put('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:putEvents');
//$app->delete('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\CalendarEventsController:deleteEvents');

//Calendar Events RSVP/Attendees
$app->put('/:version/change_status/:ZviceID/calendars/:CalendarID/events/:EventID/attendees(/)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:putAttendance');
$app->post('/:version/delete/:ZviceID/calendars/:CalendarID/events/:EventID/attendees(/)',      'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:deleteAttendance');
$app->get('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/attendees',              'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:getAttendance');
$app->post('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/attendees',             'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:postAttendance');
//$app->delete('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/attendees',           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:deleteAttendance');
$app->post('/:version/delete/:ZviceID/calendars/:CalendarID/events(/(:EventID))/rsvp',           'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:deleteRSVP');
$app->post('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/rsvp',                  'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:postRSVP');
$app->put('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/rsvp',                   'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:putRSVP');
//$app->delete('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/rsvp',                'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:deleteRSVP');
$app->post('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/notify',                'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:postNotify');
$app->post('/:version/:ZviceID/calendars/:CalendarID/events(/(:EventID))/notifyattendee',        'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:postNotifyAttendee');
$app->post('/:version/:ZviceID/calendars/:CalendarID/events/:EventID/reminderservice',           'ENABLE_TRANSACTION',                 '\TwigMeServerApplication\calendar\events\attendance\EventAttendanceController:reminderService');

//Set New Password
$app->post('/:version/users/passwordinteraction', 'ENABLE_TRANSACTION',                             '\TwigMeServerApplication\users\register_new_password\RegisterNewPasswordController:postNewPasswordInteraction');
$app->put('/:version/users/setnewpassword',       'ENABLE_TRANSACTION', 'only_appkey_authenticate', '\TwigMeServerApplication\users\register_new_password\RegisterNewPasswordController:putResetPassword');

// Tag Provisioning
$app->get('/:version/nexttag/:ZbotID/tags/:RequestedTags', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\CustomerProvisioningController:getNextAvailableTags');
$app->get('/:version/provision/:ZbotID/numtags/:TotalTags(/(provision/:ProvisionedTags))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\CustomerProvisioningController:createNewTagRanges');

//Self provisioning
$app->get('/:version/self_provisioning/:ZviceID(/)', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\SelfProvisioningController:getPage');
$app->get('/:version/self_provisioning(/)',          'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\SelfProvisioningController:get');
$app->post('/:version/self_provisioning(/)',         'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\SelfProvisioningController:post');
//Self provisioning cards
$app->get('/:version/self_provisioning/:ZviceID/available_cards(/)',                  'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\AvailableCardsController:get');
$app->post('/:version/addcard/self_provisioning/:ZviceID/calendar(/)',                'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\calendar\SelfOrgCalendarController:post');
$app->post('/:version/addcard/self_provisioning/:ZviceID/gallery(/)',                 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\gallery\SelfOrgGalleryController:post');
$app->post('/:version/addcard/self_provisioning/:ZviceID/forum(/)',                   'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\forums\SelfOrgForumsController:post');
$app->get('/:version/self_provisioning/:ZviceID/forum/:ForumID/page(/(:PageNum(/)))', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\forums\SelfOrgForumsController:getPage');
$app->post('/:version/addcard/self_provisioning/:ZviceID/locationtracking(/)',        'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\location_tracking\SelfOrgLocationTrackingController:post');
$app->post('/:version/addcard/self_provisioning/:ZviceID/textcard(/)',                'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\textcard\SelfOrgTextCardController:post');
$app->get('/:version/self_provisioning/:ZviceID/textcard/:CardID/page(/:PageNum(/))', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\cards\textcard\SelfOrgTextCardController:getPage');
//Self provisioning users
$app->get('/:version/self_provisioning/:ZviceID/users(/)',                    'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\users\SelfOrgUserController:getExploreUsers');
$app->get('/:version/share/self_provisioning/:ZviceID/users/:UserZviceID(/)', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\users\SelfOrgUserController:getUserDetailsPage');
$app->get('/:version/self_provisioning/:ZviceID/users/:UserZviceID(/)',       'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\users\SelfOrgUserController:getUserDetailsPage');
$app->post('/:version/search/self_provisioning/:ZviceID/users(/)',            'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\provisioning\self_provisioning\users\SelfOrgUserController:postUserSearchCards');
//Customer provisioning
$app->get('/:version/customers(/(:CustomerID))',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\customer_provisioning\CustomerProvisioningController:get');
$app->post('/:version/customers(/)',                     'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\customer_provisioning\CustomerProvisioningController:post');
$app->put('/:version/customers(/(:CustomerID))',         'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\customer_provisioning\CustomerProvisioningController:put');
$app->delete('/:version/customers(/(:CustomerID))',      'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\customer_provisioning\CustomerProvisioningController:delete');
$app->post('/:version/delete/customers(/(:CustomerID))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\provisioning\customer_provisioning\CustomerProvisioningController:delete');
$app->post('/:version/customers/copy/customer',   'authenticate', '\TwigMeServerApplication\provisioning\customer_provisioning\CustomerProvisioningController:copyCustomerFromTemplate');

// Report : Library Rental/Reading History info
$app->post('/:version/library/rental/:ZviceID/report', 'authenticate', '\TwigMeServerApplication\usecase\library\LibraryRentalReportController:getRentalReport');
$app->post('/:version/library/reading/:ZviceID/report', 'authenticate', '\TwigMeServerApplication\usecase\library\LibraryRentalReportController:getReadingReport');

// Dashboard Recent Businesses, Favourited Tags, Trending Tags, My Membership
$app->get('/:version/dashboard/recentbusiness(/(:PageNum))', 'authenticate', '\TwigMeServerApplication\dashboard\DashboardController:getRecentlyAddedBusinesses');
$app->get('/:version/dashboard/favourites(/(:PageNum))', 'authenticate', '\TwigMeServerApplication\dashboard\DashboardController:getFavourites');
$app->get('/:version/dashboard/trending(/(:PageNum))', 'authenticate', '\TwigMeServerApplication\dashboard\DashboardController:getTrendingBusinesses');
$app->get('/:version/dashboard/membership(/(:PageNum))', 'authenticate', '\TwigMeServerApplication\dashboard\DashboardController:getMemberships');
$app->post('/:version/dashboard/trending', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\dashboard\DashboardController:updateTrendingBusinesses');
$app->post('/:version/zvice/dashboard', 'authenticate', '\TwigMeServerApplication\dashboard\DashboardController:getDashboardCards');

// Forum and Forum Comments APIs
$app->get('/:version/forum/:ZviceID/:ForumID/page(/(:PageNum))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:getPage');
$app->get('/:version/forum/:ZviceID/:ForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:get');
$app->post('/:version/forum/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:post');
$app->put('/:version/forum/:ZviceID/:ForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:put');
$app->post('/:version/forum/:ZviceID/delete/:ForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:delete');
$app->post('/:version/cleanup/forum/:ZviceID(/)', 'ENABLE_TRANSACTION', 'systemUserAuthenticate', '\TwigMeServerApplication\forum\ForumCardController:cleanup');
$app->post('/:version/forum/:ZviceID/archive/:ForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:archive');
$app->post('/:version/forum/:ZviceID/restore/:ForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\ForumCardController:restore');
$app->get('/:version/forum/:ZviceID/comments/:ForumID(/(:PageNum))', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\comments\ForumCommentsController:get');
$app->post('/:version/forum/:ZviceID/comments/:ForumID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\comments\ForumCommentsController:post');
$app->put('/:version/forum/:ZviceID/comments/:ForumID/:CommentID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\comments\ForumCommentsController:put');
$app->post('/:version/forum/:ZviceID/comments/:ForumID/delete/:CommentID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\forum\comments\ForumCommentsController:delete');

    // User Self Signup
$app->get('/:version/:ZviceID/selfsignup', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\selfsignup\SelfSignupController:get');
$app->post('/:version/delete/:ZviceID/selfsignup', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\selfsignup\SelfSignupController:deleteRequest');
$app->post('/:version/add/:ZviceID/selfsignup', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\selfsignup\SelfSignupController:signUpRequest');
$app->post('/:version/modify/config/:ZviceID/selfsignup', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\selfsignup\SelfSignupController:updateSelfSignupConfiguration');
// Loyalty Points
$app->get('/:version/:ZviceID/loyalty', 'authenticate', '\TwigMeServerApplication\loyalty\LoyaltyPointsController:get');
$app->get('/:version/:ZviceID/loyalty/processed(/(:PageNum(/(user/:UserZviceID(/)))))', 'authenticate', '\TwigMeServerApplication\loyalty\LoyaltyPointsController:getProcessedBillSubmissionCards');
$app->get('/:version/:ZviceID/loyalty/pending(/(:PageNum(/(user/:UserZviceID(/)))))', 'authenticate', '\TwigMeServerApplication\loyalty\LoyaltyPointsController:getUnprocessedBillSubmissionCards');
// Loyalty -> User Profile
$app->post('/:version/:OrgZviceID/loyalty/acknowledge/:RecordID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\loyalty\userprofile\UserLoyaltyPointsController:acknowledgeBillSubmission');
$app->get('/:version/:ZviceID/loyalty/userpoints', 'authenticate', '\TwigMeServerApplication\loyalty\userprofile\UserLoyaltyPointsController:getUserPointDetailCards');
$app->get('/:version/:OrgZviceID/loyalty/usercoupons(/(:UserZviceID))', 'authenticate', '\TwigMeServerApplication\loyalty\userprofile\UserLoyaltyPointsController:getUserCouponDetailCards');
$app->get('/:version/:OrgZviceID/loyalty/userbills(/(:UserZviceID))', 'authenticate', '\TwigMeServerApplication\loyalty\userprofile\UserLoyaltyPointsController:getUserBillDetailCard');
$app->post('/:version/:ZviceID/loyalty/user/:UserZviceID/billsubmit', 'authenticate', '\TwigMeServerApplication\loyalty\userprofile\UserLoyaltyPointsController:submitBill');
// Loyalty -> Configure Feature & Conversion %age
$app->post('/:version/:ZviceID/configure/loyalty', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\loyalty\LoyaltyPointsController:configureLoyaltyFeature');
$app->post('/:version/:ZviceID/add/shops/loyalty', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\loyalty\LoyaltyPointsController:configureLoyaltyShops');

// Form Submission Loyalty -> Get Processed/Pending Submissions
$app->get('/:version/:ZviceID/form/loyalty/pending(/:PageNum(/catalogue/:CatalogueID(/user/:UserZviceID(/))))', 'authenticate', '\TwigMeServerApplication\loyalty\form_catalogue\LoyaltyFormController:getUnprocessedFormSubmissionCards');
$app->get('/:version/:ZviceID/form/loyalty/processed(/:PageNum(/catalogue/:CatalogueID(/user/:UserZviceID(/))))', 'authenticate', '\TwigMeServerApplication\loyalty\form_catalogue\LoyaltyFormController:getProcessedFormSubmissionCards');
// Form Submission Loyalty -> User Profile -> Submit Form,
$app->post('/:version/:OrgZviceID/form/loyalty/acknowledge/:RecordID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\loyalty\form_catalogue\UserLoyaltyFormController:acknowledgeFormSubmission');
$app->post('/:version/:OrgZviceID/form/:CatalogueID/loyalty/formsubmit(/user/:UserZviceID)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\loyalty\form_catalogue\UserLoyaltyFormController:submitLoyaltyForm');
$app->get('/:version/:OrgZviceID/form/:CatalogueID/loyalty(/user/:UserZviceID)', 'authenticate', '\TwigMeServerApplication\loyalty\form_catalogue\UserLoyaltyFormController:getUserCatalogueDetailCard');
// Form Loyalty Settings
$app->get('/:version/loyaltyforms/:ZviceID', 'authenticate', '\TwigMeServerApplication\loyalty\form_catalogue\LoyaltyFormController:getLoyaltyForms');
$app->post('/:version/loyaltyforms/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', '\TwigMeServerApplication\loyalty\form_catalogue\LoyaltyFormController:postLoyaltyForms');
$app->put('/:version/loyaltyforms/:ZviceID/catalogue/:CatalogueID', 'authenticate', 'ENABLE_TRANSACTION', '\TwigMeServerApplication\loyalty\form_catalogue\LoyaltyFormController:putLoyaltyForms');
$app->post('/:version/loyaltyforms/:ZviceID/catalogue/:CatalogueID/formcreate', 'authenticate', 'ENABLE_TRANSACTION', '\TwigMeServerApplication\loyalty\form_catalogue\LoyaltyFormController:createModifyCatalogueForm');


//Location Tracking Card to add users APIs
$app->get('/:version/lt/:ZviceID/:LTCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:get');
$app->post('/:version/lt/add/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:post');
$app->put('/:version/lt/:ZviceID/:LTCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:put');
$app->post('/:version/lt/:ZviceID/delete/:LTCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:delete');
$app->get('/:version/lt/:ZviceID/users/:LTCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:getUsers');
$app->post('/:version/lt/:ZviceID/users/add/:LTCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:addUser');
$app->post('/:version/lt/:ZviceID/users/delete/:LTCardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:deleteUser');
$app->post('/:version/lt/:ZviceID/trip/status/:LTCardID/:isStart', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:setLTTripStatus');
$app->post('/:version/lt/:ZviceID/route', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\LTCardController:plotPath');
$app->post('/:version/geofence/set/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\lt\GeoFenceController:post');
$app->post('/:version/geofence/enable/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\lt\GeoFenceController:enableGeoFence');
$app->post('/:version/geofence/disable/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\lt\GeoFenceController:disableGeoFence');


//Products Showcase Carousel Card to add products APIs
$app->get('/:version/carousel/products/:ZviceID/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:get');
$app->post('/:version/carousel/products/add/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:post');
$app->put('/:version/carousel/products/:ZviceID/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:put');
$app->post('/:version/carousel/products/:ZviceID/delete/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:delete');
$app->get('/:version/carousel/products/:ZviceID/product/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:getProducts');
$app->post('/:version/carousel/products/:ZviceID/product/add/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:addProduct');
$app->post('/:version/carousel/products/:ZviceID/product/delete/:ProductID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\products\ProductsCarouselCardController:deleteProduct');
//Banner Carousel Card to add entry APIs
$app->get('/:version/carousel/banner/:ZviceID/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:get');
$app->post('/:version/carousel/banner/add/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:post');
$app->put('/:version/carousel/banner/:ZviceID/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:put');
$app->post('/:version/carousel/banner/:ZviceID/delete/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:delete');
$app->get('/:version/carousel/banner/:ZviceID/banner/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:getBannerEntries');
$app->post('/:version/carousel/banner/:ZviceID/banner/add/:CardID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:addBannerEntry');
$app->post('/:version/carousel/banner/:ZviceID/banner/delete/:BannerID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:deleteBannerEntry');
$app->post('/:version/carousel/banner/:ZviceID/banner/:CardID/entry/:BannerID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\carousel\banner\BannerCarouselCardController:editBannerEntry');
// Location Tracking of users
$app->post('/:version/entity/track(/(:entityID))', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\lt\LTController:post');
$app->post('/:version/get/entity/track(/:Minutes)', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\lt\LTController:get');
$app->get('/:version/track/shareurl', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\lt\LTController:getShareURL');

//organisation APIs
$app->get('/:version/organisation/:OrgZviceID/users(/:ZviceID(/))', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getUsers');
$app->post('/:version/:OrgZviceID/orguserprofile/:ZviceID',       'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:postUserProfile');
$app->post('/:version/:OrgZviceID/orgusersoftwigmeuser/:ZviceID', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getOrgUserForTwigMeUser');
$app->get('/:version/:ZviceID/linktotwigmeuser', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getLinkToTwigmeUser');
$app->get('/:version/:ZviceID/favourites',       'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getFavourites');
$app->get('/:version/:ZviceID/attendance((/)days/:Days((/(end/:EndTime))))', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getAttendance');
$app->get('/:version/:ZviceID/attendance/memid/:MembershipID((/)days/:Days((/(end/:EndTime))))', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getAttendanceForMembershipID');
$app->get('/:version/:ZviceID/exploreusers',     'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getExploreUsers');
// Org User Activate/Deactivate
$app->post('/:version/org/:OrgZviceID/user/:UserZviceID/activestatus(/)', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:changeActiveStatus');
//library APIs
$app->get('/:version/library/:ZviceID/explorebooks',            'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\library\LibraryController:getExploreBooks');
$app->get('/:version/library/:ZviceID/specificcard/:CardID',    'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\library\LibraryController:getSpecificCard');
$app->get('/:version/:ZviceID/interactions/:DepartmentZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getInteractionCardAPI');
$app->get('/:version/:ZviceID/userspecificcards',               'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\organisation\OrgUserController:getUserSpecificCardsAPI');

// Membership APIs
$app->get('/:version/membership/:ZviceID/plans', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:getMembershipPlanAPI');
$app->get('/:version/membership/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:getMemberships');
$app->post('/:version/membership/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:postMembership');
$app->put('/:version/membership/:MembershipID/org/:OrgZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:putMembership');
$app->post('/:version/delete/membership/:MembershipID/org/:OrgZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:deleteMembership');
$app->get('/:version/membership/user/:UserZviceID(/:UserMembershipID)', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:getUserMemberships');
$app->post('/:version/membership/user/:UserZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:postUserMembership');
$app->put('/:version/membership/user/:UserZviceID/plan/:UserPlanID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:putUserMembership');
$app->post('/:version/membership/:OrgZviceID/user/:UserZviceID/pausehistory/plan/:PlanID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:getMembershipPauseResumeHistory');

//$app->put('/:version/membership/user/:UserZviceID/plan/edit/:UserPlanID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:putEditUserMembership');
$app->post('/:version/membership/delete/user/:UserZviceID/plan/:UserPlanID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:deleteUserMembership');
$app->get('/:version/membership/users/plan/:MembershipID/org/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:getUsersforMembership');
$app->get('/:version/membership/mymemberships/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:getMyMembershipPlans');
//Membership Reminder service
$app->post('/:version/membership/reminders/:UserZviceID', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\membership\MembershipController:reminderService');
//Membership Renew
$app->post('/:version/membership/renew/user/:UserZviceID/plan/:UserMembershipID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:renewUserMembership');
$app->get('/:version/membership/renew/history/user/:UserZviceID/plan/:UserMembershipID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:renewHistoryUserMembership');
$app->post('/:version/membership/checkout/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\membership\MembershipController:checkoutMembership');

// OTP Generation
$app->post('/:version/generate/otp', 'appkey_authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\phoneotp\OTPController:generateOTP');
$app->post('/:version/user/otp/resetpasswd', 'appkey_authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\phoneotp\OTPController:resetPassword');
$app->post('/:version/user/otp/activate', 'appkey_authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\phoneotp\OTPController:activatePhone');

// Special API to send out the APWD given Akey, email and sha512(pwd)
$app->post('/:version/thirdparty/get/apwd', '\TwigMeServerApplication\login\LoginController:getAPWD');
// Special API to send out the MPWD given Akey, email and sha512(pwd)
$app->post('/:version/thirdparty/get/mpwd', '\TwigMeServerApplication\login\LoginController:getMPWD');

// Notification APIs to store Notification data into the DynamoDB
$app->get('/:version/notification(/messages/:AfterMessageID)','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerCron\notification\dynamodb\NotificationDynamoController:getMessages');
$app->get('/:version/notification/message/:MessageID','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerCron\notification\dynamodb\NotificationDynamoController:getMessage');
$app->post('/:version/notification/markread/message','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerCron\notification\dynamodb\NotificationDynamoController:markAsReadMessages');

// API to send the verification of emailID
$app->post('/:version/twiguser/profile/:AutoITagID/send_verificationmail', 'authenticate', 'TwigMeServerApplication\user\contactupdate\TwigMeUserContactController:sendMailVerification');
$app->post('/:version/twiguser/profile/send_verificationmail/validate_otp', 'authenticate', 'TwigMeServerApplication\user\contactupdate\TwigMeUserContactController:validateEmailOTP');
$app->post('/:version/twiguser/profile/:AutoITagID/send_phone_otp', 'authenticate', 'TwigMeServerApplication\user\contactupdate\TwigMeUserContactController:sendPhoneVerification');
$app->post('/:version/twiguser/profile/send_verificationphone/validate_otp', 'authenticate', 'TwigMeServerApplication\user\contactupdate\TwigMeUserContactController:validatePhoneOTP');

// Products APIs
$app->post('/:version/products/:ZviceID/add','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:addProduct');
$app->put('/:version/products/:ZviceID/product/:ProductID','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:updateProduct');
$app->post('/:version/products/delete/:ZviceID/product/:ProductID','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:deleteProduct');
$app->post('/:version/products/images/:ZviceID/product/:ProductID','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:addImagesToProduct');
$app->post('/:version/products/images/delete/:ZviceID/product/:ProductID','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:deleteImagesForProduct');
$app->post('/:version/products/filters','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:getAllFiltersForCategory');
$app->post('/:version/products/search(/page/:PageNum)','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:searchProducts');
$app->get('/:version/products/details/:OrgZviceID/product/:ProductID','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:getProductDetails');
$app->post('/:version/products/autosearch(/page/:PageNum)','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:autoSearchProducts');
$app->post('/:version/products/category/autosearch', 'authenticate', 'TwigMeServerApplication\products\ProductsController:getAutoSearchCategories');
$app->post('/:version/products/category/filter/widgets', 'authenticate', 'TwigMeServerApplication\products\ProductsController:getCategoryFilterWidgets');
$app->put('/:version/products/:OrgZviceID/maxproducts/:MaxProducts','ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\products\ProductsController:setProductMaxCountForOrg');
// Items APIs
// Get Item Card
$app->get('/:version/items/:ZviceID/itemtype/:ItemTypeID/searchcard', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemCardController:get');
// Items
$app->get('/:version/items/:ZviceID/itemtype/:ItemTypeID/item/:ItemID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemsController:get');
$app->post('/:version/items/:ZviceID/itemtype/:ItemTypeID/item', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemsController:post');
$app->put('/:version/items/:ZviceID/itemtype/:ItemTypeID/item/:ItemID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemsController:put');
$app->delete('/:version/items/:ZviceID/itemtype/:ItemTypeID/item/:ItemID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemsController:delete');
// Item Categories
$app->get('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr/:AttrID/page/:PageNum((/)parentcategory/:ParentCategoryID)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrCategoryController:get');
$app->post('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr/:AttrID/category', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrCategoryController:post');
$app->put('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr/:AttrID/category/:Category', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrCategoryController:put');
$app->post('/:version/delete/items/:ZviceID/ItemTypeID/:ItemID/attr/:AttrID/category/:Category', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrCategoryController:delete');
$app->post('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr/:AttrID/category/autosearch', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrCategoryController:search');
// Item Attr
$app->get('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrController:get');
$app->post('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrController:post');
$app->put('/:version/items/:ZviceID/itemtype/:ItemTypeID/attr/:AttrID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrController:put');
$app->post('/:version/delete/items/:ZviceID/itemtype/:ItemTypeID/attr/:AttrID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemAttrController:delete');
// Item Types
$app->get('/:version/items/:ZviceID/itemtype(/:ItemTypeID)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemTypeController:get');
$app->post('/:version/items/:ZviceID/itemtype', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemTypeController:post');
$app->put('/:version/items/:ZviceID/itemtype/:ItemTypeID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemTypeController:put');
// Item Images
$app->post('/:version/items/:ZviceID/itemtype/:ItemTypeID/images/item/:Item', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemImageController:post');
$app->post('/:version/items/:ZviceID/itemtype/:ItemType/images/delete/item/:ItemID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemImageController:delete');
// Item Search Controller
$app->post('/:version/items/:ZviceID/itemtype/:ItemTypeID/search(/page/:PageNum)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\items\ItemSearchController:searchItems');
// Address APIs
// Area APIs
$app->get('/:version/address/:ZviceID/area(/:AreaCode)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\payments\address\DeliveryAddressController:get');
$app->post('/:version/address/:ZviceID/area', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\payments\address\DeliveryAddressController:post');
$app->put('/:version/address/:ZviceID/area/:AreaID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\payments\address\DeliveryAddressController:put');
$app->delete('/:version/address/:ZviceID/area/:AreaID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\payments\address\DeliveryAddressController:delete');
// User Address API
$app->post('/:version/address/:ZviceID/user/:UserZviceID', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\payments\address\UserDeliveryAddressController:post');

//Independent API to send email
$app->post('/:version/:ZviceID/send/email', 'authenticate', 'TwigMeServerApplication\email\EmailController:post');
$app->post('/:version/email/send/basic', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\email\EmailController:sendBasicEmail');

//Edit policies healthcheck card
$app->post('/:version/:ZviceID/healthcheck/policy',    'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\healthcheck\HealthCheckController:postPolicies');
//reactivation email to non-active users healthcheck card
$app->post('/:version/healthcheck/email/reactivate/:ZviceID',    'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\healthcheck\HealthCheckController:reactivationEmail');
//Email to users with org as favourite
$app->post('/:version/healthcheck/email/favourites/:ZviceID',    'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\cards\healthcheck\HealthCheckController:emailToFavourites');

//Tasks APIs
$app->get('/:version/tasks/:ZviceID', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:getTasks');
$app->post('/:version/tasks/add/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:addTask');
$app->post('/:version/tasks/edit/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:editTask');
$app->post('/:version/tasks/delete/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:deleteTask');
$app->get('/:version/tasks/users/:ZviceID/:TaskID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:getTaskUsers');
$app->get('/:version/tasks/:ZviceID/:CardID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:getTasks');
$app->post('/:version/tasks/comment/add/:ZviceID', 'ENABLE_TRANSACTION', 'authenticate', 'TwigMeServerApplication\tasks\comments\TasksCommentsController:post');
$app->get('/:version/user/tasks/get/:ZviceID', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:getTasksforUser');
$app->post('/:version/tasks/:TaskID/reminders/:ZviceID', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\tasks\TasksController:reminderService');
$app->get('/:version/tasks/getsingle/:ZviceID/:TaskID', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:getSingleTask');
// Tasks Leaderboard
$app->get('/:version/tasks/:ZviceID/leaderboard(/:StartEpoch/:EndEpoch)', 'authenticate', 'TwigMeServerApplication\tasks\TasksController:getTasksLeaderboard');

//API to download TwigMe Users Details for Super admin only
$app->get('/:version/report/user/details/:emailID', 'authenticate', '\TwigMeServerApplication\reports\ReportController:TwigMeUsersCSV');

//Coupons API
$app->get('/:version/coupons/:ZviceID', 'authenticate', 'TwigMeServerApplication\coupons\CouponsController:getCoupons');
$app->get('/:version/coupons/get/purchased/:ZviceID/:CouponID', 'authenticate', 'TwigMeServerApplication\coupons\CouponsController:purchasedCouponHistory');
$app->get('/:version/coupons/get/single/:ZviceID/:CouponID', 'authenticate', 'TwigMeServerApplication\coupons\CouponsController:getSingleCoupon');
$app->get('/:version/coupons/get/coupon/redeem/:ZviceID/:UserCouponID', 'authenticate', 'TwigMeServerApplication\coupons\CouponsController:getCouponRedeemView');
$app->post('/:version/coupons/add/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\coupons\CouponsController:addCoupon');
$app->post('/:version/coupons/edit/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\coupons\CouponsController:editCoupon');
$app->post('/:version/coupons/delete/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\coupons\CouponsController:deleteCoupon');
$app->post('/:version/coupons/buy/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\coupons\CouponsController:buyCoupon');
$app->post('/:version/coupons/redeem/:ZviceID/:UserCouponID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\coupons\CouponsController:redeemCoupon');
$app->post('/:version/coupons/get/coupon/search/:ZviceID/:CouponID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\coupons\CouponsController:searchCoupon');

// Login API
$app->get('/:version/user/:username/getuserstate', 'appkey_authenticate', '\TwigMeServerApplication\login\LoginController:getUserState');
$app->post('/:version/user/login', 'appkey_authenticate', '\TwigMeServerApplication\login\LoginController:loginUser');

//Cart : Checkout
$app->post('/:version/cart/create/:ZviceID(/:UserZviceID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\cart\CheckoutController:checkoutCart');
$app->post('/:version/cart/confirm/:ZviceID(/:UserZviceID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\cart\CheckoutController:confirmCart');
$app->get('/:version/cart/:ZviceID/myorders(/:PaymentItemID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\cart\CheckoutController:getMyOrders');
// Real Cart :-)
$app->get('/:version/cart/:ZviceID/usercart(/:UserZviceID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\cart\usercart\UserCartController:get');
$app->post('/:version/cart/:ZviceID/usercart(/:UserZviceID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\cart\usercart\UserCartController:post');

// Workflow
$app->get('/:version/on_demand/workflow/:OrgZviceID/add(/)',                                   'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:getActionAddWorkflow');
$app->get('/:version/myworkflows/:ZviceID(/)',                                        'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:getMyWorkflowsAPI');
$app->get('/:version/workflow/:ZviceID(/id/:WorkflowID)',                             'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:get');
$app->get('/:version/:ZviceID/workflows/:WorkflowID/documents(/)',                    'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:getAllDocumentsForWorkflow');
$app->get('/:version/search/workflow/:ZviceID(/)',                                    'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:searchWorkflowsByFilters');
$app->post('/:version/workflow/:ZviceID/search',                                      'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:search');
$app->post('/:version/workflow/:ZviceID',                                             'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:post');
$app->post('/:version/workflow/:ZviceID/type',                                        'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:postType');
$app->put('/:version/workflow/config/:ZviceID/type/:WorkflowTypeID(/id/:WorkflowID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:putConfiguration');
$app->get('/:version/workflow/:ZviceID/cards',                                        'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:getWorkflowCard');
$app->get('/:version/:ZviceID/forms/submissions/workflow/:WorkflowID',                'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowController:getWorkflowSubmissionsInfo');
//Workflow element APIs
$app->get('/:version/:OrgZviceID/workflows/:WorkflowID/elements/:WFTag(/:PageNum(/))', 'authenticate', '\TwigMeServerApplication\workflow\elements\WorkflowElementController:getWorkflowElementCards');
// Workflow Key Value DynamoDB Controller API
$app->get('/:version/workflow/:OrgZviceID/:Key',               'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowDynamoController:get');
$app->post('/:version/workflow/:OrgZviceID/:Key/value/:Value', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowDynamoController:post');
// Worflow Graph
$app->get('/:version/:ZviceID/workflow/:WorkflowID/graph/:SubFlow', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowGraphController:getWorkflowElementGraph');
$app->get('/:version/workflow/:ZviceID/project/charts', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getWFProjectCharts');
$app->get('/:version/workflow/:ZviceID/timeline/charts(/:WFID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getWFTimeLineCharts');
$app->get('/:version/workflow/:ZviceID/timeline/table(/:WorkflowID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getWFStatusTable');
$app->get('/:version/workflow/:ZviceID/timeline1/table', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:createTimeLineChartTableFromData');
$app->get('/:version/workflow/:ZviceID/timeline2/table(/:WFID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getTableForWeeklyTimeLines');
$app->get('/:version/workflow/:ZviceID/timeline3/table(/:WFID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getWFRevenueTrackerTable');
$app->get('/:version/workflow/:ZviceID/timeline4/table(/:WFID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getIdeationTrackerTable');
$app->get('/:version/workflow/:ZviceID/timeline/genepath/table(/:WorkflowID)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowChartsController:getGenepathTopTable');
$app->get('/:version/workflow/:ZviceID/update/status', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowDaemonController:updateWFElemStatus');
// Some extra APIs
$app->post('/:version/workflow/search/:ZviceID/formsubmissions', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\workflow\WorkflowSearchController:findMatchingWFIDs');
$app->post('/:version/push/qna', 'TwigMeServerApplication\workflow\StateQnAController:getQnaState');
//
//Razor Pay Payments
//capture payment
$app->post('/:version/payment/success', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\payments\PaymentsController:paymentSuccess');
$app->post('/:version/payment/success/backup/:UserID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\payments\PaymentsController:paymentSuccessBackup');
$app->post('/:version/razor_pay/enable/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\razor_pay\RazorPayController:enableRazorPay');
$app->post('/:version/razor_pay/disable/:ZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\razor_pay\RazorPayController:disableRazorPay');
$app->get('/:version/payment/categories/:ZviceID', 'authenticate', 'TwigMeServerApplication\payments\PaymentsController:getPaymentsDashboardCategories');
$app->get('/:version/payment/transactions/:ZviceID', 'authenticate', 'TwigMeServerApplication\payments\PaymentsController:getPaymentsDashboardTransactions');
$app->post('/:version/payment/item/status/:ZviceID/:UserZviceID', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\payments\paymentItems\PaymentItemsController:changeDeliveryStatus');

//admin actions API
$app->get('/:version/admin/actions/:OrgZviceID(/)', 'authenticate', 'TwigMeServerApplication\organisation\OrgController:getAdminActions');
$app->get('/:version/:OrgZviceID/departments(/)', 'authenticate', 'TwigMeServerApplication\organisation\OrgController:getAllDepartments');

//bottom bar
$app->get('/:version/:OrgZviceID/bottombar(/)',                    'authenticate',                       'TwigMeServerApplication\organisation\bottom_bar\BottomBarController:manageBottomBar');
$app->post('/:version/:OrgZviceID/bottombar(/)',                   'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\organisation\bottom_bar\BottomBarController:addElementToBottomBar');
$app->put('/:version/:OrgZviceID/bottombar/:ElementID(/)',         'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\organisation\bottom_bar\BottomBarController:editElement');
$app->post('/:version/delete/:OrgZviceID/bottombar/:ElementID(/)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\organisation\bottom_bar\BottomBarController:removeElementFromBottomBar');

//Leadership Board
$app->get('/:version/:OrgZviceID/:ZviceID/leadershipboard/:CardID/leaders(/)', 'authenticate',                       'TwigMeServerApplication\forms\leadership_board\LeadershipBoradController:getLeaders');
$app->get('/:version/:OrgZviceID/:ZviceID/leadershipboard/:CardID(/)',         'authenticate',                       'TwigMeServerApplication\forms\leadership_board\LeadershipBoradController:get');
$app->post('/:version/:OrgZviceID/:ZviceID/leadershipboard(/)',                'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\forms\leadership_board\LeadershipBoradController:post');
$app->put('/:version/:OrgZviceID/:ZviceID/leadershipboard/:CardID(/)',         'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\forms\leadership_board\LeadershipBoradController:put');
$app->post('/:version/delete/:OrgZviceID/:ZviceID/leadershipboard/:CardID(/)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\forms\leadership_board\LeadershipBoradController:delete');
//Leadership Board Map
$app->get('/:version/:OrgZviceID/:ZviceID/leadershipboardmap/:CardID(/)',         'authenticate',                       'TwigMeServerApplication\forms\leadership_board\LeadershipBoardMapController:get');
$app->post('/:version/:OrgZviceID/:ZviceID/leadershipboardmap/:CardID(/)',        'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\forms\leadership_board\LeadershipBoardMapController:post');
$app->post('/:version/delete/:OrgZviceID/:ZviceID/leadershipboardmap/:CardID/form/:FormID(/)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\forms\leadership_board\LeadershipBoardMapController:delete');

//feedback
$app->get('/:version/feedback_form',    'authenticate', 'TwigMeServerApplication\feedback\FeedbackController:onDemandAction');
$app->post('/:version/feedback_receive', 'authenticate', 'TwigMeServerApplication\feedback\FeedbackController:postFeedback');

//My Schedule
$app->get('/:version/myschedule(/)',          'authenticate', 'TwigMeServerApplication\calendar\schedule\ScheduleController:getMyScheduleOnDemandAction');
$app->post('/:version/myschedule(/)',         'authenticate', 'TwigMeServerApplication\calendar\schedule\ScheduleController:postMyScheduleForBusiness');
$app->get('/:version/myschedule/:ZviceID(/)', 'authenticate', 'TwigMeServerApplication\calendar\schedule\ScheduleController:getMyScheduleForBusiness');

//Tags
$app->post('/:version/:OrgZviceID/tags(/(:ZviceID(/)))', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\tags\TagsController:postTags');
$app->post('/:version/filter/:OrgZviceID/tags/:ZviceID(/)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\tags\TagsController:getFilteredTags');
$app->post('/:version/search/:OrgZviceID/tags/:ZviceID/cards(/:PageNum)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\tags\TagsController:getCardsFromTags');
$app->post('/:version/search/:OrgZviceID/tags/:ZviceID(/)', 'ENABLE_TRANSACTION', 'authenticate', '\TwigMeServerApplication\tags\TagsController:getTags');

//KidzCare APIs
$app->post('/:version/kidzcare/:ZviceID/reports/upload(/)',              'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\reports\KidzCareReportsController:uploadReport');
$app->post('/:version/kidzcare/:ZviceID/reports(/)',                     'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\reports\KidzCareReportsController:fetchReports');
$app->post('/:version/kidzcare/:ZviceID/form/:FormID/submit(/)',         'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\forms\KidzCareFormSubmissionController:postSubmissions');
$app->post('/:version/kidzcare/:ZviceID/calendar/:CalendarID/search(/)', 'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\calendar\KidzCareEventController:searchEvents');
$app->post('/:version/kidzcare/:ZviceID/content/upload(/)',              'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\reports\KidzCareReportsController:uploadContent');
$app->post('/:version/kidzcare/:ZviceID/content(/)',                     'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\reports\KidzCareReportsController:fetchContent');
$app->post('/:version/kidzcare/:ZviceID/get/content(/)',                     'authenticate', 'ENABLE_TRANSACTION', 'TwigMeServerApplication\kidz_care_hack\reports\KidzCareReportsController:getContent');

//File Upload URL
$app->post('/:version/:ZviceID/fileupload(/(:CardID(/)))', 'authenticate', 'TwigMeServerApplication\S3\S3Controller:uploadTempFile');

//Get Card by CardID
$app->get('/:version/get/card/:ZviceID/:CardID', 'authenticate', 'TwigMeServerApplication\cardsHelper\CardsHelperController:get');

// </editor-fold>

// DELETE THIS API Immediately after Summer Theme is over

function todoDeleteTheme($summerBusinessTagID, $title, $msg) {
    try {
        $email = NULL; $phone = NULL;
        $requestParam = getRequestParams();
        if (isset($requestParam['email'])) {
            $email = $requestParam['email'];
        } else if (isset($requestParam['phone'])) {
            $phone = $requestParam['phone'];
        } else {
            throw new TwigException("Neither phone nor email provided");
        }

        $summerBusinessDecTagID = Utils::getDecryptedZviceID($summerBusinessTagID);
        $dbConnector = getPartitionDBHandle($summerBusinessDecTagID);
        $zlMH = new ZviceListModelHelper($dbConnector, 0);
        $summerZviceRow = $zlMH->findZviceRow($summerBusinessDecTagID);

        // Find the User Identifier
        $userIdentifier = !empty($email)?$email:$phone;
        list($success, $userID) = getUserID($userIdentifier);

        if(!$success) {
            // Account does not exist
            // Create one for that Email/Phone
            list($success, $errorMsg) = registerUser($userIdentifier, $userIdentifier, RANDOMSHA512, NULL/*mysqli*/, ""/* emailMsg */, false /* createdBySelf */, false);
            if(!$success) {
                throw new TwigException("Error in User Registration");
            }
        }

        if(isValidPhone($userIdentifier)) {
            // Send out an SMS
            \TwigMeServerApplication\phoneotp\SMSController::sendSMS([$userIdentifier], "Thank you for your interest in " . $summerZviceRow->Title . ". Download the TwigMe app if you haven't: " . ZESTLWWW . "/app. Please login with your phone number $userIdentifier");
        } else {
            // Send out an email
            $extraParams["BusinessImageUrl"] = $summerZviceRow->ProfilePic;
            $extraParams["OrgZviceID"]  = $summerBusinessTagID;
            $extraParams["UserEmail"] = $email;
            $extraParams["UserName"] = $email;
            $subject = "Thank you for your interest in " . $summerZviceRow->Title;
            $message = "Thank you for your interest in " . $summerZviceRow->Title . ". Explore more and interact with this business via the TwigMe app.";
            sendEmail($email, $email, $subject, $message, $extraParams);
        }

        // Now find the UserID again
        list($success, $userID) = getUserID($userIdentifier);
        if(!$success) {
            throw new TwigException("Error finding the User ID");
        }
        // Mark the Summer Business as a favourite
        list($success, $errormsg) = updateUserZtags($userID, $summerBusinessDecTagID, 'FAVOURITE', 0, 0, $dbConnector);
        // Now that the user is either created, send the Notification
        pushNotificationToAllUsers(
            [$userID],
            $title /*$title*/,
            $msg/*$msg*/,
            Utils::buildUrl("/zvice/detailscard/$summerBusinessTagID")/*$url=NULL*/,
            "POST"/*$method=NULL*/,
            NULL/*$jsondata=NULL*/,
            $summerZviceRow->ProfilePic/*$imgurl=NULL*/
        );
        // Send response
        $statusCode = 200;
        $response["error"] = false;
    } catch (TwigException $tex) {
        \Slim\Slim::getInstance()->TwigMeLogger->logException($tex);
        $statusCode = 200;
        $response["error"] = true;
        $response["message"] = $tex->getMessage();
    } catch (Exception $ex) {
        \Slim\Slim::getInstance()->TwigMeLogger->logException($ex);
        $statusCode = 200;
        $response["error"] = true;
        $response["message"] = "Something went wrong, please contact TwigME support.";
    }
    echoResponse($statusCode, $response);
}

$app->post('/:version/tododelete/summer(/)', function($version) use($app) {
    todoDeleteTheme(SpecialConsts::SUMMER_THEME_TAGID, "Summer Activities in your area", "Click here to See all the Summer Activities in your area");
});

$app->post('/:version/tododelete/joystreet(/)', function($version) use($app) {
    todoDeleteTheme(SpecialConsts::JOYSTREET_THEME_TAGID, "Joystreet Activities", "Click here to See all the Joystreet Activities");
});

//Admin customer provisioning HTML screen
$app->post('/:version/admin/provision(/)', function($version) use($app) {
    $output = [];
    $args   = [
        'password'         => $_POST['pPassword'],
        'customer_name'    => $_POST['pCustomerName'],
        'new_customer'     => $_POST['pNewCustomer'],
        'no_of_tags'       => $_POST['pNoOfTags'],
        'extra_tags'       => $_POST['pExtraTags'],
        'no_of_extra_tags' => $_POST['pNoOfExtraTags'],
        'primary_itag'     => $_POST['pPrimaryITag'],
        'secondary_itag'   => $_POST['pSecondaryITag'],
        'url'              => PROTO . ZESTLWWW,
    ];
    //error_log(json_encode($args));
    //TODO Check for shell script attach
    exec("python /opt/provision/provision.py '" . json_encode($args) . "'", $output);
    $result = json_decode($output[0]);
    if ($result->success) {
        http_response_code(200);
        echo isset($result->data)? $result->data: "Provisioning successful";
    } else {
        http_response_code(400);
        echo !empty($result->error) ? $result->error : "Error while provisioning.";
    }

    //Log the activity here
    $activityData = [
        'ActionType'  => ActivityLog::ACTION_TYPE_INSERT,
        'RequestBody' => $_POST,
        'MetaData'    => [
            'Operation' => ActivityLog::OPERATION_CUSTOMER_PROVISIONING_ADMIN_ADD
        ]
    ];
    ActivityLog::log($activityData);

    exit(0);
});

//Admin customer provisioning HTML screen
$app->post('/:version/admin/login(/)', function($version) use($app) {
    $output = [];
    $uName  = $_POST['username'];
    $pWord  = $_POST['password'];
    $args   = [
        'username' => $uName,
        'password' => $pWord,
        'url'      => PROTO . ZESTLWWW,
    ];
    //error_log(json_encode($args));
    //TODO Check for shell script attach
    exec("python /opt/provision/login.py '" . json_encode($args) . "'", $output);
    $result = json_decode($output[0]);
    if ($result->success) {
        http_response_code(200);
        /**
         * Since we are loading javascript in ajax call following warning will be issued by browser:
         *
         * Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user's experience.
         * For more help, check https://xhr.spec.whatwg.org/.
         *
         * This means that ajax will set async to false and fetch the javascript code refered by script tag synchronously.
         * Since we want the javascript to be loaded first, then the html, this issue is unavoidable.
         */
        echo    '<script src="js/provision.js"></script>
                <h3>Provision Customer</h3>
                <div>
                    <label for="pPassword">Password :</label>
                    <input id="pPassword" type="password">
                    <label for="pCustomerName">Customer name :</label>
                    <input id="pCustomerName" type="text">
                    <label for="pNewCustomer">New customer :</label>
                    <select id="pNewCustomer">
                        <option selected="selected" value="Y">
                            YES
                        </option>
                        <option selected="selected" value="N">
                            NO
                        </option>
                    </select>
                    <label for="pNoOfTags">Number of tags to provision :</label>
                    <input id="pNoOfTags" type="text">
                    <label for="pExtraTags">Extra tags needed :</label>
                    <select id="pExtraTags">
                        <option selected="selected" value="Y">
                            YES
                        </option>
                        <option selected="selected" value="N">
                            NO
                        </option>
                    </select>
                    <label for="pNoOfExtraTags">Number of extra tags :</label>
                    <input id="pNoOfExtraTags" type="text">
                    <label for="pPrimaryITag">Primary ITAG :</label>
                    <input id="pPrimaryITag" type="text">
                    <label for="pSecondaryITag">Secondary ITAG :</label>
                    <input id="pSecondaryITag" type="text">
                    <input id="pSubmit" type="button" value="Submit">
                </div>';
    } else {
        http_response_code(400);
        echo "Invalid username and/or password.";
    }

    //Log the activity here
    $activityData = [
        'ActionType'  => ActivityLog::ACTION_TYPE_OTP_GEN,
        'RequestBody' => $_POST,
        'MetaData'    => [
            'Operation' => ActivityLog::OPERATION_CUSTOMER_PROVISIONING_ADMIN_LOGIN_GET
        ]
    ];
    ActivityLog::log($activityData);

    exit(0);
});
//Admin customer provisioning HTML ends

//LATEST_SERVER_VERSION
$app->get('/:version/server/info', function($version) use($app) {
    echo LATEST_SERVER_VERSION;
});

/**
 * User comments
 * url - /comments
 * method - POST
 * params - name, email, phone, message
 */
$app->post('/:version/comments', 'appkey_authenticate', function($version) use ($app) {
    // check for required params
    verifyRequiredParams(array('name', 'email', 'message'));

    $response = array();

    // reading post params
    $name = $app->request->post('name');
    $email = $app->request->post('email');
    $comments = $app->request->post('message');

    authRequiredParams(array($name, $email));

    // Sanitize and validate the data passed in
    $username = filter_var($name, FILTER_SANITIZE_STRING);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $comments = filter_var($comments, FILTER_SANITIZE_STRING);

    // validating email address
    validateEmail($email);

    if(strlen($comments) > 8196)
    {
        $response["error"] = true;
        $response["message"] = "Message exceeds character limit.";
        echoResponse(200, $response);
    }

    // PPADD
    $mysqli = getGlobalDBHandle(); //new mysqli(HOST, USER, PASSWORD, DATABASE);
    if ($mysqli->connect_error) {
        $errormsg = "Unable to connect to MySQL";
        $response["error"] = true;
        $response["message"] = $errormsg;
        $response["src"] = 'post /comments';
        echoResponse(500, $response);
    }

    $inst_stmt = $mysqli->prepare("INSERT INTO comments (Name, Email, Comments, CreatedDateTime) VALUES (?,?,?, now())");

    if ($inst_stmt) {

        $inst_stmt->bind_param('sss', $name, $email, $comments);
        // Execute the prepared query.
        if (! $inst_stmt->execute()) {
            $inst_stmt->close() ;
            $errormsg = "Comment insert error";
            $response["error"] = true;
            $response["message"] = $errormsg;
            $response["src"] = 'post /comments';
            echoResponse(500, $response);
        }
    }
    else
    {
        $inst_stmt->close() ;
        $errormsg = "Comment insert stmt error";
        $response["error"] = true;
        $response["message"] = $errormsg;
        $response["src"] = 'post /comments';
        echoResponse(500, $response);
    }
    $inst_stmt->close() ;


    //Send email

    $useremail = ZESTLEMAIL;
    $headers   = "From: ".ZESTLEMAIL."\n";
    $headers  .= "Reply-to: " . ZESTL_REPLY_TO_EMAIL . "\r\n";
    $subject   = "TwigMe - New comment.";

    $message = "Dear Twigme Owner,\n\n";
    $message .= "You received a new comment from: ".$name."\n\n";
    $message .= $comments."\n\n";
    $message .= "Info: ".$email."\n";

    $result = mail($useremail, $subject, $message, $headers, "-fhardik@zestl.com");

    $response["error"] = false;
    $response["showmessage"] = true;
    $response["message"] = "Feedback has been sent successfully";

    //Log the activity here
    $activityData = [
        'ActionType'  => ActivityLog::ACTION_TYPE_INSERT,
        'RequestBody' => $app->request->post(),
        'MetaData'    => [
            'Operation' => ActivityLog::OPERATION_TWIGME_FEEDBACK_ADD
        ]
    ];
    ActivityLog::log($activityData);

    echoResponse(200, $response);
});



/**
 * User Registration
 * url - /user/register
 * method - POST
 * params - name, email, password
 * Password needs to be SHA512 encrypted - same as in the JS hex_sha512 function to match web api.
 */
$app->post('/:version/user/register', 'ENABLE_TRANSACTION', 'appkey_authenticate', function($version) use ($app) {
    try {
        // check for required params
        // verifyRequiredParams(array('name', 'email', 'password'));
        verifyRequiredParams(array('name', 'password'));
        // Either email or phone number will be there

        $response = array();

        // reading post params
        $name = trim($app->request->post('name'));
        $password = trim($app->request->post('password'));

        // Validation
        // Check if the API request has at least one of email or phone passed
        if (!isset($_REQUEST['phone']) && !isset($_REQUEST['email'])) {
            throw new TwigException("Neither Phone number nor Email detected in registration");
        }

        // Set the userIdentifier based on phone or email passed
        if(isset($_REQUEST['phone'])) {
            $phone = trim($app->request->post('phone'));
            if(!isValidPhone($phone)) {
                throw new TwigException("Please enter 10 digit Phone Number only.");
            }
            $userIdentifier = $phone;
        } else if (isset($_REQUEST['email'])) {
            $email = trim($app->request->post('email'));
            // validating email address
            // Setting userIdentifier should be done here only, since
            // the EmailID can have capital characters, and APWD computed
            // would be on that exact EMail that was typed in.
            $userIdentifier = $email;
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            $email = strtolower($email);
            if(!isValidEmail($email)) {
                throw new TwigException("Invalid EmailID passed.");
            }
        }

        authRequiredParams(array($name, $userIdentifier, $password));

        // Sanitize and validate the data passed in
        $username = filter_var($name, FILTER_SANITIZE_STRING);
        $password = filter_var($password, FILTER_SANITIZE_STRING);

        $statusCode = 200;
        $response = [];

        // <editor-fold>

        // HG 11/6/15
        $context = $app->request->post('context');
        $context = filter_var($context, FILTER_SANITIZE_STRING);
        if ($context == "") $context = "Self registration";

        $mysqli = getGlobalDBHandle();

        list($success, $userid) = getUserID($userIdentifier, $mysqli);
        if ($success == true) {
            $userRow = \TwigMeServerApplication\users\UserList::findUserFromCache($userid, $mysqli);
            if($userRow->EmailVerified != 'YES' && $userRow->PhoneVerified != 'YES') {
                $response["message"] = "User exists but not activated";
                $response["error_code"] = isValidPhone($userIdentifier)?ErrorCode::USER_INACTIVE_PHONE:ErrorCode::USER_INACTIVE;
            } else {
                $response["message"] = "User already exists, please login";
            }
            // user exists
            $response["error"] = true;
            $statusCode = 409;
        } else {
            // HG 11/6/15
            // As part of changes to auto-register user, the two step verification instead changes into
            // sending a password reset link instead.
            // PP UnCommented : 04-11-2015
            // The registerUser function is 'uncommented' so that user is created immediately
            // The user entry is made in the UserList (& Summary) tables,
            // however, the activation flag is set to INACTIVE initially,
            // But the clicking on the activation link would set this to ACTIVE
            // There are three different values to this flag:
            // 1. ACTIVE : Set when user clicks on the activation link
            // 2. INACTIVE: Whenever the user entry is created by registration
            //    through the app, or by a different person (librarian for his user)
            // 3. INVALID: When a user clicks on report abuse link send when creation of
            //    user is done
            list($success, $errormsg) = registerUser($username, $userIdentifier, $password, $mysqli, NULL /*emailMsg*/, ($context == "Self registration") /*createdBySelf*/);

            /* Following is being replaced by above function
                $db = new DbHandler();
                $res = $db->createUser($name, $email, $password);
                */

            if ($success == true) {
                $response["error"] = false;
                //$response["message"] = "You are successfully registered";
                if(isValidEmail($userIdentifier)) {
                    $response["message"] = "Account activation link sent to " . $userIdentifier . ". Please click on the link to complete the registration process.";
                } else {
                    $response["message"] = "Please click on Send OTP to complete the registration process.";
                }
                // $response["link"] = $link;
                $statusCode = 201;
            } else {
                $response["error"] = true;
                $response["message"] = $errormsg;
                $statusCode = 200;
            }
        }

        // </editor-fold>

        //Log the activity here
        $activityData = [
            'ActionType' => ActivityLog::ACTION_TYPE_INSERT,
            'RequestBody' => $app->request->post(),
            'MetaData' => [
                'Operation' => ActivityLog::OPERATION_TWIGME_USER_ADD
            ]
        ];
        ActivityLog::log($activityData);
    } catch (TwigException $tex) {
        \Slim\Slim::getInstance()->TwigMeLogger->logException($tex);
        $statusCode = 200;
        $response["error"] = true;
        $response["message"] = $tex->getMessage();
    } catch (Exception $ex) {
        \Slim\Slim::getInstance()->TwigMeLogger->logException($ex);
        $statusCode = 200;
        $response["error"] = true;
        $response["message"] = "Something went wrong, please contact TwigME support.";
    }
    echoResponse($statusCode, $response);
});

// <editor-fold>

//$app->get('/:version/test/dynamo', function($version) use($app) {
//    //This is a test API. comment following two lines when testing
//    $object = [
//        'a' =>  'alpha',
//        'b' =>  123,
//        'c' =>  ['beta', 'gamma'],
//        'd' =>  [1,2,3],
//        'e' =>  [
//            'e_1_key'   =>  'e_1_value',
//            'e_e_key'   =>  ['e_2_value_1','e_2_value_2']
//        ]
//    ];
//
//    list($updateExpression, $expressionAttributeValue) = \TwigMeServerApplication\DynamoDB\DynamoDBController::createUpdateExpression($object);
//    $response['updateExpression'] = $updateExpression;
//    $response['expressionAttributeValue'] = $expressionAttributeValue;
//    echoResponse(200, $response);
//});

//$app->get('/:version/push/dynamo', function($version) use($app) {
//    //This is a test API. comment following two lines when testing
//    echo "Invalid API";
//    exit();
//    try {
//        $client = \Aws\DynamoDb\DynamoDbClient::factory(array(
//            'credentials' => array(
//                'key' => "AKIAJ6PPKXTACNKREBQQ",
//                'secret' => "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",
//                ),
//                'version' => 'latest',
//                'region' => S3REGION
//            ));
//
///*            $result = $client->putItem([
//                'TableName' => 'UserLT',
//                'Item' => [
//                    'userid'       => ['N'      => '2'      ], // Primary Key
//                    'lt'    =>
//                        ['L'      =>
//                            [
//                                ['M'      =>    ["timestamp1" => ['S' => "3.435345345, 32.234335345"]]],
//                                ['M'      =>    ["timestamp2" => ['S' => "23.435345345, 3.234335345"]]],
//                            ]
//                        ],
//                    //'Price'    => ['N'      => '25' ],
//                    'favs'  => ['NS'  => ["1235", "1236"] ]
//                ]
//            ]);*/
//
//            $result = $client->updateItem([
//                'TableName' => 'UserLT',
//                'Key' => [
//                    'userid' => [ 'N' => '2' ]
//                ],
//                'ExpressionAttributeValues' =>  [
//                    ':val1' => [
//                        ['M'      =>    ["timestamp1" => ['S' => "56.435345345, -2.234335345"]]],
//                    ]
//                ] ,
//                'UpdateExpression' => 'set lt = list_append(lt, :val1) remove favs[0], lt[0]'
//            ]);
//
//
//            /*            $newObj["userid"] = 2;
//                        $gpsData1 = new stdClass();
//                        $gpsData1->timestampA = "coordinateA";
//                        $gpsData2 = new stdClass();
//                        $gpsData2->timestampB = "coordinateB";
//
//                        $newObj["lt"][] = $gpsData1;
//                        $newObj["lt"][] = $gpsData2;
//
//                        $newObj["favs"][] = "Tag1";
//                        $newObj["favs"][] = "Tag2";
//
//                        $marshaler = new \Aws\DynamoDb\Marshaler();
//                        // echo $marshaler->marshalJson(json_encode($newObj));
//
//                        /*$client->putItem([
//                            'TableName' => 'UserLT',
//                            'Item'      => $marshaler->marshalJson(json_encode($newObj))
//                        ]);*/
//
//            // $result = $client->listTables();
//        } catch (\Aws\Exception\AwsException $ex) {
//            $result["success"] = false;
//        } finally {
//            echoResponse(200, $result);
//        }
//
//    });

$app->get('/products/search', function() use($app) {
    // Initialize Page Size
    $pageSize = 5;

    // Get the URL params searchtext and page
    $searchtext = Utils::getFilterParam("searchtext", false /*decodeJson*/);
    $page = Utils::getFilterParam("page", false /*decodeJson*/);
    if(empty($searchtext)) {
        $searchtext = '';
    } else {
        $searchtext = urldecode($searchtext);
    }
    // If the page is not provided, or is not the integer, set to 0
    if(empty($page) || (filter_var($page, FILTER_VALIDATE_INT) === FALSE)) {
        $page = 0;
    }

//        `JF_Items` :
//          `ItemID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//          `URLID` varchar(512) NOT NULL,
//          `Title` varchar(512) NOT NULL,
//          `ItemDescription` TEXT NOT NULL,
//          `Price` DECIMAL(15,2),
//          `ImageUrl` varchar(2048) DEFAULT NULL,
//          `WeightInGms`  DECIMAL(15,2),
//          `ItemGSTPercent`    DECIMAL(15,2),
//          `IsActive`     ENUM('YES', 'NO') DEFAULT 'YES',

    $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
    $qry = "SELECT * from JF_Items WHERE Title LIKE ?  ORDER BY Title ASC LIMIT ? OFFSET ?";
    $itemRows = fetchRows($dbConn, $qry, "sii", ["%$searchtext%", $pageSize, $pageSize*$page]);

    if(empty($itemRows)) {
        $itemRows = [];
    }

    $products = [];

    foreach ($itemRows as $itemRow) {
        $prodEntry = new stdClass();
        $prodEntry->id = $itemRow->ItemID;
        $prodEntry->urlid = $itemRow->URLID;
        $prodEntry->title = $itemRow->Title;
        $prodEntry->desc = $itemRow->ItemDescription;
        $prodEntry->cost = $itemRow->Price;
        $prodEntry->img = $itemRow->ImageUrl;
        // TODO : Fix this asap
        $prodEntry->qty = 10;

        $products[] = $prodEntry;
    }

    $resp['error'] = false;
    $data = new stdClass();
    $data->products = $products;
    if(sizeof($products) == $pageSize) {
        $data->more = true;
    }

    $resp['data'] = $data;
    echoResponse(200, $resp);
});

$app->get('/product/:productID', function($productID) use($app) {
    //        `JF_Items` :
//          `ItemID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//          `URLID` varchar(512) NOT NULL,
//          `Title` varchar(512) NOT NULL,
//          `ItemDescription` TEXT NOT NULL,
//          `Price` DECIMAL(15,2),
//          `ImageUrl` varchar(2048) DEFAULT NULL,
//          `WeightInGms`  DECIMAL(15,2),
//          `ItemGSTPercent`    DECIMAL(15,2),
//          `IsActive`     ENUM('YES', 'NO') DEFAULT 'YES',
    try {
        $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
        if(empty($productID)) {
            throw new TwigException("No such product exist");
        } else if (ctype_digit($productID)) {
            $qry = "SELECT * from JF_Items WHERE ItemID = ?";
            $itemRow = fetchSingleRowLenient($dbConn, $qry, "i", [$productID]);
        } else {
            $qry = "SELECT * from JF_Items WHERE URLID = ?";
            $itemRow = fetchSingleRowLenient($dbConn, $qry, "s", [$productID]);
        }

        if (empty($itemRow)) {
            throw new TwigException("No such product exist");
        }

        $prodEntry = new stdClass();
        $prodEntry->id = $itemRow->ItemID;
        $prodEntry->urlid = $itemRow->URLID;
        $prodEntry->title = $itemRow->Title;
        $prodEntry->desc = $itemRow->ItemDescription;
        $prodEntry->cost = $itemRow->Price;
        $prodEntry->img = $itemRow->ImageUrl;
        // TODO : Fix this asap
        $prodEntry->qty = 10;

        $data = new stdClass();
        $data->product = $prodEntry;

        $resp['error'] = false;
        $resp['data'] = $data;
        $resp['message'] = '';
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});


function checkAuthentication() {
    $headers = \TwigMeServerApplication\utility\HeaderUtils::getHeaders();

    if(isset($headers['authorization'])) {
        // Check if the JWT Token is valid
        $jwToken = $headers['authorization'];
        if (\TwigMeServerApplication\login\jwt\JWTUtils::isJWTValid($jwToken, $userIDJWT)) {
            Utils::setUserLoggedIn($userIDJWT);
            return true;
        }
    }
    return false;
}

// PhoneOTPInfo looks like this
// {
//      "user_activate" : {
//          "otp" : XXXXXX,
//          "valid_upto" : 19234234324
//          "latest_otp_sent_time" : 19234234324
//      },
//      "reset_password" : {
//          "otp" : XXXXXX,
//          "valid_upto" : 19234234326
//      },
//      "stats" : {
//          "YYYY-MM-DD" : {
//              "user_activate" : {
//                  "otp_count" : 23        // Total OTPs sent for this requestType on this date
//              },
//              "reset_password" : {
//                  "otp_count" : 23
//              },
//          },
//          "total_otp_sent" : 135
//      }
// }
function getOTPObject($requestType, $userDetails, &$otp, &$validityUpto) {
    $MAX_OTP_PER_DAY_PER_REQUEST = 10;

    $generateNewOTP = false; $ts = time();
    $date = new \DateTime("@$ts");
    $date->setTimezone(new \DateTimeZone(\Constants::TZ_INDIA));
    $dateStr = $date->format("Y-m-d");

    if(is_null($userDetails) || empty($userDetails->PhoneOTPInfo)) {
        $generateNewOTP = true;
        // No entry exists in DB
        $otpObj = [];
    } else {
        $otpStr = $userDetails->PhoneOTPInfo;
        $otpObj = json_decode($otpStr, true);
        if(isset($otpObj[$requestType]["otp"]) && isset($otpObj[$requestType]["valid_upto"])) {
            // An entry for this date exists in OTP Info, which means some OTP were also sent earlier
            $otp = $otpObj[$requestType]["otp"];
            $validityUpto = $otpObj[$requestType]["valid_upto"];

            // Check if the resend-OTP request is within the Valid Upto window OR not
            if($ts > $validityUpto) {
                // Old OTP has expired
                $generateNewOTP = true;
            } else {
                // OTP is going to be reused
                $latest_otp_sent_time = $otpObj[$requestType]["latest_otp_sent_time"];
                // Check when the last OTP was sent, and if it was sent in last 10 seconds or so, throw ex
                if(time() - $latest_otp_sent_time <= 10) {
                    throw new TwigException("OTP has just been sent. You'll need to wait 10 seconds to request re-sending the OTP");
                } else {
                    // This means same OTP is going to be actually sent out now via the SMS
                    $otpObj[$requestType]["latest_otp_sent_time"] = time();
                }
            }
            // Check if the #OTPs sent today have not breached the limit
            if(isset($otpObj["stats"][$dateStr][$requestType]["otp_count"])) {
                $otpCountForRequestToday = $otpObj["stats"][$dateStr][$requestType]["otp_count"];
                if($otpCountForRequestToday > $MAX_OTP_PER_DAY_PER_REQUEST) {
                    // Old OTP has expired
                    throw new \TwigException("You've exceeded daily SMS limit for this request, Please contact TwigMe support.");
                }
            }

        } else {
            $generateNewOTP = true;
        }
    }

    // Total OTP sent count till date
    $totalOTPSentTillDate = isset($otpObj["stats"]["total_otp_sent"])?$otpObj["stats"]["total_otp_sent"]:0;

    // Create the otpObj
    if($generateNewOTP) {
        $otp = getOTP(6 /*6 digit*/);
        $validityUpto = time() + 300; // Valid for 5 minutes
        // Now set the new OTP
        $otpObj[$requestType]["otp"] = $otp;
        $otpObj[$requestType]["valid_upto"] = $validityUpto;
        $otpObj[$requestType]["latest_otp_sent_time"] = time();
    }

    // Check if the current date exists, if not, clear up stats entirely,
    // so that older date is cleaned with it
    if(!isset($otpObj["stats"][$dateStr])) {
        // Save this in some thing
        $otpObj["stats"] = [];
    }

    // Increase the OTP count for the date of the request type
    $currentOTPCount = isset($otpObj["stats"][$dateStr][$requestType]["otp_count"])?$otpObj["stats"][$dateStr][$requestType]["otp_count"]:0;
    $otpObj["stats"][$dateStr][$requestType]["otp_count"] = $currentOTPCount + 1;

    // Update the Stats of Total OTP Sent for this User Till date
    $otpObj["stats"]["total_otp_sent"] = $totalOTPSentTillDate + 1;

    return $otpObj;
}

function getOTP($numDigits = 4) {
    if($numDigits == 4) {
        $bytes = 2;
    } else if ($numDigits == 6) {
        $bytes = 3;
    } else {
        throw new \TwigException("Invalid Number of Digits [$numDigits] requested for OTP");
    }

    $otp_code = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
    if($numDigits == 4) {
        $otp_code = bcmod($otp_code, 10000);
        $otp_code = Utils::get4Digit($otp_code);
    }
    if($numDigits == 6) {
        $otp_code = bcmod($otp_code, 1000000);
        if ($otp_code < 100000 && $otp_code > 9999) {
            $otp_code = "0" . $otp_code;
        } else if ($otp_code < 10000 && $otp_code > 999) {
            $otp_code = "00" . $otp_code;
        } else if ($otp_code < 1000 && $otp_code > 99) {
            $otp_code = "000" . $otp_code;
        } else if ($otp_code < 100 && $otp_code > 9) {
            $otp_code = "0000" . $otp_code;
        } else if ($otp_code < 10 && $otp_code > 0) {
            $otp_code = "00000" . $otp_code;
        } else if ($otp_code == 0) {
            $otp_code = "000000";
        }
    }
    return "" . $otp_code;
}

// POST /jg/generateotp
// Generates OTP
// TODO : Use Captcha
$app->post('/jg/generateotp', function() use($app) {

    try {
        $requestBody = getRequestParams();

        if(!isset($requestBody['username'])) {
            throw new TwigException("Mobile number not sent!");
        }
        $mobileNo = trim($requestBody['username']);
        if(!isValidPhone($mobileNo)) {
            throw new TwigException("Mobile number not valid!");
        }

        Cache::enableTransaction();

        $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
        // Check if this phone number exists in the JF_UserList
        $qry = "SELECT * from JF_UserList WHERE UserPhone = ?";
        $userRow = fetchSingleRowLenient($dbConn, $qry, "s", [$mobileNo]);

        // Fetch the OTP and validity
        $otpObj = getOTPObject("LOGIN_OTP", $userRow, $otp, $validityUpto);
        // Create OTP Info
        $otpObjStr = json_encode($otpObj);
        // Validity in Y-m-d H:i:s
        $otpExpiry = (new DateTime("@$validityUpto"))->format('Y-m-d H:i:s');

        // Update the OTP & Validity Upto for the provided phoneNumber in UserList
        if (!empty($userRow)) {
            // Entry already exists, just update the User OTP
            $updateOTPQry = 'UPDATE JF_UserList set PhoneOTPInfo = ? WHERE UserPhone = ?';
            updateRow($dbConn, $updateOTPQry, "ss", [$otpObjStr, $mobileNo]);
        } else {
            // Entry does not exists, Create an entry
            $insertUserQry = 'INSERT INTO JF_UserList (UserPhone, PhoneOTPInfo) VALUES (?,?)';
            updateRow($dbConn, $insertUserQry, "ss", [$mobileNo, $otpObjStr]);
        }

        Cache::commitTransaction();
        // Now sent the SMS
        \TwigMeServerApplication\phoneotp\SMSController::sendSMS([$mobileNo], "$otp is your OTP (One Time Password)", "");

        $resp['error'] = false;
        $resp['message'] = 'Successsfully Sent OTP';
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});



// POST jg/login
$app->post('/jg/login', function() use($app) {
    try {
        $requestBody = getRequestParams();
        $mobileNo = $requestBody['username'];
        $otp = $requestBody['password'];

        $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
        // Store this in the JF_UserList
        $qry = "SELECT * from JF_UserList WHERE UserPhone = ?";
        $userRow = fetchSingleRowLenient($dbConn, $qry, "s", [$mobileNo]);
        if(!empty($userRow)) {
            // Entry already exists
            $otpInfoStr = $userRow->PhoneOTPInfo;
            $phoneOTPObj = json_decode($otpInfoStr, true);
            $requestType = "LOGIN_OTP";
            if(isset($phoneOTPObj[$requestType]["otp"]) && isset($phoneOTPObj[$requestType]["valid_upto"])) {
                $otpCurrent = $phoneOTPObj[$requestType]["otp"];
                $validUpto = $phoneOTPObj[$requestType]["valid_upto"];

                if($otpCurrent == $otp) {
                    // Passed OTP matches that in the system
                    if ($validUpto <= time()) {
                        throw new TwigException("OTP has elapsed. Click on 'Send OTP', after which you'll get a new One Time Password by SMS");
                    }
                    // Came here means the OTP is valid!
                } else {
                    // OTP does not match
                    throw new TwigException("Invalid OTP. Please enter correct OTP, or if you haven't received an OTP yet, click on 'Sent OTP'");
                }
            } else {
                // Should not ideally come here!
                throw new TwigException("You need to generate OTP before login. Click on 'Send OTP', after which you'll get an One Time Password by SMS");
            }
        } else {
            // Entry does not exists,
            throw new TwigException("You need to generate OTP before login. Click on 'Send OTP', after which you'll get an One Time Password by SMS");
        }

        // Came here means the Login has succeeded
        $authorization = \TwigMeServerApplication\login\jwt\JWTUtils::getJWTForUser($userRow->UserID);
        // UserName :
        $userName = $userRow->UserName;

        $data = new stdClass();
        $data->authorization = $authorization;
        $data->username = !empty($userName)?$userName:$mobileNo;

        $resp['data'] = $data;
        $resp['error'] = false;
        $resp['message'] = 'Login Successful!';
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});

// POST /cart/checkout
// This would be called once the address has been set
$app->post('/cart/checkout', function() use($app) {
	$RAZORPAYKEY = 'rzp_live_HmfDi0dXerLzgK';
	$RAZORPAYSECRET = 'xSxH6qOYbv4sNecSqLWhUs0F';
    try {
        if (checkAuthentication()) {
            $userID = getLoggedUserID();
            // Construct the Cart, along with the items
            $requestBody    = getRequestParams();
            $userNAddressInfo   = $requestBody['userinfo'];
            $cart           = $requestBody['cart'];
//            On the JS Side
//            cart.cart_products[prodID] = {
//                'Quantity' : prodQuantity,
//                'CostPerUnit' : prodCostPerUnit,
//                'ItemName' : prodName,
//                'ID' : prodID,
//                'URLID' : prodUrlID
//            };

            if(!is_array($cart) || empty($cart)) {
                throw new TwigException("The cart cannot be empty on check-out");
            }
            // Check if Cart is properly structured
            foreach ($cart as $itemID => $itemInfo) {
                if(!is_int($itemID) || !array_key_exists('Quantity', $itemInfo) || !is_integer($itemInfo['Quantity']) || ($itemInfo['Quantity'] < 1)) {
                    throw new TwigException("ItemID should be Integer, and Quantity should be present in Item Listing sent in Cart");
                }
            }

            // Check if Address is correctly strucutred
            //            UserInfo has following fields
            //            name, addressline1, addressline2, landmark, city, state, pincode, mobile, email
            \TwigMeServerApplication\utility\ValidatorUtils::validate(
                $userNAddressInfo,
                [
                    [ 'column' => 'name',  'validator' => 'strrange', 'params' => [ 'min' => 2, 'max' => 100], 'message' => 'Name should be between 2 and 100 characters'],
                    [ 'column' => 'addressline1',  'validator' => 'strrange', 'params' => [  'min' => 2, 'max' => 500], 'message' => 'Address Line 1 should be between 2 and 500 characters'],
                    [ 'column' => 'addressline2',  'validator' => 'strrange', 'params' => [ 'max' => 500], 'message' => 'Address Line 2 should be less than 500 characters'],
                    [ 'column' => 'landmark',  'validator' => 'strrange', 'params' => [ 'max' => 250], 'message' => 'Landmark should be less than 250 characters'],
                    [ 'column' => 'city',  'validator' => 'strrange', 'params' => [ 'min' => 2, 'max' => 100], 'message' => 'City should be between 2 and 100 characters'],
                    [ 'column' => 'state',  'validator' => 'strrange', 'params' => [ 'min' => 2, 'max' => 100], 'message' => 'State should be between 2 and 100 characters'],
                    [ 'column' => 'pincode', 'validator' => 'regex', 'params' => [ 'regex' => '/^[0-9][0-9]{5}$/', 'nullable' => false], 'message' => 'Pincode should be of 6 numbers'],
                    [ 'column' => 'mobile', 'validator' => 'regex', 'params' => [ 'regex' => '/^[6-9][0-9]{9}$/', 'nullable' => false], 'message' => 'Invalid Mobile Number sent'],
                    [ 'column' => 'email',  'validator' => 'emailid', 'params' => [], 'message' => 'Please enter a valid Email-ID'],
                ]
            );

//              ITEMS DB Schema
//            `ItemID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `URLID` varchar(512) NOT NULL,
//            `Title` varchar(512) NOT NULL,
//            `ItemDescription` TEXT NOT NULL,
//            `Price` DECIMAL(15,2),
//            `ImageUrl` varchar(2048) DEFAULT NULL,
//            `WeightInGms`  DECIMAL(15,2),
//            `ItemGSTPercent`    DECIMAL(15,2),
//            `IsActive`     ENUM('YES', 'NO') DEFAULT 'YES',

            // Total cost of cart, inclusive of GST Tax
            $cartCostIncTax = 0;
            // Total weight of cart (in grams)
            $cartWeightInGms = 0;
            // Item ID => Item Quantity Pair
            $cartItemIDQuantityArr = [];

            Cache::enableTransaction();

            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
            // An inventory check : Don't sell more than 50K worth products in a day
            $ts = time();
            $todayYmdStart = (new DateTime("@$ts"))->format("Y-m-d 00:00:00");
            $todayYmdEnd = (new DateTime("@$ts"))->format("Y-m-d 23:59:59");
            // The total sold value on a day should not exceed 50K
            $qryTotalSold = "SELECT SUM(CostIncTax) As TotalSoldSum FROM JF_UserCart WHERE PaymentStatus = ? AND ModifiedOn >= ? AND ModifiedOn <= ?";
            $totalSoldSumRow = fetchSingleRowStrict($dbConn, $qryTotalSold, "sss", ['PAID', $todayYmdStart, $todayYmdEnd]);
            if($totalSoldSumRow->TotalSoldSum > 50000) {
                // Send sms to admin
                \TwigMeServerApplication\phoneotp\SMSController::sendSMS([9975385693], "Max day limit reached for " . $userNAddressInfo['mobile'], "");
                throw new TwigException("Max products for today have been sold, please make this payment tomorrow");
            }

            // Check if all the items IDs which are sent in the cart exists in the 'Items' table, and if they are
            // active
            $qry = 'SELECT * from JF_Items where ItemID = ?';
            foreach ($cart as $itemID => $itemInfo) {
                $itemDBRow = fetchSingleRowLenient($dbConn, $qry, "i", [$itemID]);
                if(empty($itemDBRow)) {
                    // No such entry exists in DB
                    throw new TwigException("No Product exists for ProductID [$itemID]");
                }
                // The Item exists, update cost and weight of cart
                $itemQuantity = $itemInfo['Quantity'];
                $pricePerUnit = $itemDBRow->Price;
                $weightInGmsPerUnit = $itemDBRow->WeightInGms;
                $gstTaxPercent = $itemDBRow->ItemGSTPercent;
                // Cost inclusive of Tax := Quantity X [ Price Per Unit X [ 1 + Tax-Percentage ] ]
                $cartCostIncTax += ($itemQuantity*($pricePerUnit*(1 + ($gstTaxPercent/100))));
                // Update Weight of Cart
                $cartWeightInGms += ($itemQuantity*$weightInGmsPerUnit);
                // Update cart ItemID to Quantity Array
                $cartItemIDQuantityArr[$itemID] = $itemQuantity;
            }


            if($cartCostIncTax > 10000) {
                throw new TwigException("You cannot buy items for more than 10000 INR, please reomve some items from the Cart");
            }

            // 1. Find out Cost Inc Tax, Shipping Cost, Order ID
            // 2. Store User Cart. Get CartID
            // 3. Create an order in the Razor Pay
            // 4. Update the Cart Order ID using orderID in (3)
            // 5. Store Cart Items
            // 6. Return back the Cart ID

            // 1. Find out Cost Inc Tax, Shipping Cost, Order ID
            $shippingCost = 0;
            if($cartWeightInGms < 500) {
                $shippingCost = 0;
            } else if($cartWeightInGms < 1000) {
                $shippingCost = 50;
            } else if ($cartWeightInGms < 2000) {
                $shippingCost = 100;
            } else { // if ($cartWeightInGms >= 2000) {
                // 50 Rs per 1000 grams
                $shippingCost = ($cartWeightInGms/1000)*50;
            }

            // 2. Store the User Cart
//                `CartID`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
//                `UserID`            INT UNSIGNED NOT NULL,
//                `PaymentStatus`     ENUM('STARTED', 'PAID') DEFAULT 'STARTED',
//                `CostIncTax`        DECIMAL(15,2),
//                `ShippingCost`      DECIMAL(15,2),
//                `OrderID`           VARCHAR(1024) NOT NULL,
//                `Address`           TEXT DEFAULT NULL,
//                `CreatedOn`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
//                `ModifiedOn`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            $qryCreateCart = 'INSERT into JF_UserCart(UserID, CostIncTax, ShippingCost, OrderID, Address) VALUES (?,?,?,?,?)';
            // Initialize the Order ID as an empty string, until we get the CartID
            insertRow($dbConn, $qryCreateCart, "iddss", [$userID, $cartCostIncTax, $shippingCost, "" /*OrderID*/, json_encode($userNAddressInfo)]);
            $cartID = mysqli_insert_id($dbConn);

            // 3. Rzp Order ID
            $api = new \Razorpay\Api\Api($RAZORPAYKEY, $RAZORPAYSECRET);
            $order  = $api->order->create(array('receipt' => "$cartID", 'amount' => $cartCostIncTax*100, 'currency' => 'INR', 'payment_capture' => true, 'notes' => $cartItemIDQuantityArr));
            $orderID = $order->id;

            // 4. Update the OrderID in Cart
            $qryUpdateOrderIDCart = "UPDATE JF_UserCart SET OrderID = ? WHERE CartID = ?";
            updateRow($dbConn, $qryUpdateOrderIDCart, "si", [$orderID, $cartID]);

            // 5. Now add the entries in the User Cart Items

//                `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
//                `CartID`            INT UNSIGNED NOT NULL,
//                `ItemID`            INT UNSIGNED NOT NULL,
//                `ItemTitle`         TEXT DEFAULT NULL,
//                `ItemQuantity`      INT UNSIGNED NOT NULL,
//                `ItemPricePerUnit`  DECIMAL(15,2),
//                `ItemGSTPercent`    DECIMAL(15,2),
//                `ItemTaxPerUnit`    DECIMAL(15,2),

            $qryInsertCartItem = "INSERT INTO JF_UserCartItems(CartID, ItemID, ItemTitle, ItemQuantity, ItemPricePerUnit, ItemGSTPercent, ItemTaxPerUnit) VALUES (?,?,?,?,?,?,?)";
            foreach ($cart as $itemID => $itemInfo) {
                $itemDBRow = fetchSingleRowStrict($dbConn, $qry, "i", [$itemID]);
                // Find all Item Attributes
                $itemQuantity = $itemInfo['Quantity'];
                $itemTitle = $itemDBRow->Title;
                $pricePerUnit = $itemDBRow->Price;
                $gstTaxPercent = $itemDBRow->ItemGSTPercent;
                $taxPerUnit = ($gstTaxPercent/100)*$pricePerUnit;
                // Insert UserCartItem entry
                insertRow($dbConn, $qryInsertCartItem, "iisiddd", [$cartID, $itemID, $itemTitle, $itemQuantity, $pricePerUnit, $gstTaxPercent, $taxPerUnit]);
            }

            Cache::commitTransaction();

            // 6. Return back the Cart ID
            $data = new stdClass();
            $data->checkoutid = $cartID;

            $resp['error'] = false;
            $resp['data'] = $data;
            $resp['message'] = 'Successfully added cart';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(200);
            $app->response()->status(200);
        } else {
            $resp['error'] = true;
            $resp['message'] = 'The User needs to be logged-in to perform this operation';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(401);
            $app->response()->status(401);
        }
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});


//      This is the result
//      {
//          'products' : [
//              {
//                  'id'    : 101,                // integer
//                  'mrp'   : 1499.0,             // float
//                  'units' : 3,                  // integer
//                  'title' : 'Teddy Bear (M)',   // String
//                  'tax_percentage' : 10.0,      // Float
//                  'tax_per_unit' : 149.0,       // float
//                  'total_tax' : 447.0           // float
//                  'total_incl_tax' : 4944.0     // float
//              }, ...
//          ],
//          'total_excl_tax' : 4497.0,            // float
//          'tax' : 447.0,                        // float
//          'total' : 4944.0                      // float
//          'delivery_address' : {
//              'line1' : '...',
//              'line2' : '...',
//              'city' : '...',
//          }
//      }
//
// GET /cart/:checkoutid
$app->get('/cart/:checkoutid', function($checkoutID) use($app) {

    try {
        if (checkAuthentication()) {
            $userID = getLoggedUserID();

            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
            // Confirm if the Cart that the logged in user is trying to access is something that it has created
            $qryFindCart = "SELECT * from JF_UserCart where CartID = ?";
            $cartDBRow = fetchSingleRowLenient($dbConn, $qryFindCart, "i", [$checkoutID]);
            if(empty($cartDBRow) || ($cartDBRow->UserID != $userID)) {
                throw new TwigException("Either Cart does not exist, or you don't have enough priviliges to view it");
            }

//            `CartID`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `UserID`            INT UNSIGNED NOT NULL,
//            `PaymentStatus`     ENUM('STARTED', 'PAID') DEFAULT 'STARTED',
//            `CostIncTax`        DECIMAL(15,2),
//            `ShippingCost`      DECIMAL(15,2),
//            `OrderID`           VARCHAR(1024) NOT NULL,
//            `Address`           TEXT DEFAULT NULL,

            $delivery_address = json_decode($cartDBRow->Address);

            // Now get each products one by one
            $qryFindCart = "SELECT JF_UserCartItems.*, JF_Items.URLID from JF_UserCartItems, JF_Items where JF_UserCartItems.ItemID = JF_Items.ItemID AND CartID = ?";
            $cartItemDBRows = fetchRows($dbConn, $qryFindCart, "i", [$checkoutID]);
//            JF_UserCartItems
//            `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `CartID`            INT UNSIGNED NOT NULL,
//            `ItemID`            INT UNSIGNED NOT NULL,
//            `ItemTitle`         TEXT DEFAULT NULL,
//            `ItemQuantity`      INT UNSIGNED NOT NULL,
//            `ItemPricePerUnit`  DECIMAL(15,2),
//            `ItemGSTPercent`    DECIMAL(15,2),
//            `ItemTaxPerUnit`    DECIMAL(15,2),
            if(empty($cartItemDBRows)) {
                $cartItemDBRows =[];
            }

            $shipping = $cartDBRow->ShippingCost;
            $products = [];
            $total_excl_tax = 0;
            $total_tax = 0;

            foreach ($cartItemDBRows as $cartItemDBRow) {
                $product = new stdClass();
                $product->id = $cartItemDBRow->ItemID;
                $product->urlid = $cartItemDBRow->URLID;
                $product->mrp = $cartItemDBRow->ItemPricePerUnit;
                $product->units = $cartItemDBRow->ItemQuantity;
                $product->title = $cartItemDBRow->ItemTitle;
                $product->tax_percentage = $cartItemDBRow->ItemGSTPercent;
                $product->tax_per_unit = $cartItemDBRow->ItemTaxPerUnit;

                $product_total_excl_tax = $cartItemDBRow->ItemQuantity*$cartItemDBRow->ItemPricePerUnit;
                $product_total_tax = $product_total_excl_tax*($cartItemDBRow->ItemGSTPercent/100);

                $product->total_tax = $product_total_tax;
                $product->total_incl_tax = $product_total_excl_tax + $product_total_tax;

                $total_excl_tax += $product_total_excl_tax;
                $total_tax += $product_total_tax;

                $products[] = $product;
            }

            // Create the response
            $data = new stdClass();
            $data->delivery_address = $delivery_address;
            $data->products = $products;
            $data->total_excl_tax = $total_excl_tax;
            $data->tax = $total_tax;
            $data->shipping = $shipping;
            $data->total = $total_excl_tax + $total_tax + $shipping;
            $data->rzp_orderid = $cartDBRow->OrderID;

            $resp['error'] = false;
            $resp['data'] = $data;
            $resp['message'] = 'Showing the User Cart';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(200);
            $app->response()->status(200);
        } else {
            $resp['error'] = true;
            $resp['message'] = 'The User needs to be logged-in to perform this operation';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(401);
            $app->response()->status(401);
        }
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }

});


// Called after Razor Pay payment is complete. Here we should store the payment
// information in the backend against the Cart
$app->post('/cart/payment/:checkoutid', function($checkoutID) use($app) {
	$RAZORPAYKEY = 'rzp_live_HmfDi0dXerLzgK';
	$RAZORPAYSECRET = 'xSxH6qOYbv4sNecSqLWhUs0F';
    try {
        if(checkAuthentication()) {
            $userID = getLoggedUserID();

            Cache::enableTransaction();

            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
            // Confirm if the Cart that the logged in user is trying to access is something that it has created
            $qryFindCart = "SELECT * from JF_UserCart where CartID = ?";
            $cartDBRow = fetchSingleRowLenient($dbConn, $qryFindCart, "i", [$checkoutID]);
            if(empty($cartDBRow) || ($cartDBRow->UserID != $userID)) {
                throw new TwigException("Either Cart does not exist, or you don't have enough privileges to view it");
            }

            $requestBody = getRequestParams();

            //    transaction :
            //
            //    {
            //        "razorpay_payment_id": "pay_xxxxxxxxxxxxxx",
            //        "razorpay_signature": "<Hexadecimal String>",
            //        “razorpay_order_id”: “order_xxxxxxxxxxxxxx”
            //    }
            $transaction = $requestBody['transaction'];

            $rzPaymentID = $transaction['razorpay_payment_id'];
            $rzOrderID = $transaction['razorpay_order_id'];

            if($rzOrderID != $cartDBRow->OrderID) {
                throw new \TwigException("Received Order ID does not match for the provided Cart");
            }

            $api = new \Razorpay\Api\Api($RAZORPAYKEY, $RAZORPAYSECRET);
            $payment = $api->payment->fetch($rzPaymentID);
            // Get the Order ID from the cart, and check its status
            if(empty($payment)) {
                throw new \TwigException("Payment was not successful, Aborting the process!");
            }
            if($payment->status !== "captured") {
                throw new \TwigException("Payment has not been done yet");
            }
            // Came here means the payment has been done!
            // Now also change the status of the Payment in User Cart from STARTED to PAID
            $qryUpdateCartPaymentStatus = "Update JF_UserCart set PaymentStatus = ? where CartID = ?";
            updateRow($dbConn, $qryUpdateCartPaymentStatus, "si", ["PAID", $checkoutID]);

            Cache::commitTransaction();

            // Now send the SMS
            \TwigMeServerApplication\phoneotp\SMSController::sendSMS([9975385693], "Order placed $rzOrderID", "");

            $data = new stdClass();
            $data->razorpay_payment_id = $rzPaymentID;

            $resp['error'] = false;
            $resp['data'] = $data;
            $resp['message'] = 'Payment successful';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(200);
            $app->response()->status(200);
        } else {
            $resp['error'] = true;
            $resp['message'] = 'The User needs to be logged-in to perform this operation';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(401);
            $app->response()->status(401);
        }
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});


//      This is the result
//      {
//          'products' : [
//              {
//                  'id'    : 101,                // integer
//                  'mrp'   : 1499.0,             // float
//                  'units' : 3,                  // integer
//                  'title' : 'Teddy Bear (M)',   // String
//                  'tax_percentage' : 10.0,      // Float
//                  'tax_per_unit' : 149.0,       // float
//                  'total_tax' : 447.0           // float
//                  'total_incl_tax' : 4944.0     // float
//              }, ...
//          ],
//          'total_excl_tax' : 4497.0,            // float
//          'tax' : 447.0,                        // float
//          'total' : 4944.0                      // float
//          'delivery_address' : {
//              'addressline1' : '...',
//              'addressline2' : '...',
//              'city' : '...',
//          }
//      }
//
// GET /orders/:checkoutid
$app->get('/orders/:checkoutid', function($checkoutID) use($app) {

    try {
        if (checkAuthentication()) {
            $userID = getLoggedUserID();

            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
            // Confirm if the Cart that the logged in user is trying to access is something that it has created
            $qryFindCart = "SELECT * from JF_UserCart where CartID = ?";
            $cartDBRow = fetchSingleRowLenient($dbConn, $qryFindCart, "i", [$checkoutID]);
            if(empty($cartDBRow) || ($cartDBRow->UserID != $userID)) {
                throw new TwigException("Either Cart does not exist, or you don't have enough priviliges to view it");
            }

//            `CartID`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `UserID`            INT UNSIGNED NOT NULL,
//            `PaymentStatus`     ENUM('STARTED', 'PAID') DEFAULT 'STARTED',
//            `CostIncTax`        DECIMAL(15,2),
//            `ShippingCost`      DECIMAL(15,2),
//            `OrderID`           VARCHAR(1024) NOT NULL,
//            `Address`           TEXT DEFAULT NULL,

            $delivery_address = json_decode($cartDBRow->Address);

            // Now get each products one by one
            $qryFindCart = "SELECT JF_UserCartItems.*, JF_Items.URLID from JF_UserCartItems, JF_Items where JF_UserCartItems.ItemID = JF_Items.ItemID AND CartID = ?";

            $cartItemDBRows = fetchRows($dbConn, $qryFindCart, "i", [$checkoutID]);
//            //    JF_UserCartItems
//            `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `CartID`            INT UNSIGNED NOT NULL,
//            `ItemID`            INT UNSIGNED NOT NULL,
//            `ItemTitle`         TEXT DEFAULT NULL,
//            `ItemQuantity`      INT UNSIGNED NOT NULL,
//            `ItemPricePerUnit`  DECIMAL(15,2),
//            `ItemGSTPercent`    DECIMAL(15,2),
//            `ItemTaxPerUnit`    DECIMAL(15,2),
            if(empty($cartItemDBRows)) {
                $cartItemDBRows =[];
            }

            $shipping = $cartDBRow->ShippingCost;
            $products = [];
            $total_excl_tax = 0;
            $total_tax = 0;

            foreach ($cartItemDBRows as $cartItemDBRow) {
                $product = new stdClass();
                $product->id = $cartItemDBRow->ItemID;
                $product->urlid = $cartItemDBRow->URLID;
                $product->mrp = $cartItemDBRow->ItemPricePerUnit;
                $product->units = $cartItemDBRow->ItemQuantity;
                $product->title = $cartItemDBRow->ItemTitle;
                $product->tax_percentage = $cartItemDBRow->ItemGSTPercent;
                $product->tax_per_unit = $cartItemDBRow->ItemTaxPerUnit;

                $product_total_excl_tax = $cartItemDBRow->ItemQuantity*$cartItemDBRow->ItemPricePerUnit;
                $product_total_tax = $product_total_excl_tax*($cartItemDBRow->ItemGSTPercent/100);

                $product->total_tax = $product_total_tax;
                $product->total_incl_tax = $product_total_excl_tax + $product_total_tax;

                $total_excl_tax += $product_total_excl_tax;
                $total_tax += $product_total_tax;

                $products[] = $product;
            }

            // Create the response
            $data = new stdClass();
            $data->delivery_address = $delivery_address;
            $data->products = $products;
            $data->total_excl_tax = $total_excl_tax;
            $data->tax = $total_tax;
            $data->shipping = $shipping;
            $data->total = $total_excl_tax + $total_tax + $shipping;
            $data->rzp_orderid = $cartDBRow->OrderID;

            $resp['error'] = false;
            $resp['data'] = $data;
            $resp['message'] = 'Showing the User Cart';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(200);
            $app->response()->status(200);
        } else {
            $resp['error'] = true;
            $resp['message'] = 'The User needs to be logged-in to perform this operation';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(401);
            $app->response()->status(401);
        }
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});

//  Returns all the Orders for logged in user
//
// Response:
//
//  'orders' : [
//      {
//          'rzp_orderid' : 'rzp_A34RFhgs45',     // RazorPay OrderID - string
//          'products' : [
//              {
//                  'id'    : 101,                // integer
//                  'mrp'   : 1499.0,             // float
//                  'units' : 3,                  // integer
//                  'title' : 'Teddy Bear (M)',   // String
//                  'tax_percentage' : 10.0,      // Float
//                  'tax_per_unit' : 149.0,       // float
//                  'total_tax' : 447.0           // float
//                  'total_incl_tax' : 4944.0     // float
//              }, ...
//          ],
//          'total_excl_tax' : 4497.0,            // float
//          'tax' : 447.0,                        // float
//          'total' : 4944.0                      // float
//          'delivery_address' : {
//              'name'  : '...',
//              'line1' : '...',
//              'line2' : '...',
//              'city'  : '...',
//              'mobile': '...',
//              'email' : '...'
//          },
//          'date_created' : '2018-01-05 12:05:54'
//      }
//  ]
$app->get('/myorders', function() use($app) {

    try {
        if (checkAuthentication()) {
            $userID = getLoggedUserID();

            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
            // Confirm if the Cart that the logged in user is trying to access is something that it has created
		$qryFindCart = "SELECT * from JF_UserCart where UserID = ? And PaymentStatus = ? ORDER BY CartID DESC";
            $cartDBRows = fetchRows($dbConn, $qryFindCart, "is", [$userID, 'PAID']);

            // The output
            $data = new stdClass();
            $data->orders = [];

            foreach($cartDBRows as $cartDBRow) {

//            `CartID`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `UserID`            INT UNSIGNED NOT NULL,
//            `PaymentStatus`     ENUM('STARTED', 'PAID') DEFAULT 'STARTED',
//            `CostIncTax`        DECIMAL(15,2),
//            `ShippingCost`      DECIMAL(15,2),
//            `OrderID`           VARCHAR(1024) NOT NULL,
//            `Address`           TEXT DEFAULT NULL,
//            `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//            `ModifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                $delivery_address = json_decode($cartDBRow->Address);
                $checkoutID = $cartDBRow->CartID;

                // Now get each products one by one
                $qryFindCart = "SELECT JF_UserCartItems.*, JF_Items.URLID from JF_UserCartItems, JF_Items where JF_UserCartItems.ItemID = JF_Items.ItemID AND CartID = ?";

                $cartItemDBRows = fetchRows($dbConn, $qryFindCart, "i", [$checkoutID]);
//            //    JF_UserCartItems
//            `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
//            `CartID`            INT UNSIGNED NOT NULL,
//            `ItemID`            INT UNSIGNED NOT NULL,
//            `ItemTitle`         TEXT DEFAULT NULL,
//            `ItemQuantity`      INT UNSIGNED NOT NULL,
//            `ItemPricePerUnit`  DECIMAL(15,2),
//            `ItemGSTPercent`    DECIMAL(15,2),
//            `ItemTaxPerUnit`    DECIMAL(15,2),
                if(empty($cartItemDBRows)) {
                    $cartItemDBRows =[];
                }

                $shipping = $cartDBRow->ShippingCost;
                $products = [];
                $total_excl_tax = 0;
                $total_tax = 0;

                foreach ($cartItemDBRows as $cartItemDBRow) {
                    $product = new stdClass();
                    $product->id = $cartItemDBRow->ItemID;
                    $product->urlid = $cartItemDBRow->URLID;
                    $product->mrp = $cartItemDBRow->ItemPricePerUnit;
                    $product->units = $cartItemDBRow->ItemQuantity;
                    $product->title = $cartItemDBRow->ItemTitle;
                    $product->tax_percentage = $cartItemDBRow->ItemGSTPercent;
                    $product->tax_per_unit = $cartItemDBRow->ItemTaxPerUnit;

                    $product_total_excl_tax = $cartItemDBRow->ItemQuantity*$cartItemDBRow->ItemPricePerUnit;
                    $product_total_tax = $product_total_excl_tax*($cartItemDBRow->ItemGSTPercent/100);

                    $product->total_tax = $product_total_tax;
                    $product->total_incl_tax = $product_total_excl_tax + $product_total_tax;

                    $total_excl_tax += $product_total_excl_tax;
                    $total_tax += $product_total_tax;

                    $products[] = $product;
                }

                // Create the response
                $orderData = new stdClass();
                $orderData->delivery_address = $delivery_address;
                $orderData->products = $products;
                $orderData->total_excl_tax = $total_excl_tax;
                $orderData->tax = $total_tax;
                $orderData->shipping = $shipping;
                $orderData->total = $total_excl_tax + $total_tax + $shipping;
                $orderData->rzp_orderid = $cartDBRow->OrderID;
                $orderData->date_created = $cartDBRow->ModifiedOn;

                // Add the order entry to $orders_data
                $data->orders[] = $orderData;
            }

            $resp['error'] = false;
            $resp['data'] = $data;
            $resp['message'] = 'My Orders';
            if(empty($data->orders)) {
                $resp['message'] = 'You dont have any orders. If you have paid but processing did not go through, please wait for a couple of days for reconciliation. You can contact us on 7387826610.';
                $resp['showmessage'] = true;
            }
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(200);
            $app->response()->status(200);
        } else {
            $resp['error'] = true;
            $resp['message'] = 'The User needs to be logged-in to perform this operation';
            $app->contentType('application/json');
            $app->response()->setBody(json_encode($resp));
            $app->response()->setStatus(401);
            $app->response()->status(401);
        }
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});

// Called after Razor Pay payment is complete. Here we should store the payment
// information in the backend against the Cart
$app->post('/contactus', function() use($app) {
    try {
        $requestBody = getRequestParams();

        if(!isset($requestBody['contact'])) {
            throw new TwigException("The API request is malformed.");
        }

        $requestBody = $requestBody['contact'];

        if(!isset($requestBody['name']) || !isset($requestBody['email']) || !isset($requestBody['mobile']) || !isset($requestBody['message'])) {
            throw new TwigException("All the fields are mandatory!");
        }

        $name = trim($requestBody['name']);
        $mobileNo = trim($requestBody['mobile']);
        $email = trim($requestBody['email']);
        $message = trim($requestBody['message']);

        if(sizeof($name) > 100 || sizeof($email) > 100 || sizeof($message) > 4096) {
            throw new TwigException("Message should be less than 4096 characters!");
        }

        if(!isValidPhone($mobileNo)) {
            throw new TwigException("Mobile number not valid!");
        }

        $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf");
        // Confirm if the Cart that the logged in user is trying to access is something that it has created
        $qryInsert = "INSERT INTO JF_ContactUs(UserName, EmailID, Mobile, Message) VALUES (?,?,?,?)";
        $cartDBRow = updateRow($dbConn, $qryInsert, "ssss", [$name, $email, $mobileNo, $message]);

        // Came here means the operation has succeeded
        // Send out myself an SMS
        // Now sent the SMS
        \TwigMeServerApplication\phoneotp\SMSController::sendSMS([9975385693], "Msg from $name($mobileNo):$message", "");

        // Create the response
        $data = new stdClass();

        $resp['error'] = false;
        $resp['data'] = $data;
        $resp['showmessage'] = true;
        $resp['message'] = 'Submitted Succeessfully';
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (TwigException $twEx) {
        $resp['error'] = true;
        $resp['message'] = $twEx->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    } catch (Exception $ex) {
        $resp['error'] = true;
        $resp['message'] = $ex->getMessage();
        $app->contentType('application/json');
        $app->response()->setBody(json_encode($resp));
        $app->response()->setStatus(200);
        $app->response()->status(200);
    }
});


    $app->get('/:version/push/enctest/:zviceID', function($version, $zviceID) use($app) {

        $enczviceid=bf64_encrypt(dechex($zviceID),GLOBALZVICEKEY);
        $resp['encTagID'] = $enczviceid;
        //echo 'Enctag:' . $enczviceid;
        echoResponse(200, $resp);

    });

    $app->get('/:version/push/dectest/:zviceID', function($version, $zviceID) use($app) {

        $decitagid = hexdec(bf64_decrypt($zviceID, GLOBALZVICEKEY));
        $resp['decTagID'] = $decitagid;
        echoResponse(200, $resp);

    });

    $app->get('/:version/twigme/userdetails/:tagID', 'authenticate', function($version, $tagID) use($app) {
        $decitagid = hexdec(bf64_decrypt($tagID, GLOBALZVICEKEY));
        if(!isAutoITagID($decitagid)) {
            $response["error"] = true;
            $response["message"] = "Provided Tag $tagID is not a valid TwigMe User Tag";
        } else {
            // PPHACK
            $dbConnector = getPartitionDBHandle(getLoggedUserID());
            $userDetails = \TwigMeServerApplication\users\UserList::findUser([ 'AutoItagID' => $decitagid ], $dbConnector);
            if (is_null($userDetails)) {
                $response["error"] = true;
                $response["message"] = "TwigMe Tag Provided $tagID not found";
            } else {
                $response["error"] = false;
                $response["email"] = $userDetails->UserEmail;
                $response["phone"] = $userDetails->UserPhone;
            }
        }
        echoResponse(200, $response);
    });



    $app->get('/:version/push/generate_tags/:startTag/:totalTag', function($version, $startTag, $totalTags) use($app) {
        $i = 0;
        $myfile = fopen("/tmp/tags.txt", "a") or die("Unable to open file!");
        while($i < $totalTags) {
            $enczviceid=bf64_encrypt(dechex($startTag + $i),GLOBALZVICEKEY);
            fwrite($myfile, "\n". $enczviceid);
            $i = $i + 1;
        }
        fclose($myfile);
        echoResponse(200, "Done");

    });


    $app->get('/:version/push/generate_tags1/', function () use ($app) {
        $i = 0;
        $mytagfile = fopen("/tmp/inputtags.csv", "r") or die("Unable to open file!");
        $myfile = fopen("/tmp/outputtags.csv", "a") or die("Unable to open file!");
        while (($line = fgets($mytagfile)) !== false) {
            // process the line read.
            $enczviceid = bf64_encrypt(dechex(trim($line)), GLOBALZVICEKEY);
            fwrite($myfile, $enczviceid . "\n");
        }

        fclose($mytagfile);
        fclose($myfile);
        echoResponse(200, "Done");

    });

    $app->get('/:version/push/generate_dec_tags/', function () use ($app) {
        $i = 0;
        $mytagfile = fopen("/tmp/inputtags.csv", "r") or die("Unable to open file!");
        $myfile = fopen("/tmp/outputtags.csv", "a") or die("Unable to open file!");
        while (($line = fgets($mytagfile)) !== false) {
            // process the line read.
            $devZviceID = Utils::getDecryptedZviceID(trim($line));
            fwrite($myfile, $devZviceID . "\n");
        }

        fclose($mytagfile);
        fclose($myfile);
        echoResponse(200, "Done");

    });

    // </editor-fold>

    $app->get('/:version/zvice/detailscard/:zviceID', 'authenticate',
        function($version, $zviceID) use($app) {
            detailsCardAPIHandler($version, $zviceID);
        }
    );

    $app->post('/:version/zvice/detailscard/:zviceID', 'authenticate',
        function($version, $zviceID) use($app) {
            detailsCardAPIHandler($version, $zviceID);
        }
    );

    function detailsCardAPIHandler($version, $zviceID) {
        $response = array();
        $deczviceid = hexdec(bf64_decrypt($zviceID,GLOBALZVICEKEY));

        try{
            $result = getDetailCards($deczviceid);

            if(isset(Cache::getCacheObj()->respExtraAttr)) {
                $obj = Cache::getCacheObj()->respExtraAttr;
                foreach ($obj as $key => $value) {
                    if($key == "numcolumns" || $key == "pagetype" || $key == "bgimgurl" || $key == "title" || $key == "subtitle" ||
                        $key == "toolbarbgimgurl"  || $key == "saveOffline") {
                        $response[$key] = $value;
                    } else {
                        $result[$key] = $value;
                    }
                }
            }

            $response["error"]      = false;
            $response["error_code"] = -1;
            $response["data"]       = $result;
            // Add the Title/Subtitle, HomePage
            \TwigMeServerApplication\utility\Utils::setOrgDetailsInResponse($response, $deczviceid);

            //Log the activity here
            $orgZviceID   = getOrgZviceID($deczviceid);
            $activityData = [
                'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
                'RequestBody' => null,
                'MetaData'    => [
                    'ZviceID'    => $deczviceid,
                    'OrgZviceID' => $orgZviceID,
                    'Operation'  => ActivityLog::OPERATION_DETAILS_CARD_GET
                ]
            ];
            ActivityLog::log($activityData, $orgZviceID);

            $statusCode = 200;
        } catch (TwigException $te) {
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
            $statusCode = 404;
        }

        echoResponse($statusCode, $response);
    }

    $app->get('/:version/public/zvice/:zviceID',  function($version, $zviceID) use($app) {
        global $user_id;
        $response = array();

        $deczviceid = hexdec(bf64_decrypt($zviceID,GLOBALZVICEKEY));
        try{
            $mysqli = getGlobalDBHandle();
            $qry = 'SELECT Title,Description,ProfilePic FROM ZviceListSummary WHERE ZviceType = "ZTAG" and ZviceID = AssignedZbotID and ZviceID = ?';
            $result = fetchRows($mysqli, $qry, "i", array($deczviceid));

            if(empty($result)){
                throw new \TwigException("Data Not Available",404);
            }

            $response["error"] = false;
            $response["error_code"] = -1;
            $response["data"] = $result;
            $statusCode = 200;
        } catch (TwigException $te) {
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
            $statusCode = 404;
        }

        //Log the activity here
        $orgZviceID   = getOrgZviceID($deczviceid);
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => null,
            'MetaData'    => [
                'ZviceID'    => $deczviceid,
                'OrgZviceID' => $orgZviceID,
                'Operation'  => ActivityLog::OPERATION_DETAILS_CARD_GET
            ]
        ];
        ActivityLog::log($activityData, $orgZviceID);

        echoResponse($statusCode, $response);
    });

    $app->post('/:version/user/notification/', 'authenticate', function($version) use($app) {


        $json = $app->request->getBody();
        $json = json_decode($json, true);

        $emailfrom = $json['emailfrom'];
        $emailto = $json['emailto'];
        $message = $json['msg'];
        $ts = $json['ts'];
        if(!isset($json['type'])) {
            // 0 is for text, 1 is for image
            $type = 0;
        } else {
            $type = $json['type'];
        }
        $chatRoomID = 0;
        if(isset($json['chatroomname'])) {
            $chatRoomID = $json['chatroomname'];
        }
        $messageid = NULL;
        if(isset($json['messageid'])) {
            $messageid = $json['messageid'];
        }

        $logdata = [
            'emailfrom' => $emailfrom,
            'emailTo'   => $emailto,
            'json'      => $json
        ];
        $app->TwigMeLogger->log(null, $logdata, \TwigMeServerApplication\logger\Logger::LOG_LEVEL_DEBUG);

        if(!empty($emailfrom) && !empty($emailto)) {
            list($success1, $useridfrom) = getUserID($emailfrom, NULL);
            list($success2, $useridto) = getUserID($emailto, NULL);

            if (!$success1 || !$success2) {
                $statusCode = 200;
                $response["error"] = true;
                $response["message"] = "Email From or To incorrect";
                $response["error_code"] = -1;
                echoRespnse($statusCode, $response);
            } else {

                $userIDToArr = array();
                $userIDToArr[] = $useridto;

                sendMsg($useridfrom, $userIDToArr, $message, $ts, $type, $chatRoomID, $messageid);
            }
        } else {
            $statusCode = 200;
            $response["error"] = true;
            $response["message"] = "Email From or To empty";
            $response["error_code"] = -1;
            echoRespnse($statusCode, $response);
        }
    });

    $app->get('/:version/user/notification/:token/:msg', function($version, $token, $msg) use($app) {

        $apptoken[] = $token;
    //        $apptoken[] = 'dzKSiSM619w:APA91bG8UYeoi6acuupSWoJGRgF0sjh29qGN3mhWH9yBy2PHMy404YqMmbpGYmm3z1TyerYvbFryougCFHplca2CxpzsiAU1B78MJWmPXv6eQQ5w7RCzyLcm0X1RoO1pSTQI3nBZlrfY';
        $curl_response = sendNotification($msg, $apptoken);

        $response["error"] = true;
        $response["message"] = "";
        $response["error_code"] = -1;
        $response['data'] = $curl_response;
        echoResponse(200, $response);
    });



    $app->post('/:version/user/apptoken/', 'authenticate', function($version) use($app) {
        $app->TwigMeLogger->log("Inside APP Token API Handler", null, \TwigMeServerApplication\logger\Logger::LOG_LEVEL_DEBUG);
        $headers = \TwigMeServerApplication\utility\HeaderUtils::getHeaders();
        $logintoken = $headers['logintoken'];
        $requestBody = getRequestParams();
        $response = setAppTokenForUser($logintoken, $requestBody);

        echoResponse(200, $response);
    });

    function setAppTokenForUser($logintoken, $json) {
        if(!empty($logintoken) && isset($json['apptoken']) && isset($json['useremail'])) {
            $apptoken = $json['apptoken'];
            $useremail = $json['useremail'];

            try {
                // Delete all the tokens where this apptoken is used:
                $mysqli = getGlobalDBHandle();
                $qry = "DELETE From LoginList where AppToken = ? AND Token != ?";
                deleteRows($mysqli, $qry, "ss", [$apptoken, $logintoken]);

                // Update the apptoken for the row containing the logintoken
                $source = \TwigMeServerApplication\utility\HeaderUtils::getUserAgent();
                $qry = "Update LoginList set AppToken = ?, Source = ? where Token = ? AND UserID = ?";
                updateRow($mysqli, $qry, "sssi", array($apptoken, $source, $logintoken, getLoggedUserID()));
                $response["error"] = false;
            } catch (Exception $de) {
                $response["error"] = true;
                $response["message"] = $de->getMessage() . ", emailID:" . $useremail;
                $response["error_code"] = $de->getCode();
            }
        } else {
            $response["error"] = true;
            $response["message"] = "logintoken OR apptoken and/or useremail not found";
        }

        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_LOGIN,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => "APPTOKEN",
            ]
        ];
        ActivityLog::log($activityData);

        return $response;
    }

    $app->post('/:version/tag/search/', 'authenticate', function($version) use($app) {

        //	verifyRequiredParams(array('tagIds'));

        global $user_id;
        $response = array();

        //	$tagid = $app->request->post('tagIds');

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        $tagIDArr = array();
        if(!isset($json['searchQuery']) || strlen($json['searchQuery']) <= 2) {
            $searchQuery = $json['searchQuery'];
            $response["error"] = true;
            $response["message"] = "Minimum 3 characters needed in search";
            $response["error_code"] = "-1";
        } else {
            $searchQuery = $json['searchQuery'];
            try {
                $mysqli = getGlobalDBHandle();
                $qry = "SELECT ZviceID FROM ZviceListSummary WHERE TITLE LIKE ? AND ZviceID = AssignedZbotID And ZviceType != ?";
                $find = '%' . $searchQuery . '%';
                $rows = fetchRows($mysqli, $qry, "ss", array($find, 'ITAG'));

                $tagIDArr = array();
                if($rows != NULL && sizeof($rows) > 0) {
                    foreach ($rows as $row) {
                        $isFreeBiz = TwigMeServerApplication\provisioning\self_provisioning\SelfProvisioningModelHelper::isSelfProvisionedFreeBusiness($row->ZviceID);
                        if ($isFreeBiz === false) {
                            $tagIDArr[] = $row->ZviceID;
                        }
                    }
                }

                list($success, $errormsg, $result) = getBaseCards($tagIDArr, $mysqli, false /*hideActions*/);

                if ($success == true) {
                    $response["error_code"] = -1;
                    if(empty($result) || empty($result['elements'])) {
                        // This is done deliberately, since error=false resulted in empty cards being shown
                        $response["error"] = false;
                        $response["message"] = "No matching results found";
                        $response["showmessage"] = true;
                        $response["error_code"] = -1;
                        $response["data"]["elements"] = [];
                    } else {
                        $response["error"] = false;
                        $response["message"] = "Operation successful";
                        $response["data"] = $result;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = $errormsg;
                    $response["error_code"] = -1;
                }
            } catch (TwigException $ex) {
                $response["error"] = true;
                $response["message"] = $ex->getMessage();
                $response["error_code"] = $ex->getCode();
            }
        }

        try {
                //Log the activity here
                $activityData = [
                    'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
                    'RequestBody' => $json,
                    'MetaData'    => [
                        'Operation'  => ActivityLog::OPERATION_TAG_SEARCH_GET
                    ]
                ];
                ActivityLog::log($activityData);
        } catch (Exception $ex) {
            error_log("tag/search: " . $ex->getMessage());
        }

        echoResponse(200, $response);
    });

/*    $app->get('/:version/linked_users/millenium', function($version) use($app) {
        $qry = "select count(*) as TotalLinked from ZviceListSummary where ZviceID = ZviceID AND AssignedZbotID  = ? AND ZviceID <> LinkedZviceID AND ZviceType = ?";

        $mysqli = getGlobalDBHandle();
        $row = fetchSingleRowLenient($mysqli, $qry, "is", array(1000044647, 'ITAG'));
        if($row != NULL) {
            $resp['linked_users'] = $row->TotalLinked;
            echoResponse(200, $resp);
        } else {
            $resp['error_message'] = "Something went wrong, please try again later";
            echoResponse(200, $resp);
        }

    });

    $app->get('/:version/books/millenium', function($version) use($app) {
        $qry = "select count(*) as TotalBooks from zestlZviceLibraryMillenium.ZviceList where AssignedZbotID = ? and TagProfile = ?";
        $mysqli = getGlobalDBHandle();
        $row = fetchSingleRowLenient($mysqli, $qry, "ii", array(1000044647, 2));
        if($row != NULL) {
            $resp['total_books'] = $row->TotalBooks;
            echoResponse(200, $resp);
        } else {
            $resp['error_message'] = "Something went wrong, please try again later";
            echoResponse(200, $resp);
        }

    });*/

    $app->get('/:version/book/isbn/:id', 'authenticate',  function($version, $isbn) use($app) {
        // Check for existence of isbn
        if($isbn == NULL || strlen($isbn) <= 0) {
            $response["error"] = true;
            $response["message"] = "No ISBN Code passed";
            echoResponse(200, $response);
        }
        // Find out the data from AWS, then ISBNDB, then Google Books APIs

        // AWS API
        $bookObj = getAmazonBookJSON($isbn);
        if($bookObj == NULL) {
            $bookObj = getGoogleBookJSON($isbn);
        }
        // ISBN DB
        if($bookObj == NULL) {
            $bookObj = getISBNDBBookJSON($isbn);
        }

        if($bookObj == NULL) {
            $bookObj = getGoodReadsBookJSON($isbn);
        } else {
            $fieldsToCheck = [
                'title',
                'author',
                'publisher',
                'imgurl',
                'description'
            ];
            $fieldsToFetchFromGoodreads = [];
            foreach ($fieldsToCheck as $fieldToCheck) {
                if(empty($bookObj[$fieldToCheck])) {
                    $fieldsToFetchFromGoodreads[] = $fieldToCheck;
                }
            }
            //point to note here is that the order of keys in response might change
            if (!empty($fieldsToFetchFromGoodreads)) {
                $goodreadBookObj = getGoodReadsBookJSON($isbn);
                if($goodreadBookObj != NULL) {
                    foreach($fieldsToFetchFromGoodreads as $field) {
                        if(isset($goodreadBookObj[$field])) {
                            $bookObj[$field] = $goodreadBookObj[$field];
                        }
                    }
                }
            }
        }

        if (isset($bookObj['description'])) {
            $bookObj['description'] = strip_tags($bookObj['description']);
        }

        // If no data found, at least set the isbn field
        if($bookObj == NULL) {
            $bookObj['isbn'] = $isbn;
        }

        $response["error"] = false;
        $response["message"] = "";
        $response["data"] = json_encode($bookObj);
        echoResponse(200, $response);

    });

    /**
     * Listing Front Cards for a ZviceID
     * method GET
     * url /zvice/frontcards/:id
     * Will return 404 if failure
     */

    $app->post('/:version/zvice/basecard/', 'authenticate', function($version) use($app) {
        getBaseCardsForTagIDs();
    });

    $app->post('/:version/basecard/scan', 'authenticate', function($version) use($app) {
        $json = getRequestParams();
        $qrCode = $json['tagIds'];
        getBaseCardsForTagIDs($qrCode);
    });


    $app->post('/:version/library/fastscan', 'ENABLE_TRANSACTION', 'authenticate', function($version) use($app) {


        $json = $app->request->getBody();
        $json = json_decode($json, true);

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_LIBRARY_FAST_SCAN_POST
            ]
        ];
        ActivityLog::log($activityData);

        if(isset($json['tagIds'])) {
            $qrCode = $json['tagIds'];
            $decQRCode = hexdec(bf64_decrypt($qrCode, GLOBALZVICEKEY));
            // PP:  27-01-16 : Find linked tag ID
            $decQRCode = getLinkedZviceID($decQRCode);
            $qrCode = bf64_encrypt(dechex($decQRCode),GLOBALZVICEKEY);
            $json['tagIds'] = $qrCode;

            $interactionID = $json['interactionID'];
            $libraryZviceID = $json['libTagID'];
            $deczviceid = hexdec(bf64_decrypt($libraryZviceID,GLOBALZVICEKEY));
            $mysqli = getPartitionDBHandle($deczviceid, "zvice");

            try {
                $cards = interaction($deczviceid, $interactionID, $json, $mysqli);
                $response["error"] = false;
                $response["message"] = "";
                $response["error_code"] = -1;
                $res["elements"] = $cards;
                $response["data"] = $res;

                echoResponse(200, $response);

            } catch (DBException $de) {
                $response["error"] = true;
                $response["message"] = $de->getMessage();
                $response["error_code"] = $de->getCode();
                $response["query"] = $de->getQuery();
                echoResponse(201, $response);
            } catch (Exception $ex) {
                $response["error"] = true;
                $response["message"] = "Operation Failed! " . $ex->getMessage();
                $response["error_code"] = $ex->getCode();
                echoResponse(201, $response);
            }
        } else {
            $response["error"] = true;
            $response["message"] = "Tag not found";
            $response["error_code"] = -1;
            echoResponse(200, $response);
        }
    });


    $app->post('/:version/user/proximitytags', 'authenticate', function($version) use($app) {

        global $user_id;
        $response = array();

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_NEAR_BY_TAGS_POST
            ]
        ];
        ActivityLog::log($activityData);

        $lat = $json['lat'];
        $lon = $json['lng'];
        // error_log("[UserID: $userid] Lat : $lat, Long : $lon");
        if(isset($json['radius'])) {
            $radius = $json['radius'];
        } else {
            // Use a default value for the radius
            $radius = 10; // KILO meters
        }

        findLatBoundary($radius, $lat, $lat1, $lat2);
        findLonBoundary($radius, $lat, $lon, $lat1, $lat2, $lon1, $lon2);

        $mysqli = getGlobalDBHandle();
        $qry = "SELECT ZviceID, DISTANCE(Latitude, Longitude, ?, ?) as Distance from ZviceListSummary where " .
            " Latitude BETWEEN ? AND ? AND " .
            " Longitude BETWEEN ? AND ? " .
            " AND ZviceID = AssignedZbotID HAVING" .
            " Distance < ? ORDER BY Distance";
        $rows = fetchRows($mysqli, $qry, "sssssss", array($lat, $lon, $lat1, $lat2, $lon1, $lon2, $radius));

        $zviceList = NULL; $zviceDistArr = array();
        if($rows != NULL) {
            foreach($rows as $row) {
                $zviceList[] = $row->ZviceID;
                /*
					$enczviceid=bf64_encrypt(dechex($row->ZviceID),GLOBALZVICEKEY);
					$zviceDistArr[$enczviceid] = $row->Distance;
				*/
            }
        }

        $cards = NULL;
        list($success, $errormsg, $response1) = getBaseCards($zviceList, NULL, false);
        if($success == true) {
            if($response1["elements"] !=NULL) {
                foreach($response1["elements"] as $basecard) {
                    /*
						if(isset($basecard->tagId)) {
							if(isset($zviceDistArr[$basecard->tagId])) {
								$basecard->distance = $zviceDistArr[$basecard->tagId];
								$cards[$basecard->distance] = $basecard;
							}
						}
						*/
                    $cards[] = $basecard;
                }
            }
            /*
				if($cards != NULL) {
					ksort($cards);
					$cards = array_values($cards);
				}
		*/
            $response['pagetype'] = 3;
            $response["error"] = false;
            $response["message"] = "";
            $response["error_code"] = -1;
            $res["elements"] = $cards;
            $response["data"] = $res;
        } else {
            $response["error"] = true;
            $response["message"] = $errormsg;
            $response["error_code"] = -1;
        }

        echoResponse(200, $response);
    });


    $app->post('/:version/zvice/setcoordinates/:id', 'ENABLE_TRANSACTION', 'authenticate', function($version, $zviceID) use($app) {


        global $user_id;
        $response = array();

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        //Log the activity here
        $decZviceID   = Utils::getDecryptedZviceID($zviceID);
        $OrgZviceID   = getOrgZviceID($decZviceID);
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'ZviceID'    => $decZviceID,
                'OrgZviceID' => $OrgZviceID,
                'Operation'  => ActivityLog::OPERATION_ZVICE_SET_COORDINATES_POST
            ]
        ];
        ActivityLog::log($activityData);

        if(($decZviceID < ZVICEIDMIN) || ($decZviceID > ZVICEIDMAX))
        {
            // not a valid tag id
            $response["error"] = true;
            $response["message"] = "This is not a valid Tag";
            echoResponse(200, $response);
        }

        $lat = $json['lat'];
        $long = $json['lng'];


        // The data that would be passed on to
        // the methods which process this interaction
        $data = array();
        $data["lat"] = $lat;
        $data["long"] = $long;

        // $interactionID = $app->request->post('interactionID');
        $interactionID = CommonInteraction::INTERACTION_TYPE_SET_COORDINATES;

        $mysqli = getPartitionDBHandle($decZviceID, "zvice");

        try {
            $cards = interaction($decZviceID, $interactionID, $data, $mysqli);
            $response["error"] = false;
            $response["message"] = "";
            $response["error_code"] = -1;
            $res["elements"] = $cards;
            $response["data"] = $res;

            echoResponse(200, $response);

        } catch (DBException $de) {
            $response["error"] = true;
            $response["message"] = $de->getMessage();
            $response["error_code"] = $de->getCode();
            $response["query"] = $de->getQuery();
            echoResponse(404, $response);
        }

    });

    $app->post('/:version/image/upload(/)', 'authenticate', function($version) use($app) {
        // reading post params
        $errorMsg = '';
        $json = $app->request->getBody();
        $json = json_decode($json, true);

        if(isset($json["media_local_path"]) && isset($json["rte_id"])) {
            $media_local_path = $json["media_local_path"];
            $rte_id = $json["rte_id"];
            $success = uploadAttachmentBusiness($json, $fileNameArr, $s3urlArr);
            if(!$success) {
                $errorMsg = 'Some error uploading to S3';
            }
        } else {
            $success = false;
            $errorMsg = 'Media Local Path not sent';
        }

        if(!$success) {
            $response["error"] = true;
            $response["message"] = $errorMsg;
            echoResponse(200, $response);
        } else {
            $response["error"] = false;
            $response["message"] = 'Image successfully uploaded';
            $response["s3URL"] = $s3urlArr[0];
            $response["media_local_path"] =  $media_local_path;
            $response["rte_id"] = $rte_id;
            echoResponse(200, $response);
        }
    });

    $app->post('/:version/image/compress/s3', 'ENABLE_TRANSACTION', 'authenticate', function($version) use($app) {
        $logger = $app->TwigMeLogger;
        try {
            if(Utils::isSuperAdmin(getLoggedUserID())) {
                throw new TwigException("Operation only allowed to Super Admins");
            }

            $requestBody               = getRequestParams(); // json_decode($this->app->request()->getBody(), true)
            $handle = $requestBody['s3handle'];
            $status = \TwigMeServerApplication\S3\S3Controller::compressS3Image($handle, 'MEDIUM', $errorMsg);
            $response['error'] = !$status;
            if(!empty($errorMsg)) {
                $response['message'] = $errorMsg;
            }
        } catch (\Exception $ex) {
            $logger->logException($ex);
            $response["error"]   = true;
            $response["message"] = "Operation unsuccessful. " . $ex->getMessage();
        }
        echoResponse(200, $response);
    });

    $app->post('/:version/zvice/interaction(/(:id))', 'ENABLE_TRANSACTION', 'authenticate', function($version, $zviceID=NULL) use($app) {

        // reading post params

        $request_params = array();
        $request_params = $_REQUEST;

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        // The API has been modified to be used such that $zviceID can be sent as a body parameter
        if($zviceID == NULL) {
            $zviceID = GETVAL($json, 'zviceid');
            if($zviceID == NULL) {
                // Neither the API URI contains the zviceid
                // nor the body param. Return failure
                $response = array();
                $response["error"] = true;
                $response["message"] = 'No TagID provided';
                echoResponse(200, $response);
            }
        }

        //Log the activity here
        $decZviceID = Utils::getDecryptedZviceID($zviceID);
        $OrgZviceID = null;
        try {
            $OrgZviceID   = getOrgZviceID($decZviceID);
        } catch (Exception $ex) {
            $app->TwigMeLogger->logException($ex);
        }
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'ZviceID'    => $decZviceID,
                'OrgZviceID' => $OrgZviceID,
                'Operation'  => ActivityLog::OPERATION_ZVICE_INTERACTION_POST
            ]
        ];
        ActivityLog::log($activityData);

        // The data that would be passed on to
        // the methods which process this interaction
        $data = array();

//		foreach($_POST as $name => $value) {
        if($json != NULL) {
            foreach($json as $name => $value) {
                $data[$name] = $value;
            }
        }

//		$interactionID = $app->request->post('interactionID');
        $interactionID = $json['interactionID'];
        $deczviceid = hexdec(bf64_decrypt($zviceID,GLOBALZVICEKEY));

        $mysqli = getPartitionDBHandle($deczviceid, "zvice");

        try {
            $cards = interaction($deczviceid, $interactionID, $data, $mysqli);
            if(isset(Cache::getCacheObj()->respExtraAttr)) {
                $obj = Cache::getCacheObj()->respExtraAttr;
                foreach ($obj as $key => $value) {
                    // cardid added on Sujoy's request for Text Card addition
                    if($key == "pagetype" || $key == "title" || $key == "bgimgurl" || $key == "subtitle" || $key == "cardid" || $key == "toolbarbgimgurl") {
                        $response[$key] = $value;
                    } else {
                        $res[$key] = $value;
                    }
                }
            }

            $response["error"] = false;
            $response["message"] = "";
            $response["error_code"] = -1;
            $res["elements"] = $cards;
            $response["data"] = $res;
            // Add the Title/Subtitle, HomePage
            \TwigMeServerApplication\utility\Utils::setOrgDetailsInResponse($response, $deczviceid);

            echoResponse(200, $response);

        } catch (DBException $de) {
            $app->TwigMeLogger->logException($de);
            $response["error"] = true;
            $response["message"] = TwigException::OPERATION_FAILURE_MSG;
            $response["error_code"] = $de->getCode();
            echoResponse(200, $response);
        } catch (TwigException $e) {
            $app->TwigMeLogger->logException($e);
            $response["error"] = true;
            $response["message"] = $e->getMessage();
            $response["error_code"] = $e->getCode();
            echoResponse(200, $response);
        } catch (Exception $e) {
            $app->TwigMeLogger->logException($e);
            $response["error"] = true;
            $response["message"] = "Something went wrong. Please try again.";
            echoResponse(200, $response);
        }

    });

    /**
     * User deletion
     * url - /user/delete
     * method - POST
     * params - email, password
     * Password needs to be SHA512 encrypted - same as in the JS hex_sha512 function to match web api.
     */
    $app->post('/:version/user/delete', 'appkey_authenticate', function($version) use ($app) {
        // check for required params
        verifyRequiredParams(array('email', 'password'));

        $response = array();

        // reading post params
        $email = $app->request->post('email');
        $password = $app->request->post('password');

        authRequiredParams(array($email, $password));

        // Sanitize and validate the data passed in
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $password = filter_var($password, FILTER_SANITIZE_STRING);

        // validating email address
        validateEmail($email);

        // PPDEL
        $mysqli = NULL; /*new mysqli(HOST, USER, PASSWORD, DATABASE);
			if ($mysqli->connect_error) {
				$errormsg = "Unable to connect to MySQL";
				$response["error"] = true;
				$response["message"] = $errormsg;
				$response["src"] = 'post /user/register/';
				echoResponse(500, $response);
			} */

        list($success, $userid) = getUserID($email, $mysqli);
        if ($success == false) {
            // user exists
            $response["error"] = true;
            $response["message"] = "User does not exist.";
            echoResponse(200, $response);
        }
        else
        {

            list($success, $errormsg) = deleteUser($userid, $password, $mysqli);

            if ($success == true) {
                $response["error"] = false;
                $response["message"] = "User deleted.";
                echoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "Error deleting user: ".$errormsg;
                echoResponse(200, $response);
            }
        }
    });


    /**
     * Google Login
     * url - /user/googlelogin
     * method - POST
     * params - email, authToken
     */
    $app->post('/:version/user/googlelogin', 'ENABLE_TRANSACTION', 'appkey_authenticate', function($version) use ($app) {
        $requestBody                = getRequestParams();
        $attributes                 = [];
        $attributes['email']        = filter_var($requestBody['email'], FILTER_SANITIZE_EMAIL);
        $attributes['accessToken']  = $requestBody['accessToken'];
        $attributes['apptoken']     = isset($requestBody['apptoken'])?$requestBody['apptoken']:NULL;

        $app_version               = $requestBody['version'];
        $mysqli = null;

        try {
            $googleLogin                 = new GoogleLogin($attributes);
            list($statusCode, $response) = $googleLogin->login();

            // Add the upgrade attributes if upgrade/force-upgrade
            $response_upg = upgradeAPK($app_version);
            $response = array_merge($response, $response_upg);
            // Add Dashboard Content into the response, and
            // create a Business User for White Labelled app
            $response = (new \TwigMeServerApplication\login\LoginController())->addResponseContentForLogin($requestBody, $response);
        } catch (Exception $ex) {
            $app->TwigMeLogger->logException($ex);
            $statusCode = 500;
            // user credentials are wrong
            $response            = [];
            $response['error']   = true;
            $response['message'] = 'Login failed. Internal Server Error';
        }

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_LOGIN,
            'RequestBody' => $requestBody,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_GOOGLE_LOGIN
            ]
        ];
        ActivityLog::log($activityData);

        echoResponse($statusCode, $response);
    });

$app->post('/:version/user/send_activate_link', function($version) use ($app) {
        try {
            $json             = $app->request->getBody();
            $json             = json_decode($json, true);
            $email            = GETVAL($json, 'email');
            $OrgZviceID          = GETVAL($json, 'OrgZviceID');

            $dbConnector = getPartitionDBHandle($OrgZviceID);
            $zviceInfoObj = new ZviceListModelHelper($dbConnector, getLoggedUserID());
            $zviceInfoResult = $zviceInfoObj->find(['ZviceID' => $OrgZviceID], ['Title', 'ProfilePic']);

            $extraParams                   = new stdClass();
            $extraParams->OrgName          = $zviceInfoResult->Title;
            $extraParams->BusinessImageUrl = $zviceInfoResult->ProfilePic;
            $extraParams->BusinessName     = $zviceInfoResult->Title;
            $extraParams->OrgZviceID       = \TwigMeServerApplication\utility\Utils::getEncryptedZviceID($OrgZviceID);
            $extraParams->icon             = Constants::$ICON_LINKTOTWIGME;
            $extraParams->icon_label       = "TwigMe User Registration";

            list($success, $errorMsg) = sendActivationLink($email,$extraParams);

            $response['error'] = !$success;
            $response['message'] = $errorMsg;
            $response['showmessage'] = true;
            echoResponse(201, $response);
        } catch (Exception $ex) {
            $response['error'] = true;
            $response['message'] = $ex->getMessage();
            echoResponse(201, $response);
        }
    });

    $app->post('/:version/upgrade/available', 'isUpgradeAvailable');

    /**
     * User Logout
     * url - /user/logout
     * method - POST
     * params - email, logintoken
     */
    $app->post('/:version/user/logout', 'appkey_authenticate', function($version) use ($app) {
        // check for required params
        verifyRequiredParams(array('email', 'logintoken'));

        global $app_id;

        // reading post params
        $requestBody = $app->request()->post();
        $email       = $requestBody['email'];
        $logintoken  = $requestBody['logintoken'];
        $response    = array();

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_LOGOUT,
            'RequestBody' => $requestBody,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_USER_LOGOUT
            ]
        ];
        ActivityLog::log($activityData);

        authRequiredParams(array($email, $logintoken));

        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

        $mysqli = NULL; /*new mysqli(HOST, USER, PASSWORD, DATABASE);
			if ($mysqli->connect_error) {
				$errormsg = "Unable to connect to MySQL";
				$response["error"] = true;
				$response["message"] = $errormsg;
				$response["src"] = 'post /user/logout';
				echoResponse(500, $response);
			} */

        list($success, $userid) = getUserID($email, $mysqli);
        if ($success == true) {

            list($success, $errormsg) = (new TwigMeServerApplication\login\TokenController())->releaseLoginToken($userid, $logintoken, $mysqli);
            if ($success == false) {
                // unknown error occurred
                $response['error'] = true;
                $response['message'] = $errormsg;
                $response["src"] = 'post /user/logout';
                echoResponse(500, $response);
            }

        }
        else
        {
            // unknown error occurred
            $response['error'] = true;
            $response['message'] = "An error occurred. Please try again";
            $response["src"] = 'post /user/logout';
            echoResponse(500, $response);
        }


        $response['error'] = false;
        echoResponse(200, $response);

    });

    /**
     * User password reset
     * url - /user/password
     * method - POST
     * params - email
     */
    $app->post('/:version/user/password', 'appkey_authenticate', function($version) use ($app) {

        $statusCode = null;
        // check for required params
        verifyRequiredParams(array('email'));

        // reading post params
        $requestBody = $app->request()->post();
        $email       = $requestBody['email'];

        try {
            authRequiredParams(array($email));
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);

            resetPassword($email);

            $statusCode = 200;
            $response["error"] = false;
            $response['message'] = "Reset Link mailed to user";
        } catch (\DBException $dbEx) {
            $statusCode          = 500;
            $response["error"]   = true;
            $response["message"] = "Operation unsuccessful. Please try again";
        } catch (\TwigException $twEx) {
            $statusCode          = 500;
            $response["error"]   = true;
            $response["message"] = $twEx->getMessage();
        } catch (\Exception $ex) {
            $statusCode          = 500;
            $response["error"]   = true;
            $response["message"] = "Operation unsuccessful. Please try again";
        }

        //Log the activity here
        $activityData = [
            'ActionType' => ActivityLog::ACTION_TYPE_PWD_RESET,
            'RequestBody' => $requestBody,
            'MetaData' => [
                'Operation' => ActivityLog::OPERATION_USER_PWD_RESET
            ]
        ];
        ActivityLog::log($activityData);

        echoResponse($statusCode, $response);
    });

    $app->put('/:version/zvice/update/:id', 'ENABLE_TRANSACTION', 'authenticate', function ($zviceid = NULL) use ($app) {

        $contentType = strtolower($_SERVER["CONTENT_TYPE"]);
        if ($contentType != NULL && ($contentType == "application/json" || $contentType == "application/json;charset=utf-8")) {
            // This is a json request
            $request_params = json_decode($app->request->getBody(), true);

        } else {
            $request_params = $_REQUEST;
            // Handling PUT request params
            if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                $app = \Slim\Slim::getInstance();
                parse_str($app->request()->getBody(), $request_params);
            }
        }

        $extraData = GETVAL($request_params, 'extradata');
        if ($extraData != NULL) {
            try {
                $extraData = json_decode($extraData);
                $deczviceid = hexdec(bf64_decrypt($zviceid,GLOBALZVICEKEY));
                updateZviceList($deczviceid, $extraData);
                $response["error"] = false;
                $response["message"] = "Tag Successfully updated";
                echoResponse(201, $response);
            } catch (Exception $ex) {
                $response["error"] = true;
                $response["message"] = "Failure! " . $ex->getMessage();
                echoResponse(201, $response);
            }
        } else {
            $response["error"] = true;
            $response["message"] = "No fields defined!";
            echoResponse(201, $response);
        }
    });

    /**
     * Registering Zvice
     * method PUT
     * params zbotid, zvicetype, zviceloc, zviceinfo
     * url - /zvice/register/:id
     */
    $app->put('/:version/zvice/register(/(:id))', 'ENABLE_TRANSACTION', 'authenticate', function($version, $zviceid = NULL) use($app) {
        try {

            $contentType = strtolower($_SERVER["CONTENT_TYPE"]);
            if ($contentType != null && (in_array($contentType, ["application/json", "application/json;charset=utf-8"]))) {
                // This is a json request
                $request_params = json_decode($app->request->getBody(), true);
            } else {
                $request_params = $_REQUEST;
                // Handling PUT request params
                if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                    $app = \Slim\Slim::getInstance();
                    parse_str($app->request()->getBody(), $request_params);
                }
            }

            //Log the activity here
            try {
                $decZviceID   = Utils::getDecryptedZviceID($zviceid);
                $OrgZviceID   = getOrgZviceID($decZviceID);
                $activityData = [
                    'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
                    'RequestBody' => $request_params,
                    'MetaData'    => [
                        'ZviceID'    => $decZviceID,
                        'OrgZviceID' => $OrgZviceID,
                        'Operation'  => ActivityLog::OPERATION_ZVICE_REGISTER_POST
                    ]
                ];
                ActivityLog::log($activityData, $OrgZviceID);
            } catch (Exception $ex) {
                $app->TwigMeLogger->logException($ex);
            }

            //If this is Org tag replacement then check if loggedin user is superadmin or not
            //If this is not super admin then throw an error
            $linkedZviceID = GETVAL($request_params, "linkzviceid") ? Utils::getDecryptedZviceID(GETVAL($request_params, "linkzviceid")) : null;
            $linkedZbotID = GETVAL($request_params, "zbotid") ? Utils::getDecryptedZviceID(GETVAL($request_params, "zbotid")) : null;
            if (isset($linkedZviceID) && isset($linkedZbotID) && !Utils::isSuperAdmin(getLoggedUserID()) && $linkedZviceID === $linkedZbotID) {
                $response = [];
                $response["error"] = true;
                $response["message"] = 'You cannot replace organisation tag. Please contact support.';
                echoResponse(200, $response);
            }

            // The API has been modified to be used such that $zviceID can be sent as a body parameter
            if ($zviceid == null) {
                $zviceid = GETVAL($request_params, 'zviceid');
                if ($zviceid == null) {
                    // Neither the API URI contains the zviceid
                    // nor the body param. Return failure
                    $autoGenTag = GETVAL($request_params, 'autogentag');
                    if ($autoGenTag == "true" && !is_null($linkedZbotID)) {
                        // Generate the tag Automatically if needed
                        // Generate the tag by customer provisioning get Next Available Tag API handler
                        $orgZbotID = getOrgZviceID($linkedZbotID);
                        $newProvisionedTagIDArr = (new \TwigMeServerApplication\provisioning\CustomerProvisioningController())->getNextAvailableTagsHelper(1, Utils::getEncryptedZviceID($orgZbotID));
                        $newProvisionedTagID = $newProvisionedTagIDArr[0];
                        $zviceid = $newProvisionedTagID;
                    } else {
                        $response = [];
                        $response["error"] = true;
                        $response["message"] = 'No TagID provided';
                        echoResponse(200, $response);
                    }
                }
            }

            // Set the default title/desc to tagID, if they are empty
            $title = GETVAL($request_params, 'title');
            $desc = GETVAL($request_params, 'zviceinfo');
            if ($title == null || $title == "") {
                $request_params['title'] = $zviceid;
            }
            if ($desc == null || $desc == "") {
//                $request_params['zviceinfo'] = $zviceid;
                $request_params['zviceinfo'] = "";
            }

            // If department is being added, then the tagprofilestr field would be set, which we need to
            // reverse to tagProfile
            if (isset($request_params['tagprofilestr']) && !isset($request_params['tagprofile'])) {
                $request_params['tagprofile'] = Profile::getProfileIDFromProfileString($request_params['tagprofilestr']);
            }

            $moreInfo = GETVAL($request_params, 'moreInfo');
            if ($moreInfo != null) {
                $zbotMoreInfoObjs = [];
                $tagProfile = GETVAL($request_params, 'tagprofile');
                $zbotid = GETVAL($request_params, 'zbotid');
                if (!empty($zbotid)) {
                    $deczbotid = hexdec(bf64_decrypt($zbotid, GLOBALZVICEKEY));
                    //Fetch more info from zbot_more_info table
                    $attributes = [
                        [
                            'column_name' => 'ZbotID',
                            'value' => $deczbotid
                        ],
                        [
                            'column_name' => 'TagProfile',
                            'value' => $tagProfile
                        ]
                    ];
                    $query = "SELECT ZbotID, TagProfile, MoreInfoHeader FROM zbot_more_info WHERE ZbotID = ? AND TagProfile = ?";
                    $dbConnector = getPartitionDBHandle($deczbotid, "attrib");
                    $zbotMoreInfoObjs = \TwigMeServerApplication\zbot\ZbotMoreInfo::queryModel($dbConnector, $query)->findAll($attributes);
                }
                if (!empty($zbotMoreInfoObjs)) {
                    $allowedFields = [];
                    foreach ($zbotMoreInfoObjs as $zbotMoreInfoObj) {
                        $allowedFields[] = $zbotMoreInfoObj->MoreInfoHeader;
                    }
                    $moreInfo = Utils::getFilteredInput(
                        $moreInfo,
                        $allowedFields
                    );
                    $request_params['moreInfo'] = $moreInfo;
                } else {
                    unset($request_params['moreInfo']);
                }
            }

            // Setting the body/_REQUEST back to updated value, so validateRequiredParams doesn't fail
            if ($contentType != NULL && ($contentType == "application/json" || $contentType == "application/json;charset=utf-8")) {
                // This is a json request
                $env = $app->environment;
                $env['slim.input_original'] = $env['slim.input'];
                $env['slim.input'] = json_encode($request_params);
            } else {
                $_REQUEST = json_encode($request_params);
            }

            list($code, $response) = zviceRegisterAPIHelper($app, $zviceid, $request_params);

            // If the response is okay, now is the right time to upload the image, if it is sent
            if ($response['error'] == false) {
                if (IS_FILE_EXISTS($request_params)) {
                    $deczviceid = hexdec(bf64_decrypt($zviceid, GLOBALZVICEKEY));
                    try {
                        uploadTagBaseImage($deczviceid, $request_params);
                    } catch (TwigException $te) {
                        // Registration succeeded, but image upload failed!
                        $response['error'] = false;
                        $response['message'] = "Tag registration succeeded, but image upload failed";
                    } catch (ErrorException $eex) {
                        // Registration succeeded, but image upload failed!
                        $response['error'] = false;
                        $response['message'] = "Tag registration succeeded, but image upload failed";
                    }
                }
            }

            if ($response['error'] === false) {
                Cache::setSuccessMessage(Constants::SUCCESS_MSG_TAG_REGISTRATION);
            }
        } catch (TwigException $te) {
            $app->TwigMeLogger->logException($te);
            $code = 200;
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
        } catch (Exception $ex) {
            $app->TwigMeLogger->logException($ex);
            $code = 200;
            $response["error"] = true;
            $response["message"] = "Something went wrong, please contact support";
            $response["error_code"] = $ex->getCode();
        }
        echoResponse($code, $response);
    });


/**
 *  Click on Owned in hamburger menu will show
 *      Base cards of the businesses where the person is admin at any
 *      level.
 */
    $app->post('/:version/zvice/owned', 'authenticate', function($version) use($app) {

        global $user_id;
        $response = array();
        $success = false;
        $errormsg = "";

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        if(isset($json['pageNum'])) {
            $pageNum = $json['pageNum'];
            if($pageNum == NULL) {
                $pageNum = 0;
            }
        } else {
            $pageNum = 0;
        }

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_ZVICE_OWNED_POST
            ]
        ];
        ActivityLog::log($activityData);

        try {

            // Radhika APP UI Changes : 27-04-16
            // $zviceList = getUserTags__PP($user_id, "OWNED", NULL, $pageNum);
            $zviceList = getUserTags__PP($user_id, "REGISTERED", $pageNum);

            list($success, $errormsg, $result) = getBaseCards($zviceList, NULL, false, true /*baseContext*/);
            if($zviceList !== NULL && sizeof($zviceList) == PAGESIZE) {
                // Put another next page card
                $bodyContent = new stdClass();
                $bodyContent->pageNum = $pageNum + 1;
                $nextpageCard = new NextPageCard(PROTO . ZESTLWWW .'/' . API_VER() . '/zvice/owned', json_encode($bodyContent));
                $nextpageCard->setShowHeader("false");
                $result["elements"][] = $nextpageCard;
            }

            if ($success == true) {
                $response["pagetype"] = 6;
                $response["error"] = false;
                $response["message"] = "Operation successful";
                $response["error_code"] = -1;
                $response["data"] = $result;
                echoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = $errormsg;
                $response["error_code"] = -1;
                echoResponse(200, $response);
            }

        } catch (TwigException $te)  {
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
        }
        echoResponse(200, $response);
    });

/**
 * Click on Registered in hamburger menu will show
 *  Base cards of the businesses where the person is a user
 */
    $app->post('/:version/zvice/registered', 'authenticate', function($version) use($app) {

        global $user_id;
        $response = array();
        $success = false;
        $errormsg = "";

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        if(isset($json['pageNum'])) {
            $pageNum = $json['pageNum'];
            if($pageNum == NULL) {
                $pageNum = 0;
            }
        } else {
            $pageNum = 0;
        }

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_ZVICE_REGISTERED_POST
            ]
        ];
        ActivityLog::log($activityData);

        try {
//          Radhika APP UI Changes : 27-04-16
//          $zviceList = getUserTags__PP($user_id, "REGISTERED", NULL, $pageNum);
            $userID = getLoggedUserID();
            $autoItagID = getUserAutoItagID__PP($userID);
            $mysqli = getGlobalDBHandle();
            $offset = ($pageNum)*PAGESIZE;
            $limit = PAGESIZE;
            $qry = "SELECT * FROM ZviceListSummary where CurrentOwnerID = ? LIMIT " . $limit . "  OFFSET " . $offset;
            $rows = fetchRows($mysqli, $qry, "i", array($autoItagID));
            $zviceList = [];
            if(!empty($rows)) {
                foreach ($rows as $row) {
                    $zviceList[] = $row->ZviceID;
                }
            }

            list($success, $errormsg, $result) = getBaseCards($zviceList, NULL, false, true /*baseContext*/);
            if($zviceList !== NULL && sizeof($zviceList) == PAGESIZE) {
                // Put another next page card
                $bodyContent = new stdClass();
                $bodyContent->pageNum = $pageNum + 1;
                $nextpageCard = new NextPageCard(PROTO . ZESTLWWW .'/' . API_VER() . '/zvice/registered', json_encode($bodyContent));
                $nextpageCard->setShowHeader("false");
                $result["elements"][] = $nextpageCard;
            }

            if ($success == true) {
                $response["pagetype"] = 6;
                $response["error"] = false;
                $response["message"] = "Operation successful";
                $response["error_code"] = -1;
                $response["data"] = $result;
                echoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = $errormsg;
                $response["error_code"] = -1;
                echoResponse(200, $response);
            }

        } catch (TwigException $te)  {
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
        }
        echoResponse(200, $response);
    });



    $app->post('/:version/zvice/following', 'authenticate', function($version) use($app) {

        global $user_id;
        $response = array();
        $success = false;
        $errormsg = "";

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        if(isset($json['pageNum'])) {
            $pageNum = $json['pageNum'];
            if($pageNum == NULL) {
                $pageNum = 0;
            }
        } else {
            $pageNum = 0;
        }

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_ZVICE_FOLLOWING_POST
            ]
        ];
        ActivityLog::log($activityData);

        try {

            $zviceList = getUserTags__PP($user_id, "FOLLOWING", $pageNum);

            list($success, $errormsg, $result) = getBaseCards($zviceList, NULL, false, true /*baseContext*/);
            if($zviceList !== NULL && sizeof($zviceList) == PAGESIZE) {
                // Put another next page card
                $bodyContent = new stdClass();
                $bodyContent->pageNum = $pageNum + 1;
                $nextpageCard = new NextPageCard(PROTO . ZESTLWWW .'/' . API_VER() . '/zvice/following', json_encode($bodyContent));
                $nextpageCard->setShowHeader("false");
                $result["elements"][] = $nextpageCard;
            }

            if ($success == true) {
                $response["pagetype"] = 6;
                $response["error"] = false;
                $response["message"] = "Operation successful";
                $response["error_code"] = -1;
                $response["data"] = $result;
                echoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = $errormsg;
                $response["error_code"] = -1;
                echoResponse(200, $response);
            }

        } catch (TwigException $te)  {
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
        }
        echoResponse(200, $response);
    });

    $app->post('/:version/zvice/favorites', 'authenticate', function($version) use($app) {

        global $user_id;
        $response = array();
        $success = false;
        $errormsg = "";

        $json = $app->request->getBody();
        $json = json_decode($json, true);

        if(isset($json['pageNum'])) {
            $pageNum = $json['pageNum'];
            if($pageNum == NULL) {
                $pageNum = 0;
            }
        } else {
            $pageNum = 0;
        }

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $json,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_ZVICE_FAVOURITES_POST
            ]
        ];
        ActivityLog::log($activityData);

        try {
            $zviceList = getUserTags__PP($user_id, "FAVOURITE", $pageNum);

            list($success, $errormsg, $result) = getBaseCards($zviceList, NULL, false, true /*baseContext*/);

            if($zviceList !== NULL && sizeof($zviceList) == PAGESIZE) {
                // Put another next page card
                $bodyContent = new stdClass();
                $bodyContent->pageNum = $pageNum + 1;
                $nextpageCard = new NextPageCard(PROTO . ZESTLWWW .'/' . API_VER() . '/zvice/favorites', json_encode($bodyContent));
                $nextpageCard->setShowHeader("false");
                $result["elements"][] = $nextpageCard;
            } else if(empty($result["elements"]) && \TwigMeServerApplication\utility\HeaderUtils::isUserAgentIOS()) {
                // Check if the thing has come from iOS.
                // Add TwigMe User Profile Base Card if no favourites are found
                $itagID = getUserAutoItagID__PP(getLoggedUserID());
                list($success, $errormsg, $result) = getBaseCards([$itagID], NULL, false, true /*baseContext*/);
            }

            if ($success == true) {
                $response["pagetype"] = 1;
                $response["error"] = false;
                $response["message"] = "Operation successful";
                $response["error_code"] = -1;
                $response["data"] = $result;
                echoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = $errormsg;
                $response["error_code"] = -1;
                echoResponse(200, $response);
            }

        } catch (TwigException $te)  {
            $response["error"] = true;
            $response["message"] = $te->getMessage();
            $response["error_code"] = $te->getCode();
        }
        echoResponse(200, $response);
    });

    /**
     * Add/remove ztag favourites
     * method PUT
     * favourite
     * url - /ztag/favourite/:id
     */
    $app->put('/:version/ztag/favourite/:id', 'authenticate', function($version, $ZviceID) use($app) {

        $contentType = strtolower($_SERVER["CONTENT_TYPE"]);
        if($contentType != NULL && ($contentType == "application/json" || $contentType == "application/json;charset=utf-8" )) {
            // This is a json request
            $request_params = json_decode($app->request->getBody(), true);
            $favourite = GETVAL($request_params, 'currentvalue');
        } else {
            $request_params = $_REQUEST;
            // Handling PUT request params
            if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                $app = \Slim\Slim::getInstance();
                parse_str($app->request()->getBody(), $request_params);
            }
            // check for required params
            verifyRequiredParams(array('favourite'));
            $favourite = GETVAL($request_params, 'favourite');
        }

        //Log the activity here
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_UPDATE,
            'RequestBody' => $request_params,
            'MetaData'    => [
                'Operation'  => ActivityLog::OPERATION_ZTAGS_FAVOURITES_POST
            ]
        ];
        ActivityLog::log($activityData);

        global $user_id;
        $response = array();
        $success = false;
        // PPDEL
        // $favourite = $app->request->put('favourite');
        // Use the request_params as the PUT content type could be
        // form or json

        if($favourite === false) {
            $favourite = "NO";
        } else if ($favourite === true) {
            $favourite = "YES";
        }
        $userid = $user_id;

        verifyParamValues('favourite', $favourite, array('YES', 'NO'));

        $deczviceid = hexdec(bf64_decrypt($ZviceID,GLOBALZVICEKEY));
        if(($deczviceid < ZVICEIDMIN) || ($deczviceid > ZVICEIDMAX))
        {
            // This is not a valid zvice id.
            $response["error"] = true;
            $response["message"] = "This is not a valid Zvice ID.";
            echoResponse(200, $response);
            $app->stop();
        }


        if($favourite == 'YES')
        {
            // userztag table always lists the tag that was accessed, not the linked version.
            list($success, $errormsg) = updateUserZtags($userid, $deczviceid, 'FAVOURITE', 0, 0, NULL);

            // Send the notification to the registered Owner
            $bodyContent = new stdClass();
            $autoItagID = getUserAutoItagID__PP($userid);
            $bodyContent->tagIds = [$ZviceID, Utils::getEncryptedZviceID($autoItagID)];
            $url = Utils::buildUrl("/zvice/basecard");
            $method = "POST";
            $jsondata = json_encode($bodyContent);
            // pushNotificationToAllUsers($userIDList, $title, $msg, $url=NULL, $method=NULL, $jsondata=NULL, $img=NULL, $actions=NULL);
            $userRows = getUserNameForUserID([$userid]);
            if($userRows !== NULL && sizeof($userRows) > 0) {
                $userName = $userRows[0]->UserName;
                list ($zviceObj, $NULL) = getAllZviceInfo($deczviceid, $userid, GLOBALZVICEKEY, Constants::GET_IMMEDIATE_PARENT);
                $regOwnerIDArr = getAllRegisteredOwners($deczviceid);
                $orgDetails = ZviceListModelHelper::getOrganisationDetailsFromZviceID($deczviceid);
                //$message = "$userName has favourited $zviceObj->Title";
                $message = \TwigMeServerApplication\utility\NotificationUtils::getNotificationMsg($orgDetails->Title, "$userName has favourited $zviceObj->Title");
                $details = "$userName has favourited $zviceObj->Title";
                $img     = $zviceObj->ProfilePic;
                $notifExtraParams = [
                    "OrgZviceID"  => getOrgZviceID($deczviceid),
                    "GroupSuffix" => GroupNotification::GROUP_SUFFIX_FAVOURITES
                ];
                pushNotificationToAllUsers(
                    $regOwnerIDArr, $message, $details, $url, $method, $jsondata, $img, NULL, $notifExtraParams
                );
                // Now send the messages through chat also
                // pushChatMessageToAllUsers($regOwnerIDArr, $message);
            }
        }
        else
        {
            // userztag table always lists the tag that was accessed, not the linked version.
            list($success, $errormsg) = updateUserZtags($userid, $deczviceid, 'NOTFAVOURITE', 0, 0, NULL);
        }

        if($success == false) {
            $response["error"] = true;
            $response["message"] = "Error: ".$errormsg;
            echoResponse(200, $response);
            $app->stop();
        }

        $response["error"] = false;
        $response["message"] = "Zvice tag favourites status updated.";

        echoResponse(200, $response);

    });


    /**
     *** Ultimately, /ztag/notes_PP should become the defacto /ztag/notes API
     **/
    $app->put('/:version/ztag/notes_PP/:id', 'ENABLE_TRANSACTION', 'authenticate', function($version, $zviceid) use($app) {


        // check for required params
        verifyRequiredParams(array('notetype', 'lat', 'long', 'tagnotes'));

        global $user_id;
        $response = array();
        $success = false;
        $errormsg = "";


        $contentType = strtolower($_SERVER["CONTENT_TYPE"]);
        if($contentType != NULL && ($contentType == "application/json" || $contentType == "application/json;charset=utf-8" )) {
            // This is a json request
            $request_params = json_decode($app->request->getBody(), true);
        } else {
            $request_params = $_REQUEST;
            // Handling PUT request params
            if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                $app = \Slim\Slim::getInstance();
                parse_str($app->request()->getBody(), $request_params);
            }
        }


        $notetype = GETVAL($request_params, 'notetype');
        //$favourite = $app->request->put('favourite');
        $tagnotes = GETVAL($request_params, 'tagnotes');

        // TagNotes will contain JSON data, where each attribute
        // would be specific to the Note Type and the ProfileType
        // For example, the data for Library Profile (1) and
        // note type P would be something like the following:
        //	{
        //		"Timings" : "Open all days. 9 am to 7 pm",
        //	}
        // NOTE::
        // (1) "ZviceID" : "..." should not be included as its inferred
        // (2) "Favourite" : "..." should not be included as its NO by default
        // (3) "AuthorID" : "..." should not be included as its inferred
        // (4) "MediaURLs" : "..." should not be included as its sent as medianame
        // (5) "MediaType" : "..." should not be included as its sent as mediatype
        // (6) "Thumbnails" : "..." should not be included as its sent as thumbnails

        // The subsequent functions which process the note deal in objects
        // So we convert the json to object

        $notesObj = json_decode($tagnotes);

        if(json_last_error() != JSON_ERROR_NONE) {
            // The json was malformed, throw an exception
            $errormsg = "Malformed JSON String";
            $response["error"] = true;
            $response["message"] = $errormsg;
            echoResponse(200, $response);
        }

        $accessemail = GETVAL($request_params, 'accessemail');
        $ncode = GETVAL($request_params, 'ncode');
        $lat = GETVAL($request_params, 'lat');
        $long = GETVAL($request_params, 'long');
        //$ip = $app->request->put('ip');
        $ip = $_SERVER['REMOTE_ADDR']; // change 6/10/15

        $cc = GETVAL($request_params, 'cc');
        $cntr = GETVAL($request_params, 'cntr');
        $rgn = GETVAL($request_params, 'rgn');
        $city = GETVAL($request_params, 'city');
        $zip = GETVAL($request_params, 'zip');
        $mediapointer = GETVAL($request_params, 'medianame');
        $mediatype = GETVAL($request_params, 'mediatype');
        $thumbnails = GETVAL($request_params, 'thumbnails');
        $generic = GETVAL($request_params, 'generic');
        if($generic == NULL) {
            $generic = false;
        }

        $deczviceid = hexdec(bf64_decrypt($zviceid,GLOBALZVICEKEY));
        if($ncode != "" && $ncode != NULL) {
            $decncode = hexdec(bf64_decrypt($ncode,GLOBALZVICEKEY));
        } else {
            $decncode = NULL;
        }


        try {
            putZtagNotes__PP($deczviceid, $notetype, $generic, $notesObj, $accessemail, $decncode, $lat, $long,
                $ip, $cc, $cntr, $city,  $rgn, $zip, $mediapointer, $mediatype, $thumbnails);
            // 	putZtagNotes__PP($zviceid, $notetype, $tagnotes, $accessemail, $ncode, $lat, $long,
            //		$ip, $cc, $cntr, $city, $rgn, $zip, $mediapointer, $mediatype, $thumbnails)
        } catch (DBException $de) {
            $errormsg = $de->getMessage() . ", Query : " . $de->getQuery();
            $response["error"] = true;
            $response["message"] = $errormsg;
            echoResponse(200, $response);
        } catch (TwigException $te) {
            $errormsg = $te->getMessage();
            $response["error"] = true;
            $response["message"] = $errormsg;
            $response["error_code"] = $te->getCode();
            echoResponse(200, $response);
        }
        $response["error"] = false;
        $response["message"] = "Note Inserted";
        echoResponse(200, $response);
    });

    /**
     * Uploading ztag follower status
     * method PUT
     * url - /ztag/follow/ID
     */
    $app->put('/:version/ztag/follow/:id', 'authenticate', function($version, $zviceid) use($app) {

        $contentType = strtolower($_SERVER["CONTENT_TYPE"]);
        if($contentType != NULL && ($contentType == "application/json" || $contentType == "application/json;charset=utf-8" )) {
            // This is a json request
            $request_params = json_decode($app->request->getBody(), true);
            $follow = GETVAL($request_params, 'currentvalue');
        } else {
            $request_params = $_REQUEST;
            // Handling PUT request params
            if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                $app = \Slim\Slim::getInstance();
                parse_str($app->request()->getBody(), $request_params);
            }
            // check for required params
            verifyRequiredParams(array('followstatus'));
            $follow = GETVAL($request_params, 'followstatus');
        }

        //Log the activity here
        $decZviceID   = Utils::getDecryptedZviceID($zviceid);
        $OrgZviceID   = getOrgZviceID($decZviceID);
        $activityData = [
            'ActionType'  => ActivityLog::ACTION_TYPE_VIEW,
            'RequestBody' => $request_params,
            'MetaData'    => [
                'ZviceID'    => $decZviceID,
                'OrgZviceID' => $OrgZviceID,
                'Operation'  => ActivityLog::OPERATION_ZTAGS_FOLLOW_PUT
            ]
        ];
        ActivityLog::log($activityData);

        global $user_id;
        $response = array();
        $success = false;
        $errormsg = "";

        // PPDEL
        // $favourite = $app->request->put('favourite');
        // Use the request_params as the PUT content type could be
        // form or json

        if($follow === false) {
            $follow = "UNFOLLOW";
        } else if ($follow === true) {
            $follow = "FOLLOW";
        }

        verifyParamValues('follow', $follow, array('FOLLOW', 'UNFOLLOW'));

        $userid = $user_id;

        // PPDEL
        $mysqli = NULL; /*new mysqli(HOST, USER, PASSWORD, DATABASE);
			if ($mysqli->connect_error) {
				$errormsg = "Unable to connect to MySQL";
				$response["error"] = true;
				$response["message"] = $errormsg;
				$response["src"] = 'put /ztag/follow';
				echoResponse(500, $response);
			} */

        $deczviceid = hexdec(bf64_decrypt($zviceid,GLOBALZVICEKEY));
        if(($deczviceid < ZVICEIDMIN) || ($deczviceid > ZVICEIDMAX))
        {
            // This is not a valid zvice id.
            $response["error"] = true;
            $response["message"] = "This is not a valid Tag";
            echoResponse(200, $response);
            $app->stop();
        }

        list($success, $errormsg, $linkedzviceid) = getZviceLinkInfo($deczviceid, $mysqli);
        if($success == false) {
            // This is not a registered zvice id.
            $response["error"] = true;
            $response["message"] = "This is not a registered Zvice ID.";
            echoResponse(200, $response);
            $app->stop();
        }


        list($success, $errormsg) = updateZtagFollowers($userid, $linkedzviceid, $follow, $mysqli);
        if($success == true)
        {
            $response["error"] = false;
            $response["message"] = "Ztag follower status successfully updated.";
        }
        else
        {
            $response["error"] = true;
            $response["message"] = $errormsg;
            $response["src"] = 'put /ztag/follow';
            echoResponse(500, $response);
        }

        // userztag table always lists the tag that was accessed, not the linked version.
        list($success, $errormsg) = updateUserZtags($userid, $deczviceid, $follow, 0, 0, $mysqli);
        if($success == false) {
            $response["error"] = true;
            $response["message"] = $errormsg;
            $response["src"] = 'put /ztag/follow';
            echoResponse(500, $response);
        }


        echoResponse(200, $response);

    });

    /**
     * Claiming Ztag ownership
     * method PUT
     * params owneremail, ownership, status, ptagid
     * url - /ztag/ownership/:id
     */
    $app->put('/:version/ztag/ownership/:id', 'authenticate', function ($zviceid) use ($app) {
        // check for required params
        //verifyRequiredParams(array('owneremail', 'ownership'));

        global $user_id;
        $newcurrentowneremail = $app->request->put('owneremail');
        $newownership = $app->request->put('ownership');
        $newstatus = $app->request->put('status');
        $ptagid = $app->request->put('ptagid');
        $lat = $app->request->put('lat');
        $long = $app->request->put('long');

        list($success, $newCurrentUserID) = getUserID($newcurrentowneremail, NULL /*mysql*/);
        if ($success == false) {
            $errormsg = 'Error accessing user ID. Please try again.';
            $response["error"] = true;
            $response["message"] = $errormsg;
            $response["src"] = 'put /ztag/ownership';
            echoResponse(200, $response);
        }

        try {
            $itagID = getTwigMeUserIDForCurrentOwnerID($newCurrentUserID);
            list($code, $response) = putZtagOwnership__PP($zviceid, $user_id, $itagID, $newownership, $newstatus, $ptagid, $lat, $long);
            echoResponse($code, $response);
        } catch (Exception $ex) {
            $response["error"] = true;
            $response["message"] = "Failure! " . $ex->getMessage();
            $response["src"] = 'put /ztag/ownership';
            echoResponse(200, $response);
        }
    });


// examples from slim

    $app->get('/:version/hello/:name', function ($name) {
        echo "Hello, $name";
    });



/////////// 4/5/16 - HPG - copied get code from html/index.php to here. This is to support both twig.me and zestl.com and twig.me/<business-name> websites.
// This change was necessitated because of the versioning changes done by Pushkar/Lankesh earlier.

// GET route
$app->get('/:name', function ($name) {
	global $logged;
	global $user_id;
	global $mysqli;
	global $zestl;

    $UserAgent = \TwigMeServerApplication\utility\HeaderUtils::getUserAgent();

    /*
     * We redirect only if the request is originating from web
     */
    if (!(
            $UserAgent === \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_ANDROID
        ||
            $UserAgent === \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_IOS
    )) {
        /**
         * We do this only for Summer theme
         * NOTE: It's an ugly hack. Should be removed
         */
        if (strtolower($name) === "summer") {
            $app = getSlimInstance();
            $app->redirect(PROTO . ZESTLWWW . "/themes/summer.html");
        }
        /**
         * We do this only for Joystreet theme
         * NOTE: It's an ugly hack. Should be removed
         */
        if (strtolower($name) === "joystreet") {
            $app = getSlimInstance();
            $app->redirect(PROTO . ZESTLWWW . "/themes/joystreet.html");
        }
    }

	$web = 'www'.$_SERVER['SERVER_NAME'];

	if(stripos($web, 'zestl.com') !== FALSE) {
		//if($web == "www.zestl.com")
		$zestl = true;
	}
	else
		$zestl = false;

	sec_session_start();

	if (login_check($mysqli) == true) {
		$logged = 'in';
		$user_id = $_SESSION['user_id'];
	} else {
		$logged = 'out';
	}



	if($zestl == true)
	{
		$template = <<<EOT
        <BR><BR><BR>
        <BR><BR><BR>
		<p  style="text-align:center" font-size="56px">Zestl Inc.</p><BR>
		<p  style="text-align:center" font-size="28px">We will be up shortly...	</p>

EOT;

	}
	else
	{
		$deczviceid = hexdec(bf64_decrypt($name,GLOBALZVICEKEY));
		if(($deczviceid > ZVICEIDMIN) && ($deczviceid < ZVICEIDMAX))
		{
// This may be a valid zvice id. Redirect to scan. // 6/9/16 --> redirect to app store.
            //header('Location: ./scan_ztag_open.php?tag='.$name);
            //redirect to default twig me page

                if ($UserAgent == \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_ANDROID) {
                    getBaseCardsForTagIDs($name);
                } else if ($UserAgent == \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_WEB) {
                    getBaseCardsForTagIDs($name);
                } else if ($UserAgent == \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_IOS) {
                    getBaseCardsForTagIDs($name);
                }

            /*$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
            if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
                Utils::downloadApp();//header('Location: PLAY_STORE_MARKETTING_LINK');
                exit();
            }
            else
            {
                if(stripos($ua,'apple') !== false) { // && stripos($ua,'mobile') !== false) {
                    Utils::downloadApp();//header('Location: APP_STORE_MARKETTING_LINK');
                    exit();
                }
                else
                {
                    Utils::downloadApp();
                    //header('Location: https://www.twig.me');
                    exit();
                }
            }*/
            // APP_STORE_MARKETTING_LINK, PLAY_STORE_MARKETTING_LINK
        }

		if ($logged == 'in') {
			list($success, $errormsg, $user_name, $user_tag, $user_add, $user_phone, $user_info, $user_photo) = getUserInfo($user_id, $mysqli);
			if($success == false)
			{
				header('Location: ../error.php?err='.$errormsg);
				exit();
			}
		}

		list($success1, $errormsg, $userid) = getUserFromTagName($name, $mysqli);
		if($success1 == true)
		{
			// this is a valid user tagname.


			if ($logged == 'in') {
				list($success, $errormsg, $username, $usertag, $useradd, $userphone, $userinfo, $userphoto) = getUserInfo($userid, $mysqli);
				if($success == false)
				{
					header('Location: ../error.php?err='.$errormsg);
					exit();
				}
				if($userid == $user_id)
				{ // this is the user's own page
					header('Location: ../mytags.php');
					exit();
				}
				else {
					header('Location: ../mytags_public.php?ut='.$name);
					exit();
				}
			}
			else {
				header('Location: ../mytags_public.php?ut='.$name);
				exit();
			}
		}
		list($success2, $errormsg, $manufacturerid) = getManufacturerFromTagName($name, $mysqli);
		if($success2 == true)
		{
			// this is a valid user tagname.

			if ($logged == 'in') {
				list($success, $errormsg, $tagname, $picture, $info, $email, $phone, $address) = getManufacturerLimitedInfo($manufacturerID, $mysqli);
				if($success == false)
				{
					header('Location: ../error.php?err='.$errormsg);
					exit();
				}

				list($success) = checkManufacturerOwnerList($user_id, $manufacturerID, $mysqli);

				if($success == true)
				{ // Logged in user is an owner for manufacturer id.
					echo "Hello, $user_name, welcome to your manufacturer page.";
					header('Location: ../manutags.php');
					exit();
				}
				else {
					echo "Hello $user_name, welcome to manufacturer ID $manufacturerid's page.";
					header('Location: ../manutags_public.php?ut='.$name);
					exit();
				}
			}
			else {
				echo "Hello, welcome to manufacturer ID $name's page.";
				header('Location: ../manutags_public.php?ut='.$name);
				exit();
			}
		}

        list($success3, $errormsg, $OrgZviceID) = getTagFromOrgName($name,$mysqli);
        if($success3 == true){
            if ($UserAgent == \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_ANDROID || $UserAgent == \TwigMeServerApplication\utility\HeaderUtils::USER_AGENT_IOS) {
                    $OrgZviceID = Utils::getEncryptedZviceID($OrgZviceID);
                    getBaseCardsForTagIDs($OrgZviceID);
            } else {
                $OrgZviceID = Utils::getEncryptedZviceID($OrgZviceID);
                $app = getSlimInstance();
                $app->redirect(PROTO . ZESTLWWW . '/TwigMeWeb/index.html#?OrgZviceID=' . $OrgZviceID);
                exit();
            }
        }
		if(($success1 == false) && ($success2 == false) && ($success3 == false))
		{
			// This is not a valide tagname nor a valid ztag id. Go to home page.
			if ($logged == 'in') {
				header('Location: ../user_home.php');
				exit();
			}
			else {
                Utils::downloadApp();
				//header('Location: ../index.html');
				exit();
			}
		}
		/*	if ($logged == 'in') {
                echo "<h1  style='text-align:center'>Welcome ".$_SESSION['username']."! </h1>";
                echo "Hello, $user_id";
            }
                echo "Hello, $name";
        */
	}

});


// POST route
$app->post('/', function () {
	global $logged;
	global $zestl;
	global $error;
	global $info;

	$web = 'www'.$_SERVER['SERVER_NAME'];

	if(stripos($web, 'zestl.com') !== FALSE) {
		//if($web == "www.zestl.com")
		$zestl = true;
	}
	else
		$zestl = false;

	sec_session_start();
        $mysqli = null;
	if (login_check($mysqli) == true) {
		$logged = 'in';
		$user_id = $_SESSION['user_id'];
	} else {
		$logged = 'out';
	}




	if($zestl == true)
	{
		$name = $_POST["name"];
		$email = $_POST["email"];
		$phone = "";
		$comments = $_POST["comments"];

		//$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
		$mysqli = getGlobalDBHandle();
		$inst_stmt = $mysqli->prepare("INSERT INTO comments (Name, Email, Phone, Comments, CreatedDateTime) VALUES (?,?,?,?, now())");

		if ($inst_stmt) {

			$inst_stmt->bind_param('ssss', $name, $email, $phone, $comments);
			// Execute the prepared query.
			$inst_stmt->execute();
		}
		$inst_stmt->close() ;

		//Send email

		$useremail = ZESTLEMAIL;
		$headers  = "From: ".ZESTLEMAIL."\n";
		$headers .= "Reply-to: " . ZESTL_REPLY_TO_EMAIL . "\r\n";
		$subject = "Zestl - New comment.";

		$message = "Dear Zestl Owner,\n\n";
		$message .= "You received a new comment from: ".$name."\n\n";
		$message .= $comments."\n\n";
		$message .= "Info: ".$phone.", ".$email."\n";


		$result = mail($useremail, $subject, $message, $headers);

		if ($result) {
			// Login success
			header("Location: ../index.html");
			exit();
		} else {
			// Login failed
			header('Location: ../index.html');
			exit();
		}


	}

}
);

// GET route
$app->get('/', function () {
	global $logged;
	global $zestl;
    global $toc;
	global $error;
	global $info;
	global $mysqli;

	$web = 'www'.$_SERVER['SERVER_NAME'];

	if(stripos($web, 'zestl.com') !== FALSE) {
		//if($web == "www.zestl.com")
		$zestl = true;
	}
	else
		$zestl = false;

    if(stripos($web, 'manukahoneyindia.com') !== FALSE) {
        //if($web == "www.theorganiccarbon.com")
        $toc = true;
    }
    else
        $toc = false;

    if(stripos($web, 'future') !== FALSE) {
        //if($web == "www.theorganiccarbon.com")
        $future = true;
    }
    else
        $future = false;

	sec_session_start();

	if (login_check($mysqli) == true) {
		$logged = 'in';
		$user_id = $_SESSION['user_id'];
	} else {
		$logged = 'out';
	}



	if($zestl == true)
	{
		header('Location: ../zestl.php');
		exit();
	} else if($toc == true) {
        header('Location: ../jargo/index.html#/main');
        exit();
    } else if($future == true) {
        header('Location: ../futuregroup/index.html');
        exit();
    } else
	{
		if ($logged == 'in') {
			header('Location: ../user_home.php');
			exit();
		}

		//$template = <<<EOT
//EOT;
		//echo $template;

		header('Location: ../index.html');
		exit();

	}

}
);


/////////////////////TEST ONLY //////////////////

    /**
     * TEST
     */
    $app->get('/:version/tagid/:id', function($version, $tagid) use($app) {
        $response = array();

        $a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //$k = $app->request->get('k') % 26;	// modulo 26
        $count = $app->request->get('ct');
        $doenc = $app->request->get('enc');
        $k = 0;
        $m=0;
        if($count == "") $count = 1;

        if($doenc == 1)
        {
            for($i = $tagid; $i < ($tagid + $count); $i++)
            {
                // check if this is a valid zbot id.
                $enc = bf64_encrypt(dechex($i),GLOBALZVICEKEY);
                //$enc1 = bf64_encrypt(($tagid),GLOBALZVICEKEY);
                //$dec1 = (bf64_decrypt($tagid,GLOBALZVICEKEY));

                $response["enc"][$i] = $enc;
                $response["ct"][$i] = $a[$m%26].$k;
                //$response["enc1"] = $enc1;
                //$response["dec1"] = $dec1;
                if($k == 99)
                {
                    $k = 0;
                    $m++;
                }
                else $k++;
            }
        }

        $dec = hexdec(bf64_decrypt($tagid,GLOBALZVICEKEY));
        $response["error"] = false;
        $response["dec"] = $dec;

        echoResponse(404, $response);
    });

?>
