(function() {

    var app = angular.module("jargoViewer");

    //var ProductController = function($scope) {
    var ProductsSearchController = function($scope, $routeParams, $location, ProdSearchService, UserCart, globalStore) {
        // Used to show Loading bar
        // $scope.projects_loading = true;
        $scope.globalStore = globalStore;

        // This controller would intercept the routes which are
        // /products/search/page/:page?searchtext=ghee

        // initialize page and searchtext
        $scope.page = 0;
        $scope.searchtext = '';

        if (('page' in $routeParams) && !isNaN($routeParams.page)) {
            $scope.page = parseInt($routeParams.page);
        }
        // Check if the router Param contains the field searchtext, if so, check if its a string
        if (('searchtext' in $routeParams) && (typeof $routeParams.searchtext === 'string')) {
            $scope.searchtext = $routeParams.searchtext;
        }

        console.log('scope page : ' + $scope.page + ', searchtext : ' + $scope.searchtext);

        // Anything that you code below would execute immediately even if the
        // response of the getData(..) promise has not returned
        $scope.userCart = UserCart.cart;

        $scope.addToCart = function(prodID, prodURLID, costPerUnit, quantity, itemName) {
           UserCart.addProdInCart(prodID, prodURLID, costPerUnit, quantity, itemName);
        };

        $scope.remInCart = function(prodID, quantity) {
           UserCart.delProdInCart(prodID, quantity);
        };

        // Called from 'Search All Products', Next Page, and Previous Page
        $scope.search = function(searchtext, page) {
            console.log('searchtext : ' + searchtext + ', page : ' + page);
            // Change the search text and pagenum accordingly
            ProdSearchService.changeSearch(searchtext, page);
        };

        // Call the Product Search Service to actually fetch the data
        var myDataPromise = ProdSearchService.searchProducts($scope.searchtext, $scope.page);
        myDataPromise.then(function(result) {
            console.log(result);
            // this is only run after getData() resolves
            $scope.products = result.products;
            $scope.more = result.more;
            // REST API has returned the response. Stop the Loading
            // $scope.projects_loading = false;
        });

    };

    app.controller("ProductsSearchController", ProductsSearchController);

}());
