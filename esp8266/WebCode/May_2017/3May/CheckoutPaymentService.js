/**
Filename : CheckoutPaymentService.js

Contains the HTTP API invocations to be made for the following:

a) Checkout Cart : To send the Cart information and the User Address to backend
b) Payment : Before the Payment is to be made, and after checkout is done, API
call is done to show the final payment page

**/

(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");

    // Create the factory
    app.factory('CheckoutPaymentService', function($routeParams, $route, $location, $q, UserCart, RestService) {

        // Checkout (Products added in) cart
        // Response :
        // {
        //      'error'   : false,
        //      'data'  : {
        //          'checkoutid'  : 3456      // Integer
        //      }
        // }
        var checkout = function(userInfo) {
            console.log("Came inside CheckoutPaymentService[checkoutCart], userInfo :"+userInfo);
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "post",
                url: "/cart/checkout",
                // params: {
                //     userinfo: userInfo,
                //     cart: UserCart.cart
                // },
                data: {
                    userinfo: userInfo,
                    cart: UserCart.cart.cart_products
                }
            });
        };

        // Get Cart Checkout Details
        // Response :
        // {
        //      'error' : false,
        //      'data' : {
        //          'rzp_orderid' : 'rzp_A34RFhgs45',     // RazorPay OrderID - string
        //          'products' : [
        //              {
        //                  'id'    : 101,                // integer
        //                  'urlid' : 'Ghee-1KG-Amul'     // string
        //                  'mrp'   : 1499.0,             // float
        //                  'units' : 3,                  // integer
        //                  'title' : 'Teddy Bear (M)',   // String
        //                  'tax_percentage' : 10.0,      // Float
        //                  'tax_per_unit' : 149.0,       // float
        //                  'total_tax' : 447.0           // float
        //                  'total_incl_tax' : 4944.0     // float
        //              }, ...
        //          ],
        //          'total_excl_tax' : 4497.0,            // float
        //          'tax' : 447.0,                        // float
        //          'total' : 4944.0                      // float
        //          'delivery_address' : {
        //              'name'  : '...',
        //              'line1' : '...',
        //              'line2' : '...',
        //              'city' : '...',
        //              'mobile' : '...',
        //              'email' : '...'
        //          }
        //      }
        // }
        var getCheckoutDetails = function(checkoutid) {
            console.log("CheckoutPaymentService[getCheckoutDetails], checkoutid :"+checkoutid);
            // Now make the http API hit to fetch the cart info
            return RestService.invoke_api({
                method: "get",
                url: "/cart/"+checkoutid,
            });
        };

        // Get Order Details (Response is similar to GET Cart info)
        // Response :
        // {
        //      'error' : false,
        //      'data' : {
        //          'rzp_orderid' : 'rzp_A34RFhgs45',     // RazorPay OrderID - string
        //          'products' : [
        //              {
        //                  'id'    : 101,                // integer
        //                  'urlid' : 'Ghee-1KG-Amul',    // string
        //                  'mrp'   : 1499.0,             // float
        //                  'units' : 3,                  // integer
        //                  'title' : 'Teddy Bear (M)',   // String
        //                  'tax_percentage' : 10.0,      // Float
        //                  'tax_per_unit' : 149.0,       // float
        //                  'total_tax' : 447.0           // float
        //                  'total_incl_tax' : 4944.0     // float
        //              }, ...
        //          ],
        //          'total_excl_tax' : 4497.0,            // float
        //          'tax' : 447.0,                        // float
        //          'total' : 4944.0                      // float
        //          'delivery_address' : {
        //              'name'  : '...',
        //              'line1' : '...',
        //              'line2' : '...',
        //              'city' : '...',
        //              'mobile' : '...',
        //              'email' : '...'
        //          }
        //      }
        // }
        var getOrderDetails = function(checkoutid) {
            console.log("CheckoutPaymentService[getOrderDetails], checkoutid :"+checkoutid);
            // Now make the http API hit to fetch the cart info
            return RestService.invoke_api({
                method: "get",
                url: "/orders/"+checkoutid,
            });
        };

        // Get all the Orders for the currently logged in user
        //
        //  This is the response
        //
        //  'orders' : [
        //      {
        //          'rzp_orderid' : 'rzp_A34RFhgs45',     // RazorPay OrderID - string
        //          'products' : [
        //              {
        //                  'id'    : 101,                // integer
        //                  'mrp'   : 1499.0,             // float
        //                  'units' : 3,                  // integer
        //                  'title' : 'Teddy Bear (M)',   // String
        //                  'tax_percentage' : 10.0,      // Float
        //                  'tax_per_unit' : 149.0,       // float
        //                  'total_tax' : 447.0           // float
        //                  'total_incl_tax' : 4944.0     // float
        //              }, ...
        //          ],
        //          'total_excl_tax' : 4497.0,            // float
        //          'tax' : 447.0,                        // float
        //          'total' : 4944.0                      // float
        //          'delivery_address' : {
        //              'name'  : '...',
        //              'line1' : '...',
        //              'line2' : '...',
        //              'city'  : '...',
        //              'mobile': '...',
        //              'email' : '...'
        //          },
        //          'date_created' : '2018-01-05 12:05:54'
        //      }
        //  ]
        var getAllOrderDetails = function() {
            console.log("CheckoutPaymentService[getAllOrderDetails]");
            // Now make the http API hit to fetch the cart info
            return RestService.invoke_api({
                method: "get",
                url: "/myorders",
            });
        };

        // Handler when Payment has succeeded from Razor Pay
        var razorPayTransactionComplete = function(transaction, cartAmount, checkoutid) {
            //    transaction :
            //
            //    {
            //        "razorpay_payment_id": "pay_xxxxxxxxxxxxxx",
            //        "razorpay_signature": "<Hexadecimal String>",
            //        “razorpay_order_id”: “order_xxxxxxxxxxxxxx”
            //    }
            console.log('[razorPayTransactionComplete] transaction : ' + JSON.stringify(transaction, null, 4) + ', cartAmount : ' + cartAmount);
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "post",
                url: "/cart/payment/"+checkoutid,
                data: {
                    transaction:  transaction,
                    amount : cartAmount
                }
            })
            .then(function(result) {
                // The processing has completed, time to redirect to the
                // orders/:orderid
                $location.path("orders/"+checkoutid);
            });
        };

        return {
            checkout : checkout,
            getCheckoutDetails : getCheckoutDetails,
            getAllOrderDetails : getAllOrderDetails,
            getOrderDetails : getOrderDetails,
            razorPayTransactionComplete : razorPayTransactionComplete
        };
    });
}());
