(function() {

    var app = angular.module("jargoViewer");

    var CheckoutController = function($scope, $http, $q, $location, $routeParams, UserCart, CheckoutPaymentService) {
        $scope.user = {};

        $scope.checkout = function () {
            // Save the user info as found from the Address Information
            // into the UserCart Service
            //
            UserCart.setUserInfo($scope.user);
            // Now is the time to also store this infomation inside server
            // Store the products added in the Cart, and the User Information
            // into the Server against the User
            // On the backend, the cart info along with the User Info is stored,
            // and Payment Gateway's Orders API is created
            CheckoutPaymentService.checkout($scope.user).then(function(successData) {
                //    'data'  : {
                //        'checkoutid'  : 3456      // Integer
                //    }
                checkoutid = successData.checkoutid;

                // This is the correct place to change the location route to payments
                // If Success,
                // 1. Change the Route, and point to the /cart/payment
                // So that the payment page could be processed
                $location.path("cart/payment/"+checkoutid);
                // 2. Also, clear the Cart items from the UI
                UserCart.resetCart();
            });
        };
    };

    app.controller("CheckoutController", CheckoutController);

}());
