(function() {

    var app = angular.module("jargoViewer");

    var MainController = function($scope, RestService) {
        $scope.contact = {
            'name' : '',
            'mobile' : '',
            'email' : '',
            'message' : ''
        };

        $scope.submitContactUsForm = function(contact) {
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "post",
                url: "/contactus",
                // params: {
                //     userinfo: userInfo,
                //     cart: UserCart.cart
                // },
                data: {
                    contact: contact,
                }
            }).finally(function(){
                // Reset to the default after submission!
                $scope.contact.name = '';
                $scope.contact.mobile = '';
                $scope.contact.email = '';
                $scope.contact.message = '';
            });
        };
    };

    app.controller("MainController", MainController);

}());
