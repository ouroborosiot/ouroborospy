(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");

    // Create the factory that share the User Cart with various controllers
    app.factory('UserCart', function($cookieStore) {
        var cart = {
            cart_val: 0,
            cart_size : 0,
            cart_products : {},
        };

        var user = {};

        // Check if the cookie is already set for Cart
        // and if set, initialize the cart
        initializeCartFromCookie();

        var addProductInCart = function(prodID, prodUrlID, prodCostPerUnit, prodQuantity, prodName) {
            if((prodID in cart.cart_products)) {
                // true if "prodID" exist in cart_items
                // Add the new prodID key element now
                cartProdObj = cart.cart_products[prodID];
                cartProdObj.Quantity = cartProdObj.Quantity + prodQuantity;
            } else {
                // A product with same key doesnt exists
                cart.cart_products[prodID] = {
                    'Quantity' : prodQuantity,
                    'CostPerUnit' : prodCostPerUnit,
                    'ItemName' : prodName,
                    'ID' : prodID,
                    'URLID' : prodUrlID
                };
            }
            // Add the total newly added products cost to Total Cart Value
            cart.cart_val += prodCostPerUnit * prodQuantity;
            cart.cart_size += prodQuantity;
            // Now add/update the entire cart into the Cookie Store
            updateCookie();
        };

        var removeProductInCart = function(prodID, prodQuantity) {
            if((prodID in cart.cart_products)) {
                // true if "prodID" exist in cart_items
                // Add the new prodID key element now
                prodObj = cart.cart_products[prodID];
                existingQuantity = prodObj.Quantity;
                prodCostPerUnit = prodObj.CostPerUnit;
                if(prodQuantity > existingQuantity) {
                   alert('No more of this item exists in the cart!');
                } else {
                    prodObj.Quantity = prodObj.Quantity - prodQuantity;
                    // Add the total newly added products cost to Total Cart Value
                    cart.cart_val -= prodCostPerUnit * prodQuantity;
                    cart.cart_size -= prodQuantity;
                    if(prodObj.Quantity < 1) {
                        // No more of this product left in cart, remove from cart list
                        delete cart.cart_products[prodID];
                    }
                }
                // Now add/update the entire cart into the Cookie Store
                updateCookie();
            } else {
                // Error
                alert('No more of this item exists in the cart!');
            }
        }

        // Called if the entire Cart has to be reset
        var resetCart = function() {
            cart.cart_val = 0;
            cart.cart_size = 0;
            cart.cart_products = {};
            // Update the cookie
            updateCookie();
        }

        ///////////// User Info Functions Start ////////////////
        function setUserInfo(userInfo) {
            user = userInfo;
        }

        function getUserInfo() {
            return user;
        }
        ///////////// User Info Functions Ends /////////////////

        ///////////// COOKIE Functions Start ////////////////

        function updateCookie() {
            // Now add/update the entire cart into the Cookie Store
            $cookieStore.put('cart', cart);
        }

        /**
         * This function initializes the cart and cart_items
         * from the cookie (if the cookie exists)
         */
        function initializeCartFromCookie() {
            // Check if the cart cookie exists
            var cartCookie = $cookieStore.get('cart');
            if(!(cartCookie == null)) {
                // Cookie exists, set to cart!
                cart = cartCookie;
            }
        }
        ///////////// COOKIE Functions End ////////////////

        // Return the Interface of UserCart
        return {
            // Cart related
            cart          : cart,
            addProdInCart : addProductInCart,
            delProdInCart : removeProductInCart,
            resetCart     : resetCart,
            // Set/Get User Info
            setUserInfo   : setUserInfo,
            getUserInfo   : getUserInfo
        };
    });
}());
