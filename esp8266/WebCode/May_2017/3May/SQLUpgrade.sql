
    public function upgrade_3_0_10($params = null)
    {
        return [

            "CREATE TABLE `JF_Items` (
                  `ItemID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                  `URLID` varchar(190) NOT NULL,
                  `Title` varchar(512) NOT NULL,
                  `ItemDescription` TEXT NOT NULL,
                  `Price` DECIMAL(15,2),
                  `ImageUrl` varchar(2048) DEFAULT NULL,
                  `WeightInGms`  DECIMAL(15,2),
                  `ItemGSTPercent`    DECIMAL(15,2),
                  `IsActive`     ENUM('YES', 'NO') DEFAULT 'YES',
                  `CreatedOn`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                  `ModifiedOn`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`ItemID`),
                  UNIQUE KEY `UQ_JF_It_URLID` (`URLID`)
            ) DEFAULT CHARSET=utf8mb4",

            "CREATE TABLE `JF_UserList` (
                  `UserID`    INT UNSIGNED NOT NULL AUTO_INCREMENT,
                  `UserPhone` varchar(16) DEFAULT NULL,
                  `UserName` varchar(256) DEFAULT '',
                  `UserPhoto` varchar(256) DEFAULT NULL,
                  `UserEmail` varchar(256) DEFAULT NULL,
                  `UserAddr` varchar(512) DEFAULT '',
                  `CreatedOn`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                  `ModifiedOn`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  `PhoneVerified` enum('YES','NO') NOT NULL DEFAULT 'NO',
                  `PhoneOTPInfo` varchar(4096) DEFAULT NULL,
                  PRIMARY KEY (`UserID`),
                  UNIQUE KEY `UQ_JF_UL_UserPhone` (`UserPhone`)
            ) DEFAULT CHARSET=utf8mb4",

            "CREATE TABLE IF NOT EXISTS `JF_UserCart` (
                `CartID`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `UserID`            INT UNSIGNED NOT NULL,
                `PaymentStatus`     ENUM('STARTED', 'PAID') DEFAULT 'STARTED',
                `CostIncTax`        DECIMAL(15,2),
                `ShippingCost`      DECIMAL(15,2),
                `OrderID`           VARCHAR(190) NOT NULL,
                `Address`           TEXT DEFAULT NULL,
                `CreatedOn`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                `ModifiedOn`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY(`CartID`),
            CONSTRAINT `FK_JF_UsrCrt_UL_UID` FOREIGN KEY (`UserID`)
                REFERENCES JF_UserList(`UserID`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            UNIQUE KEY `UQ_JF_UC_OrderID` (`OrderID`)
            )  DEFAULT CHARSET=utf8mb4",

            "CREATE TABLE IF NOT EXISTS `JF_UserCartItems` (
                `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `CartID`            INT UNSIGNED NOT NULL,
                `ItemID`            INT UNSIGNED NOT NULL,
                `ItemTitle`         TEXT DEFAULT NULL,
                `ItemQuantity`      INT UNSIGNED NOT NULL,
                `ItemPricePerUnit`  DECIMAL(15,2),
                `ItemGSTPercent`    DECIMAL(15,2),
                `ItemTaxPerUnit`    DECIMAL(15,2),
                `CreatedOn`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                `ModifiedOn`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY(`ID`),
            CONSTRAINT `FK_JF_UsrCrtItm_CI_UsrCrt_CI` FOREIGN KEY (`CartID`)
                REFERENCES JF_UserCart(`CartID`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            CONSTRAINT `FK_JF_UsrCrtItm_IID_Items_IID` FOREIGN KEY (`ItemID`)
                REFERENCES JF_Items(`ItemID`)
                ON DELETE CASCADE
                ON UPDATE CASCADE
            )  DEFAULT CHARSET=utf8mb4",

            "insert into JF_Items(Title, URLID, ItemDescription, Price, ImageUrl) values ('Desi Cow Ghee 1 KG', 'Desi-Cow-Ghee-1-KG', 'Desi Cow Ghee 1 KG - Awesome', 459.0, 'http://healthyandwholesomefoods.com/images/products/16.png')",
            "insert into JF_Items(Title, URLID, ItemDescription, Price, ImageUrl) values ('Toddler Sweater Ages 3-12 months', 'Toddler-Sweater-Ages-3-12-months', 'Toddler Sweater Ages 3-12 months - Good one', 550.0, 'https://i.pinimg.com/736x/32/a8/e9/32a8e911e72674677fb2fc755e122c9a--toddler-girls-boys.jpg')",
            "insert into JF_Items(Title, URLID, ItemDescription, Price, ImageUrl) values ('Amul Ghee 1 KG', 'Amul-Ghee-1-KG', 'Amul Ghee 1 KG - White in color', 399.0, 'https://dtgxwmigmg3gc.cloudfront.net/files/5487c76cc566d77395002166-icon-256x256.png')",
            "insert into JF_Items(Title, URLID, ItemDescription, Price, ImageUrl) values ('Organic Toor Dal 1 KG ', 'Organic-Toor-Dal-1-KG', 'Organic Toor Dal 1 KG - Orangish Red', 240.0, 'https://www.namasteplaza.com/media/catalog/product/cache/1/image/256x256/cbe61a817c5c62ebe48032852c927cce/4/6/46_2.jpg')"

            "CREATE TABLE IF NOT EXISTS `JF_ContactUs` (
                `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `UserName`          TEXT DEFAULT NULL,
                `Mobile`            TEXT DEFAULT NULL,
                `EmailID`           TEXT DEFAULT NULL,
                `Message`           TEXT DEFAULT NULL,
                `CreatedOn`         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                `ModifiedOn`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY(`ID`)
            )  DEFAULT CHARSET=utf8mb4",
        ];
    }
