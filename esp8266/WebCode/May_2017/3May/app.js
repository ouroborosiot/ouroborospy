(function(){

    // jargoViewer Create a new Angular Module
    // This would go into the html tag for index.html
    var app = angular.module("jargoViewer", ["ngRoute", "ngCookies", 'ui.bootstrap', 'angular-loading-bar', 'ngAnimate']);
    // Will indicate if HTTP request is in progress or not
    app.value('globalStore', {
        'in_progress' : false,
    });
    app.constant('CONFIG', {
        'APP_NAME' : 'Ouroboros IoT',
        'APP_VERSION' : '0.0.1',
        'GOOGLE_ANALYTICS_ID' : '',
        'BASE_URL' : 'http://ouroborosiot.com',
    })
    // Route configuration
    app.config(function($routeProvider){
        $routeProvider
            .when("/main", {
                templateUrl: "main.html",
                controller: "MainController"
            })
            .when("/water-timer", {
                templateUrl: "water-timer.html"
            })
            .when("/how-to-configure", {
                templateUrl: "how-to-configure.html"
            })
            .when("/login", {
                templateUrl: "login.html",
                controller: "LoginController"
            })
            .when("/register", {
                templateUrl: "register.html",
                controller: "RegisterController"
            })
            .when("/resetpassword", {
                templateUrl: "resetpassword.html",
                controller: "ResetController"
            })
            .when("/mydevices", {
                templateUrl: "mydevices.html",
                controller: "MyDevicesController"
            })
            // Product Search
            .when("/products/search/page/:page", {
                templateUrl: "products.html",
                controller: "ProductsSearchController",
            })
            // Product Details
            .when("/product/:productid", {
                templateUrl: "productdetails.html",
                controller: "ProductController"
            })
            // View Cart
            .when("/cart", {
                templateUrl: "Cart.html",
                controller: "NavController"
            })
            // Checkout Cart
            .when("/checkout", {
                templateUrl: "Checkout.html",
                controller: "CheckoutController"
            })
            // View Payment Page for a Checked-out Cart
            .when("/cart/payment/:checkoutid", {
                templateUrl: "CartPayment.html",
                controller: "PaymentController"
            })
            // View Order which has been paid for
            .when("/orders/:checkoutid", {
                templateUrl: "Orders.html",
                controller: "OrdersController"
            })
            // View All My Orders
            .when("/myorders", {
                templateUrl: "MyOrders.html",
                controller: "MyOrdersController"
            })
            .when("/terms", {
                templateUrl: "TermsOfService.html"
            })
            .when("/privacy", {
                templateUrl: "Privacy.html"
            })
            .when("/refund", {
                templateUrl: "Refund.html"
            })
            .otherwise({redirectTo:"/main"});
    });

}());
