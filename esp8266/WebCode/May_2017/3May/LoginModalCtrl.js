// LoginModalCtrl.js

(function() {

    var app = angular.module("jargoViewer");

    app.controller('LoginModalCtrl', ['$scope', '$q', 'LoginService', function ($scope, $q, LoginService) {
        // Info  https://angular-ui.github.io/bootstrap/
        // The $scope here refers to the Modal instance
        // The scope associated with modal's content is augmented with:
        //
        //  $close(result) (Type: function) - A method that can be used to close a modal, passing a result.
        //
        //  $dismiss(reason) (Type: function) - A method that can be used to dismiss a modal, passing a reason.
        //
        // Those methods make it easy to close a modal window without a need to create a dedicated controller.

        // The error message to be shown whenever any of the API fails!
        this.errorMsg = '';
        // Some info message to be shown
        this.infoMsg = '';
        // Bind the "cancel" varibale in controller to the dismissal of modal
        this.cancel = $scope.$dismiss;

        ctrl = this;

        this.submit = function (email, password) {
          // Reset the error message
            ctrl.errorMsg = '';
            this.infoMsg = '';
            // Make the Login API Call
            LoginService.login(email, password).then(
                function (user) {
                    $scope.$close(user);
                },
                function (errorMsg) {
                    // The Login has failed! Show the error message
                    ctrl.errorMsg = errorMsg;
                    return $q.reject(errorMsg);
                }
            );
        };

        this.generateOTP = function (email) {
            // Reset the error message
            ctrl.errorMsg = '';
            this.infoMsg = '';
            // Make the Generate OTP API Call
            LoginService.generateOTP(email).then(
                function (data) {
                    // OTP Sent Successfully
                    ctrl.infoMsg = 'OTP has been Successfully sent';
                },
                function (errorMsg) {
                    // The OTP Generation has failed! Show the error message
                    ctrl.errorMsg = errorMsg;
                    return $q.reject(errorMsg);
                }
            );
        };
    }]);
}());
