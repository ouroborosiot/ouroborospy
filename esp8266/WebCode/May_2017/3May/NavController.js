(function() {

    var app = angular.module("jargoViewer");

    var NavController = function($scope, $location, UserCart, ProdSearchService, LoginService) {
        // All Cart Related Code STARTS
        $scope.userCart = UserCart.cart;
        $scope.cart_products = UserCart.products_in_cart;
        $scope.searchtext = '';

        $scope.addToCart = function(prodID, costPerUnit, quantity, itemName) {
            UserCart.addProdInCart(prodID, costPerUnit, quantity, itemName);
        };

        $scope.remInCart = function(prodID, quantity) {
            UserCart.delProdInCart(prodID, quantity);
        };

        $scope.doTheBack = function() {
            window.history.back();
        };

        // Function to invoke the Prod search based on input
        /**/
        $scope.search = function() {
            console.log("searchText : " + $scope.searchtext);
            ProdSearchService.changeSearch($scope.searchtext, 0);
        };
        // All Cart Related Code ENDS

        // ALL Login related code STARTS
        $scope.userInfo = LoginService.userInfo;
        console.log($scope.userInfo);
        // scope's logout variable is pointing to the logout function
        $scope.logout = LoginService.logout;
        // ALL Login related code ENDS
    };

    app.controller("NavController", NavController);

}());
