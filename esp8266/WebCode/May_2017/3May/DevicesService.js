/**
Filename : DevicesService.js

Contains the HTTP API invocations to be made for the following:

a) Getting Devices for logged in user
b) Linking a New Device to a User Account
c) Sending Control Commands to User's Device

**/

(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");

    // Create the factory
    app.factory('DevicesService', function($routeParams, $route, $location, $q, RestService) {

        // Get all my devices
        // Response :
        //  {
        //      'error' : false,
        //      'data' : {
        //          'devices' : [
        //              {
        //                  'id'    : 101,                // integer
        //                  'identifier' : '5CCF7FA5D434' // string
        //                  'title' : 'Water Terrace',    // string
        //                  'numoutputs' : 3,             // integer
        //                  'status' : '{...}',           // String - JSON
        //                  'statusrecvtime' : 'Y-m-d h:i:s', // String - Date Time
        //                  'type' : 'TIMER',             // String
        //                  'model' : 'OUB-01'            // String
        //              }, ...
        //          ]
        //      }
        //  }
        var getMyDevices = function() {
            console.log("Came inside DevicesService[getMyDevices]");
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "get",
                url: "/iot/devices"
            });
        };

        // Get all my devices
        // Response :
        //  {
        //      'error' : false,
        //      'message' : 'Linking of Device to the User Done successfully'
        //  }
        var linkDevice = function(deviceIdentifier, accessKey) {
            console.log("Came inside CheckoutPaymentService[linkDevice], deviceIdentifier: "+deviceIdentifier+", accessKey: "+accessKey);
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "post",
                url: "/iot/device/"+deviceIdentifier+"/link",
                // params: {
                //     AccessKey: accessKey
                // },
                data: {
                    AccessKey: accessKey
                }
            });
        };

        var turn_device_on = function(deviceIdentifier, gpioIndex, how_long) {
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "get",
                url: "/iot/turnon/" + deviceIdentifier + "/" + gpioIndex + "/" + how_long,
            });
        };

        var turn_device_off = function(deviceIdentifier, gpioIndex) {
            // Now make the http API hit to fetch the products
            return RestService.invoke_api({
                method: "get",
                url: "/iot/turnoff/" + deviceIdentifier + "/" + gpioIndex,
            });
        };


        return {
            getMyDevices : getMyDevices,
            linkDevice : linkDevice,
            turn_device_on: turn_device_on,
            turn_device_off: turn_device_off
        };
    });
}());
