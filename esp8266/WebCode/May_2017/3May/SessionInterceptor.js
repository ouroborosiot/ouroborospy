// sessionInterceptor.js

(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");


    //app.factory('sessionInterceptor', ['LoginService', '$timeout', '$q', '$injector', function(LoginService, $timeout, $q, $injector) {
    app.factory('sessionInterceptor', ['$timeout', '$q', '$injector', function($timeout, $q, $injector) {
        var sessionInterceptor = {
            request: function(config) {
                var loginService;
                // this trick must be done so that we don't receive
                // `Uncaught Error: [$injector:cdep] Circular dependency found`
                //$timeout(function () {
                    loginService = $injector.get('LoginService');
                //});
                if (loginService.userInfo.authorization) {
                    config.headers['authorization'] = loginService.userInfo.authorization;
                }

                // if (LoginService.userinfo.authorization) {
                //     config.headers['authorization'] = LoginService.userInfo.authorization;
                // }

                return config;
            }
            //
            // responseError: function(response) {
            //     // Session has expired or authentication is needed
            //     if (response.status == 401) {
            //         var loginModal, loginService, $http, $location;
            //
            //         // this trick must be done so that we don't receive
            //         // `Uncaught Error: [$injector:cdep] Circular dependency found`
            //         //$timeout(function () {
            //            $http = $injector.get('$http');
            //            $location = $injector.get('$location');
            //            loginModal = $injector.get('LoginModal');
            //            // loginService = $injector.get('LoginService');
            //         //});
            //
            //         var deferred = $q.defer();
            //
            //         loginModal()
            //         .then(function (user) {
            //             deferred.resolve( $http(response.config) );
            //         })
            //         .catch(function (rejection) {
            //             // $location.path('main');
            //             deferred.reject(rejection);
            //         });
            //
            //         return deferred.promise;
            //     }
            //     return $q.reject(response);
            // }
        };
        return sessionInterceptor;
    }]);

    app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('sessionInterceptor');
    }]);


}());
