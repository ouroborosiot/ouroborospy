var gulp = require('gulp');
var gutil = require('gulp-util');
var less = require('gulp-less');
var gulpLoadPlugins = require('gulp-load-plugins');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var filter = require('gulp-filter');
var pkg = require('./package.json');

// Set the banner content
var banner = ['/*!\n',
    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license.type %> (<%= pkg.license.url %>)\n',
    ' */\n',
    ''
].join('');

const $ = gulpLoadPlugins();

// Compile LESS files from /less into /css
gulp.task('less', function() {
    var f = filter(['*', '!mixins.less', '!variables.less']);
    return gulp.src('less/*.less')
        .pipe(f)
        .pipe(less())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Minify compiled CSS
gulp.task('minify-css', ['less'], function() {
    return gulp.src('css/freelancer.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});


// Minify JS
gulp.task('minify-js', function() {
    var bowerJSFiles = ['bower_components/**/*.js', '!bower_components/**/*.min.js'];
    // var bowerMinJSFiles = ['bower_components/**/*.min.js'];
    // <script src="bower_components/angular/angular.min.js"></script>
    // <script src="bower_components/angular-animate/angular-animate.min.js"></script>
    // <script src="bower_components/angular-route/angular-route.min.js"></script>
    // <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    // <script src="bower_components/angular-cookies/angular-cookies.min.js"></script>
    // <script src="bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
    // <script src="bower_components/jquery/dist/jquery.min.js"></script>
    // <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    var bowerMinJSFiles = ['bower_components/angular/angular.min.js', 'bower_components/angular-animate/angular-animate.min.js',
      'bower_components/angular-route/angular-route.min.js', 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
      'bower_components/angular-cookies/angular-cookies.min.js', 'bower_components/angular-loading-bar/build/loading-bar.min.js',
      'bower_components/jquery/dist/jquery.min.js', 'bower_components/bootstrap/dist/js/bootstrap.min.js',
      'bower_components/jquery.easing/js/jquery.easing.min.js'];
    var bowerJSFiles = ['bower_components/angular/angular.js', 'bower_components/angular-animate/angular-animate.js',
    'bower_components/angular-route/angular-route.js', 'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'bower_components/angular-cookies/angular-cookies.js', 'bower_components/angular-loading-bar/build/loading-bar.js',
        'bower_components/jquery/dist/jquery.js', 'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/jquery.easing/js/jquery.easing.js'];
    var otherJSFiles = ['js/contact_me.js', 'js/freelancer.js', 'js/jqBootstrapValidation.js'];
    // <script src="app.js"></script>
    // <script src="RestService.js"></script>
    // <script src="UserCart.js"></script>
    // <script src="ProdSearchService.js"></script>
    // <script src="ProductsSearchController.js"></script>
    // <script src="MainController.js"></script>
    // <script src="ProductController.js"></script>
    // <script src="NavController.js"></script>
    // <script src="CheckoutController.js"></script>
    // <script src="PaymentController.js"></script>
    // <script src="LoginModal.js"></script>
    // <script src="LoginModalCtrl.js"></script>
    // <script src="LoginService.js"></script>
    // <script src="CheckoutPaymentService.js"></script>
    // <script src="OrdersController.js"></script>
    // <script src="SessionInterceptor.js">
    var myJSFiles = ['app.js', 'RestService.js', 'UserCart.js', 'ProdSearchService.js', 'ProductsSearchController.js', 'MainController.js', 'ProductController.js',
    'NavController.js', 'CheckoutController.js', 'PaymentController.js', 'LoginModal.js', 'LoginModalCtrl.js', 'LoginService.js', 'CheckoutPaymentService.js', 'OrdersController.js',
  'SessionInterceptor.js'];
    // var myJSFiles = ['*.js', '!*.min.js'];

    // Concat is a JS function to merge two arrays
    return gulp.src(
          // bowerMinJSFiles
          //bowerJSFiles
          []
          .concat(otherJSFiles)
          .concat(myJSFiles)
            // myJSFiles
        )
        .pipe(concat('scripts.js'))
        .pipe(uglify().on('error', gutil.log))
        //.pipe(header(banner, { pkg: pkg }))
        .pipe(rename('scripts.min.js'))
        .pipe(gulp.dest('dist'));
});

// Copy vendor libraries from /node_modules into /vendor
gulp.task('copy', function() {
    gulp.src(['node_modules/bootstrap/dist/**/*', '!**/npm.js', '!**/bootstrap-theme.*', '!**/*.map'])
        .pipe(gulp.dest('vendor/bootstrap'))

    gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
        .pipe(gulp.dest('vendor/jquery'))

    gulp.src([
            'node_modules/font-awesome/**',
            '!node_modules/font-awesome/**/*.map',
            '!node_modules/font-awesome/.npmignore',
            '!node_modules/font-awesome/*.txt',
            '!node_modules/font-awesome/*.md',
            '!node_modules/font-awesome/*.json'
        ])
        .pipe(gulp.dest('vendor/font-awesome'))
})

// Run everything
gulp.task('default', ['less', 'minify-css', 'minify-js', 'copy']);

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: ''
        },
    })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'less', 'minify-css', 'minify-js'], function() {
    gulp.watch('less/*.less', ['less']);
    gulp.watch('css/*.css', ['minify-css']);
    gulp.watch('js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
});
