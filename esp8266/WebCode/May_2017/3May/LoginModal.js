// LoginModal.js

(function() {
    // Get reference to the app
    var app = angular.module("jargoViewer");

    app.factory('LoginModal', function ($modal, $rootScope) {
    // app.service('LoginModal', function ($modal, $rootScope) {

        function assignCurrentUser (user) {
            $rootScope.currentUser = user;
            return user;
        }

        return function() {
            var instance = $modal.open({
                templateUrl: 'LoginModalTemplate.html',
                controller: 'LoginModalCtrl',
                controllerAs: 'LoginModalCtrl'
            });

            return instance.result.then(assignCurrentUser);
        };

    });

}());
