(function() {

    var app = angular.module("jargoViewer");

    //var ProductController = function($scope) {
    var ProductController = function($scope, $routeParams, $location, ProdSearchService, UserCart, globalStore) {
        // Used to show Loading bar
        $scope.globalStore = globalStore;
        // $scope.projects_loading = true;

        // This controller would intercept the routes which are
        // /product/:productid

        // initialize productid
        $scope.productid = null;
        $scope.prod = null;

        if ('productid' in $routeParams) {
            if(!isNaN($routeParams.productid)) {
                $scope.productid = parseInt($routeParams.productid);
            } else {
                $scope.productid = $routeParams.productid;
            }
        }

        $scope.userCart = UserCart.cart;

        $scope.addToCart = function(prodID, prodURLID, costPerUnit, quantity, itemName) {
           UserCart.addProdInCart(prodID, prodURLID, costPerUnit, quantity, itemName);
        };

        $scope.remInCart = function(prodID, quantity) {
           UserCart.delProdInCart(prodID, quantity);
        };

        // Called from 'Search All Products', Next Page, and Previous Page
        $scope.search = function(searchtext, page) {
            console.log('searchtext : ' + searchtext + ', page : ' + page);
            // Change the search text and pagenum accordingly
            ProdSearchService.changeSearch(searchtext, page);
        };

        // Call the Product Search Service to actually fetch the data
        var myDataPromise = ProdSearchService.getProduct($scope.productid);
        myDataPromise.then(function(result) {
            console.log(result);
            // this is only run after getData() resolves
            $scope.prod = result.product;
            // REST API has returned the response. Stop the Loading
            //$scope.projects_loading = false;
        });
        // Anything that you code below would execute immediately even if the
        // response of the getData(..) promise has not returned
    };

    app.controller("ProductController", ProductController);

}());
