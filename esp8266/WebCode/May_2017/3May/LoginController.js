(function() {

    var app = angular.module("jargoViewer");

    var LoginController = function($scope, $http, $q, $location, $routeParams, LoginService) {
        $scope.user = {};

        $scope.login = function () {
            LoginService.login($scope.user.email, $scope.user.password).then(function(successData) {
                // This is the correct place to change the location route to payments
                // If Success, change the Route, and point to the /mydevices
                $location.path("mydevices");
            });
      };
  };

  app.controller("LoginController", LoginController);

}());
