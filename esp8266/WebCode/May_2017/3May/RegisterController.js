(function() {

    var app = angular.module("jargoViewer");

    var RegisterController = function($scope, $http, $q, $location, $routeParams, LoginService) {
        $scope.user = {};

        $scope.register = function () {
            // params: {
            //     username: email,
            //     pwd: password,
            //     name: username,
            //     password: otp
            // },
            var payload = {
                email : $scope.user.email,
                password: $scope.user.password,
                name: $scope.user.name,
                otp: $scope.user.otp
            };
            LoginService.registerUser(payload).then(function(successData) {
                // This is the correct place to change the location route to payments
                // If Success, change the Route, and point to the /mydevices
                $location.path("mydevices");
            });
      };

      $scope.generateOTP = function () {
          LoginService.generateOTP($scope.user.email).then(function(successData) {
              // Nothing to do
          });
    };
  };

  app.controller("RegisterController", RegisterController);

}());
