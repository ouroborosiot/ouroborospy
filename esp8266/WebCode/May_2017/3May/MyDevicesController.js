(function() {

    var app = angular.module("jargoViewer");

    var MyDevicesController = function($scope, $http, $q, $location, $routeParams, DevicesService) {
        $scope.devices = {};
        $scope.linkDev = {
            'devIdentifier' : '',
            'accessKey' : ''
        };

        // Get all of My devices
        DevicesService.getMyDevices().
            then(function(data){
                $scope.devices = data.devices;
            });

        // Now set the other functions to be used
        $scope.turn_device_on = function(deviceIdentifier, gpioIndex, how_long) {
            // Now make the http API hit to fetch the products
            DevicesService.turn_device_on(deviceIdentifier, gpioIndex, how_long).
            then(function(){
                // Refresh the page?
                $location.absUrl($location.path());
                $location.url($location.path());
                $location.reload();
            });
        };

        // Now set the other functions to be used
        $scope.turn_device_off = function(deviceIdentifier, gpioIndex) {
            // Now make the http API hit to fetch the products
            DevicesService.turn_device_off(deviceIdentifier, gpioIndex).
            then(function(){
                // Refresh the page?
                $location.absUrl($location.path());
                $location.url($location.path());
                $location.reload();
            });
        };

        $scope.link_new_device = function() {
            // Now, hit the API
            DevicesService.linkDevice($scope.linkDev.devIdentifier, $scope.linkDev.accessKey).
            then(function(){
                // Refresh the page?
                $location.absUrl($location.path());
                $location.url($location.path());
                $location.reload();
            });
            // Now make the Form empty again
            $scope.linkDev.devIdentifier = '';
            $scope.linkDev.accessKey = '';
        };
  };

  app.controller("MyDevicesController", MyDevicesController);

}());
