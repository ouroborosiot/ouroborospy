<?php


require  '/var/www/html/pushkar/jf/vendor/autoload.php';

use React\Socket\ConnectionInterface;

class DeviceSchedule
{
    /**
     * {
     *      "5" : [
     *          {
     *              "start" : "12:00",
     *              "duration" : <integer>,
     *              "when" : "Once | Monday | Tuesday | ..."
     *          }
     *      ], ...
     * }
     */
    protected $schedule;
    /**
     * Upcoming schedule which is computed for a few days in advance
     * 
     * {
     *      "computed_till" : <Epoch till which the Advanced Schedule is Computed>,
     *      "upcoming_schedule" : {
     *          "5" : [
     *              {
     *                  "start_epoch" : <Epoch>
     *                  "duration" : <Time in seconds>,
     *                  "cmd_sent" : <Boolean>
     *              },
     *              ...
     *          ], ...
     *      }
     * }
     */
    protected $inAdvanceSchedule;

    public function __construct($schedule = NULL)
    {
        $this->schedule = $schedule;
        $this->computeUpcomingSchedule();
    }

    /**
     * Returns all the start epoch and the end epoch where at least the end epoch lies after the "sinceEpoch"
     * The computation is done only for 1 week
     * 
     * @param $dayOfWeek string : Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
     * @param $startTime string : "H:i" format. Ex: "23:00"
     * @param $duration integer : Duration in seconds
     * @param $sinceEpoch integer : Epoch after which we have to find the start and end epoch
     * 
     * Example  : If Monday is day of week, Start Time is 11:20, duration is 3600 seconds, and sinceEpoch is 24th Feb (Monday) 12:00 IST
     *          : In such case, it should return the epoch corresponding to 
     *          :   (a) 24th Feb 11:20 IST and 24th Feb 12:20 IST for this Monday (today),
     *          :   (b) 3rd March 11:20 IST and 3rd March 12:20 IST for next Monday, 
     *          :   (c) BUT NOTHING for the previous Monday
     * Example  : If Sunday is day of week, Start Time is 11:20, duration is 3600 seconds, and sinceEpoch is 24th Feb (Monday) 12:00 IST
     *          : In such case, it should return the epoch corresponding to 
     *          :   (a) NOTHING for the previous Sunday
     *          :   (b) 2nd March 11:20 IST and 2nd March 12:20 IST for next Sunday, 
     *          :   (c) BUT NOTHING for the previous Sunday
     */
    private function getAllStartEndEpochsForDay($dayOfWeek, $startTime, $duration, $sinceEpoch) {
        // To return array containing the [["Start":<startEpoch>, "End":<endEpoch>], ...]
        $returnStartEndArr = [];
        // Previous Week
        $prevWeekDayStartEpoch = strtotime("last $dayOfWeek $startTime");
        $prevWeekDayEndEpoch = $prevWeekDayStartEpoch + $duration;
        // This Week
        $thisWeekDayStartEpoch = strtotime("this $dayOfWeek $startTime");
        $thisWeekDayEndEpoch = $thisWeekDayStartEpoch + $duration;
        // Next Week
        $nextWeekDayStartEpoch = strtotime("next $dayOfWeek $startTime");
        $nextWeekDayEndEpoch = $nextWeekDayStartEpoch + $duration;

        // Now check if previous week's entry would lie within the sinceEpoch
        if($prevWeekDayEndEpoch > $sinceEpoch) {
            $returnStartEndArr[] = ["Start" => $prevWeekDayStartEpoch, "End" => $prevWeekDayEndEpoch];
        }
        // Now check this week's entry would lie within the sinceEpoch
        if($thisWeekDayEndEpoch > $sinceEpoch) {
            $returnStartEndArr[] = ["Start" => $thisWeekDayStartEpoch, "End" => $thisWeekDayEndEpoch];
        }
        // Add for next week:
        if($nextWeekDayStartEpoch !== $thisWeekDayStartEpoch) {
            // It may happen that this Monday and next Monday are one and the same. So in that case, dont add
            $returnStartEndArr[] = ["Start" => $nextWeekDayStartEpoch, "End" => $nextWeekDayEndEpoch];
        }

        return $returnStartEndArr;
    }

    private function computeUpcomingSchedule($computeFrom = NULL) {
        if(empty($this->schedule)) {
            // Nothing to be done
            return;
        }
        // Came here means the computation has to be done beyond $computedTill for next 1 week?
        if(empty($computeFrom)) {
            $computeFrom = time();
        }
        // Compute the in advance schedule entries from Previous Till upto next 7 days
        $computeUpto = $computeFrom + 7*24*60*60; 

        // Logic to compute the in-advance schedule Starts
        foreach ($this->schedule as $pinNumber => $pinSchedules) {
            if(!empty($pinSchedules)) {
                foreach ($pinSchedules as $scheduleIndex => $pinSchedule) {
                    $scheduleWhen = $pinSchedule["when"];
                    $scheduleStart = $pinSchedule["start"];
                    $scheduleDuration = $pinSchedule["duration"];

                    if($scheduleWhen == "Once") {
                        // Create once schedule, and blindly add it every time
                        $onceSchedule["start_epoch"] = strtotime($scheduleStart);
                        $onceSchedule["duration"] = $scheduleDuration;
                        $onceSchedule["cmd_sent"] = false;
                        $this->inAdvanceSchedule["upcoming_schedule"][$pinNumber][] = $onceSchedule;
                    } else {
                        // when is then day of the week
                        $startEndEpochArr = $this->getAllStartEndEpochsForDay($scheduleWhen, $scheduleStart, $scheduleDuration, $computeFrom);
                        if(!empty($startEndEpochArr)) {
                            foreach ($startEndEpochArr as $startEndEpoch) {
                                $startEpoch = $startEndEpoch["Start"];
                                $endEpoch = $startEndEpoch["End"]; // Redundant info
                                // Set the schedule
                                $periodicSchedule["start_epoch"] = $startEpoch;
                                $periodicSchedule["duration"] = $scheduleDuration;
                                $periodicSchedule["cmd_sent"] = false;
                                $this->inAdvanceSchedule["upcoming_schedule"][$pinNumber][] = $periodicSchedule;
                                // Update the computed upto if the end Epoch returned exceeds that
                                if($scheduleDuration > 0 && ($computeUpto < $endEpoch)) {
                                    $computeUpto = $endEpoch;
                                }
                            }
                        }
                    }
                }
            }
        }
        // Set the computed till to Today + 7 days end OR Max returned end Epoch
        $this->inAdvanceSchedule["computed_till"] = $computeUpto;
    }

    /**
     * Returns the pin number along with the duration for which the Pin has to be turned ON
     */
    public function getPinsToTurnOn() {
        // Array containing ["Pin #" => <Duration | Integer>]
        $toSendSwitchOnCmds = [];

        if(empty($this->schedule)) {
            return;
        }
        // Check if the upcoming schedule has been computed till now.
        $currTimeEpoch = time();
        if($this->inAdvanceSchedule["computed_till"] < $currTimeEpoch) {
            // Current Time has exceeded time till which the entire thing was last computed
            // Recompute from previously set "computed till" time + 1 seconds
            $this->computeUpcomingSchedule($this->inAdvanceSchedule["computed_till"] + 1);
        }

        /**
         * Upcoming schedule which is computed for a few days in advance
         * 
         * {
         *      "computed_till" : <Epoch till which the Advanced Schedule is Computed>,
         *      "upcoming_schedule" : {
         *          "5" : [
         *              {
         *                  "start_epoch" : <Epoch>
         *                  "duration" : <Time in seconds>,
         *                  "cmd_sent" : <Boolean>
         *              },
         *              ...
         *          ], ...
         *      }
         * }
         */
        if(!empty($this->inAdvanceSchedule["upcoming_schedule"])) {
            foreach ($this->inAdvanceSchedule["upcoming_schedule"] as $pinNumber => $pinSchedules) {
                if(!empty($pinSchedules)) {
                    foreach ($pinSchedules as $scheduleIndex => $pinSchedule) {
                        // Check if the pinSchedule has not already been processed
                        if($pinSchedule["cmd_sent"]) {
                            // Command has been sent for this pin, simply ignore
                            continue;
                        }
                        $duration = $pinSchedule["duration"];
                        $scheduleStartTime = $pinSchedule["start_epoch"];
                        $scheduleEndTime = $scheduleStartTime + $duration;
                        // Check if the pin needs to be turned ON based on the schedule
                        if(
                            // IF current time has exceeded start epoch
                            ($currTimeEpoch >= $scheduleStartTime) && 
                            // AND 
                            // IF End Epoch has not ended
                            (
                                ($duration == 0 ) ||      // Special case for ONCE, where its infinitely OPEN
                                ($currTimeEpoch < $scheduleEndTime) // End Date has not elapsed
                            )
                        ) {
                            // It is time to send the ON command to the device
                            // Check if the current time and the start_epoch are too far apart?
                            //      This could happen if the device was switched ON midway in a schedule
                            //      In such case, for now, consider 2 minute delay OK. 
                            // PP HACK
                            if($duration > 0) {
                                if($currTimeEpoch - $scheduleStartTime > 120) {
                                    // Decrease the duration to be used
                                    $duration = $scheduleEndTime - $currTimeEpoch;
                                } // Otherwise, let duration stay as is
                            }
                            $toSendSwitchOnCmds[$pinNumber] = $duration;
                            // Since this is going to send the Command to Turn ON, so set the cmd_sent attribute:
                            $this->inAdvanceSchedule["upcoming_schedule"][$pinNumber][$scheduleIndex]["cmd_sent"] = true;
                        }
                    }
                }
            }
        }
        // Array containing ["Pin #" => <Duration | Integer>]
        return $toSendSwitchOnCmds;
    }

    public function getStringOut() {
        error_log("schedule : " . json_encode($this->schedule)) . PHP_EOL;
        error_log("inAdvanceSchedule : " . json_encode($this->inAdvanceSchedule)) . PHP_EOL;
    }

}

/**
 * {
 *      "5" : [
 *          {
 *              "start" : "12:00",
 *              "duration" : <integer>,
 *              "when" : "Once | Monday | Tuesday | ..."
 *          }
 *      ], ...
 * }
 */

// $schedule = new stdClass();
// $pinNum = "5";
// $pinScheduleSundayA = ["start" => "14:40:00", "duration" => 3600, "when" => "Sunday"];
// $pinScheduleSundayB = ["start" => "16:00:00", "duration" => 3600, "when" => "Sunday"];
// $pinScheduleMondayA = ["start" => "12:00:00", "duration" => 3600, "when" => "Monday"];
// $pinScheduleMondayB = ["start" => "14:00:00", "duration" => 3600, "when" => "Monday"];
// $schedule->$pinNum = [$pinScheduleSundayA, $pinScheduleSundayB, $pinScheduleMondayA, $pinScheduleMondayB];
// $devSchIns = new DeviceSchedule($schedule);
// $devSchIns->getStringOut();
// $pinsToTurnON = $devSchIns->getPinsToTurnOn();
// error_log("Pins to turn ON : " . json_encode($pinsToTurnON)) . PHP_EOL;
// $devSchIns->getStringOut();
    

class IoTServer
{
    const PKT_TYPE_KEEPALIVE = 1;
    const PKT_TYPE_KEEPALIVE_RESP = 2;
    const PKT_TYPE_CMD = 3;
    const PKT_TYPE_STATUS_ACK = 4;
    const PKT_TYPE_STATUS = 10;
    const PKT_TYPE_UPDATE_SCHEDULE = 20; // Sent when the schedule for a device is updated
    const PKT_TYPE_UPDATE_SCHEDULE_ACK = 21; // Received from Device as ACK of schedule

    const CMD_SUBTYPE_ON_INDEFINITE = 1;
    const CMD_SUBTYPE_OFF_INDEFINITE = 2;
    const CMD_SUBTYPE_ON_PERIODIC = 3;
    const CMD_SUBTYPE_ON_LATER_PERIODIC = 4;
    const CMD_SUBTYPE_SET_SCHEDULE = 5;

    const TEMP_SECRET = "asdasrefwefefergf9rerf3n4r23irn1n32f";

    /**
     * This is used to keep track of when the last data pkt
     * was received for the connection
     * @var SplObjectStorage
     *  connection => last keep data received timestamp
     */
    protected $all_opened_connections;

    /** @var SplObjectStorage
     *  connection => MACAddress
     */
    protected $valid_connections;

    /** @var array
     *  MACAddress => connection
     */
    protected $macaddr_connections;

    public function __construct()
    {
        $this->valid_connections = new SplObjectStorage();
        $this->all_opened_connections = new SplObjectStorage();
        $this->macaddr_connections = [];
    }

    /**
     * This function would get called every 60 seconds. It should be used to
     * close connections which have been idle for over 2-3 minutes
     */
    public function periodicHandler() {
        echo "[" . date('Y-m-d H:i:s') ."] Starting Periodic Handler" . PHP_EOL;

        // Rewind the iterator to the beginning
        $this->all_opened_connections->rewind();

        // Connections to close due to timeout
        $close_conn_list = [];

        // Iterate
        while($this->all_opened_connections->valid()) {
            $conn = $this->all_opened_connections->current();
            $tsObj = $this->all_opened_connections->getInfo();

            // Connection TS
            $last_data_ts = $tsObj->last_data_ts;
            $valid_ts = $tsObj->valid_ts;
            $conn_ts = $tsObj->conn_ts;
            $version = isset($tsObj->version)?$tsObj->version:NULL;

            // CLOSE Connection
            // (A) If last received packet exceeds 150 sec
            // OR
            // (B) If the connection was received more than 30 sec ago
            // and connection has not become valid
            if(((time() - $last_data_ts) > 150) || (((time() - $conn_ts) > 30) && empty($valid_ts))) {
                $close_conn_list[] = $conn;
            } else {
                // Only do manual sending of periodic ON/OFF for Devices on version 2 or lower
                if(is_numeric($version) && $version <= 2) {
                    // Check the schedule and send ON (TODO : Delayed Turn ON) based on that ... 
                    if(!empty($tsObj->dev_schedule)) {
                        // Check the schedule
                        // @var $devSchedule DeviceSchedule
                        $devSchedule = $tsObj->dev_schedule;
                        // Check the pins to be invoked for Scheduling next
                        $pinsToTurnOn = $devSchedule->getPinsToTurnOn();
                        if(!empty($pinsToTurnOn)) {
                            echo "[" . date('Y-m-d H:i:s') ."] Turning ON Pins for Schedule " . json_encode($pinsToTurnOn) . PHP_EOL;
                            foreach($pinsToTurnOn as $pinNumber => $pinDuration) {
                                // Send the Packet to turn ON the Device's PinNumber for PinDuration
                                $this->sendTurnOnNow($conn, intval($pinNumber), $pinDuration);
                            }
                        }
                        // Since the device Schedule may have changed, set it back into tsObj, and attach it again
                        $tsObj->dev_schedule = $devSchedule;
                        $this->all_opened_connections->attach($conn, $tsObj);
                    }
                }
            }

            $this->all_opened_connections->next();
        }
        // Iterate over connection list now
        foreach ($close_conn_list as $conn_to_close) {
            $this->closeConnection($conn_to_close);
        }
        echo "[" . date('Y-m-d H:i:s') ."] Periodic Handler End" . PHP_EOL;
    }

    /**
     * @param ConnectionInterface $connection
     */
    public function add(ConnectionInterface $connection)
    {
        // Add new connection to the ALL Connection List
        $this->addNewConnection($connection);

        // On receiving the data we loop through other connections
        // from the pool and write this data to them
        $connection->on('data', function ($data) use ($connection) {
            $this->updateConnectionDataTS($connection);
            $this->processDataPacket($data, $connection);
        });

        // When connection closes detach it from the pool
        $connection->on('close', function() use ($connection) {
            echo "[" . date('Y-m-d H:i:s') . "] Closing connection via close event" . PHP_EOL;
            $this->closeConnection($connection);
        });

        // When connection errors out
        $connection->on('error', function (Exception $e) {
            echo 'error: ' . $e->getMessage() . PHP_EOL;
        });
    }

    /**
     * @param ConnectionInterface $connection
     */
    protected function closeConnection($connection) {
        if($this->valid_connections->contains($connection)) {
            $macAddr = $this->getConnectionData($connection);
            $this->valid_connections->detach($connection);
            unset($this->macaddr_connections[$macAddr]);
            // Log
            echo "[" . date('Y-m-d H:i:s') . "] Closing connection for MAC [$macAddr]" . PHP_EOL;
        }
        if($this->all_opened_connections->contains($connection)) {
            // Get the tsObj for the connection
            $tsObj = $this->all_opened_connections->offsetGet($connection);
            echo "[" . date('Y-m-d H:i:s') . "] Closing connection TS Info : " . json_encode($tsObj) . PHP_EOL;
            // Detach
            $this->all_opened_connections->detach($connection);
        }
        $connection->close();
    }

    protected function  addNewConnection(ConnectionInterface $connection) {
        // Add the connection to the list of all connections
        $tsObj = new stdClass();
        // The time at which latest data packet was received
        $tsObj->last_data_ts = NULL;
        // The time at which the Keep Alive packet where SHA
        // validation happens was received. This is the packet
        // where from a connection goes from Unverified to Verified
        $tsObj->valid_ts = NULL;
        // Connection TS
        $tsObj->conn_ts = time();

        $this->all_opened_connections->attach($connection, $tsObj);

        echo "[" . date('Y-m-d H:i:s') . "] Adding a NEW Connection with tsObj : " . json_encode($tsObj) . PHP_EOL;
    }

    protected function updateConnectionDataTS(ConnectionInterface $connection) {
        if($this->all_opened_connections->contains($connection)) {
            $tsObj = $this->all_opened_connections->offsetGet($connection);
            // Update the data last received with the time received
            $tsObj->last_data_ts = time();
            // Update the connection's ts info
            $this->all_opened_connections->attach($connection, $tsObj);
        }
        echo "[" . date('Y-m-d H:i:s') . "] Updating Connection Data TS tsObj : " . json_encode($tsObj) . PHP_EOL;
    }

    /**
     * Called when the Connection becomes valid, since it has passed the Validation Test!
     */
    protected function updateConnectionValidityTS(ConnectionInterface $connection, $mac, $version = NULL) {
        if($this->all_opened_connections->contains($connection)) {
            $tsObj = $this->all_opened_connections->offsetGet($connection);
            // Update the ts for validity
            $tsObj->valid_ts = time();
            // Set the version of the connection device
            $tsObj->version = $version;
            //Set mac
            $tsObj->mac = $mac;
            // Find out the schedule for this mac
            $schedule = $this->getScheduleForDevice($mac);
            $tsObj->dev_schedule = new DeviceSchedule($schedule);
            // Update the connection's ts info
            $this->all_opened_connections->attach($connection, $tsObj);
            // Send the latest Schedule Info to the device:
            if(is_numeric($version) && $version > 2) {
                $this->sendScheduleToDevice($connection, $schedule);
            }
            echo "[" . date('Y-m-d H:i:s') . "] Updating Connection Valid TS tsObj : " . json_encode($tsObj) . PHP_EOL;
        }
    }

    /**
     * Called when the schedule of a device has been updated from the UI
     */
    protected function updateConnectionSchedule(ConnectionInterface $connection, $schedule) {
        if($this->all_opened_connections->contains($connection)) {
            $tsObj = $this->all_opened_connections->offsetGet($connection);
            // Update the ts for validity
            $tsObj->dev_schedule = new DeviceSchedule($schedule);
            // Update the connection's ts info
            $this->all_opened_connections->attach($connection, $tsObj);
            // First find out the version of the device
            $version = isset($tsObj->version)?$tsObj->version:NULL;
            // Send the latest Schedule Info to the device:
            if(is_numeric($version) && $version > 2) {
                $this->sendScheduleToDevice($connection, $schedule);
            }
            echo "[" . date('Y-m-d H:i:s') . "] Updating Connection Schedule tsObj : " . json_encode($tsObj) . PHP_EOL;
        }
    }

    /**
     * Adds ONLY the valid connection - which have passed the SHA validation
     * @param ConnectionInterface $connection
     * @param mixed $mac
     */
    protected function addNewValidConnection(ConnectionInterface $connection, $mac, $ssid, $version = NULL)
    {
        echo "[" . date('Y-m-d H:i:s') . "] The Connection from MAC[$mac] is Valid! SSID[$ssid] " . PHP_EOL;

        $this->valid_connections->attach($connection, $mac);
        // Also set a reverse mapping of MAC to connection
        $macConnObj = new stdClass();
        $macConnObj->conn = $connection;
        $macConnObj->ssid = $ssid;
        $macConnObj->version = $version;

        $this->macaddr_connections[$mac] = $macConnObj;
        // Also update the TS
        $this->updateConnectionValidityTS($connection, $mac, $version);
    }

    /**
     * @param ConnectionInterface $connection
     * @return bool
     */
    protected function isMacConnected($mac)
    {
        return array_key_exists($mac, $this->macaddr_connections);
    }

    protected function getConnectionForMac($mac) {
        $connection = NULL;
        if($this->isMacConnected($mac)) {
            $macConnObj = $this->macaddr_connections[$mac];
            $connection = $macConnObj->conn;
        }
        return $connection;
    }

    /**
     * Returns the SSID from which connection was initiated
     * @param ConnectionInterface $connection
     * @return bool
     */
    protected function getSSIDForMac($mac) {
        $ssid = NULL;
        if($this->isMacConnected($mac)) {
            $macConnObj = $this->macaddr_connections[$mac];
            $ssid = $macConnObj->ssid;
        }
        return $ssid;
    }

    /**
     * Returns the macaddress associated with the connection
     * @param ConnectionInterface $connection
     * @return mixed
     */
    protected function getConnectionData(ConnectionInterface $connection)
    {
        return $this->valid_connections->offsetGet($connection);
    }

    /**
     * Returns the version associated with the valid connection
     * @param ConnectionInterface $connection
     * @return mixed
     */
    protected function getConnectionInfo(ConnectionInterface $connection)
    {
        $version = -1;
        $mac = $this->getConnectionData($connection);
        if(!empty($mac)) {
            if($this->isMacConnected($mac)) {
                $macConnObj = $this->macaddr_connections[$mac];
                $version = $macConnObj->version;
            }
        }
        return [$mac, $version];
    }

    /**
     * Gets you the first N bytes of the payload, and increases the offset of the payload
     * @param $payload
     * @param $N
     * @return bool|string
     * @throws Exception
     */
    private function getNBytes(&$payload, $N) {
        if(strlen($payload) < $N) {
            throw new Exception("Could not get $N bytes");
        }
        $response = substr($payload, 0, $N);
        $payload = substr($payload, $N);
        return $response;
    }

    private function unpack_bytes($bytes, $format) {
        list($unpacked_val) = array_values(unpack($format, $bytes));
        return $unpacked_val;
    }

    /**
     * Handle the Very first Keep alive packet received from a connection
     * If version 1: MAC (6 bytes) + TimeStamp (4 bytes) + MD5-Hash of Secret.TimeStamp (128 bytes)
     * If version 2: MAC (6 bytes) + TimeStamp (4 bytes) + MD5-Hash of Secret.TimeStamp (128 bytes) + SSID Bytes Length(2 bytes) + SSID (SSID Byte Length bytes)
     *
     * @param $version : 1/2/3
     * @param $payload : Bytes of payload
     * @param ConnectionInterface $connection
     * @throws Exception
     */
    private function processKeepAlivePacket($version, $payload, $connection) {
        try {
            // Get the first 6 bytes of mac
            $mac_bytes = $this->getNBytes($payload, 6);
            // Timestamp
            $ts_bytes = $this->getNBytes($payload, 4);
            // Hash of secret + ts
            $md5_bytes = $this->getNBytes($payload, 32);

            // Find out the MAC from bytes
            $mac = bin2hex($mac_bytes);
            // Time Stamp bytes
            // N        unsigned long (always 32 bit, big endian byte order)
            $ts = $this->unpack_bytes($ts_bytes, 'N');

            // Find if the MAC Entry is there in device info:
            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf", True);
            // Check the entry in DB for the DevID + AccessKey
            $qryFindDevice = "SELECT * from IOT_DeviceInfo where DeviceIdentifier = ?";
            $deviceDBRow = fetchSingleRowLenient($dbConn, $qryFindDevice, "s", [strtoupper($mac)]);
            if (empty($deviceDBRow)) {
                $act_code =  self::TEMP_SECRET;
            } else {
                // Came here means entry is there in DB
                $act_code =  $deviceDBRow->AccessKey;
            }
            echo "[" . date('Y-m-d H:i:s') . "] Activation Code [$act_code] for mac[$mac] ts[$ts]" . PHP_EOL;
            $dbConn->close();

            if (abs(time() - $ts) > 10 * 60) {
                // Some one trying to tamper with system by relaying older packets
                throw new Exception("Invalid Timestamp received in KeepAlive!");
            } else {
                // Compute the MD5 hash of secret.timestamp
                $combinedStr = $act_code . $ts;
                //$computedMD5 = md5($combinedStr, True);
                $computedMD5 = hash('sha256', $combinedStr, True);
                echo "[" . date('Y-m-d H:i:s') . "] computedMD5[$computedMD5] md5_bytes[$md5_bytes] " . PHP_EOL;
                if ($computedMD5 == $md5_bytes) {
                    // Valid connection request.
                    // Check if there's another connection with the
                    if ($this->isMacConnected($mac)) {
                        $old_conn = $this->getConnectionForMac($mac);
                        // Close the old connection & And start one afresh
                        echo "[" . date('Y-m-d H:i:s') . "] Closing old connection for MAC[$mac] as received a new connection" . PHP_EOL;
                        $this->closeConnection($old_conn);
                    }
                    // If version is 2, then SSID is also sent
                    $ssid = NULL;
                    if($version > 1) {
                        $ssid_bytelen_bytes = $this->getNBytes($payload, 2);
                        // n    unsigned short (always 16 bit, big endian byte order)
                        $ssid_bytelen = $this->unpack_bytes($ssid_bytelen_bytes, 'n');
                        $ssid_bytes = $this->getNBytes($payload, $ssid_bytelen);
                        // a    NUL-padded string
                        // $ssid = $this->unpack_bytes($ssid_bytes, 'a');
                        $ssid = $ssid_bytes;
                        $ssid = utf8_decode($ssid);
                    }

                    // Create the new connection now
                    $this->addNewValidConnection($connection, $mac, $ssid, $version);
                    // Time to also send back an ACK
                    // Pkt Type (2 bytes) + TimeStamp (4 bytes) + Status (1 byte)
                    $response = pack("nNC", self::PKT_TYPE_KEEPALIVE_RESP, time(), 1 /*Status*/);
                    $this->writePacket($connection, $response);
                } else {
                    // Invalid MD5, someone trying to screw with the data?
                    throw new Exception("Invalid MD5!");
                }
            }
        } catch (Exception $exception) {
            echo "[" . date('Y-m-d H:i:s') . "] Closing connection due to exception" . PHP_EOL;
            $this->closeConnection($connection);
            throw $exception;
        }
    }


    /**
     * Finds the schedule, if any for the device
     */
    private function getScheduleForDevice($mac) {
        try {
            // Find if the MAC Entry is there in device info:
            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf", True);
            // Check the entry in DB for the DevID + AccessKey
            $qryFindDevice = "SELECT DeviceSchedule from IOT_DeviceInfo where DeviceIdentifier = ?";
            $deviceDBRow = fetchSingleRowLenient($dbConn, $qryFindDevice, "s", [strtoupper($mac)]);
            if(!empty($deviceDBRow)) {
                $schedule = $deviceDBRow->DeviceSchedule;
                return json_decode($schedule, True);
            } else {
                throw new TwigException("The device is not found!");
            }
        } finally {
            if(!empty($dbConn)) {
                $dbConn->close();
            }
        }
    }

    /**
     * Server would get this once when ever the schedule has been synced with device
     * @param mixed $payload
     * @param ConnectionInterface $connection
     * @throws Exception
     */
    private function processScheduleACKPacket($data, $connection) {
        list($mac, $version) = $this->getConnectionInfo($connection);
        // Update the ScheduleSync info into the DB
        try {
            $scheduleSyncedYes = "YES";
            $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf", True);
            $setDeviceScheduleSyncedQry = "UPDATE IOT_DeviceInfo set DeviceScheduleSynced = ? WHERE DeviceIdentifier = ?";
            updateRow($dbConn, $setDeviceScheduleSyncedQry, "ss", [$scheduleSyncedYes, strtoupper($mac)]);
        } finally {
            if(!empty($dbConn)) {
                $dbConn->close();
            }
        }
    }

    /**
     * Server would send this once in a while whenever the Schedule has been modified
     * @param mixed $payload
     * @param ConnectionInterface $source_connection
     * @throws Exception
     */
    private function processScheduleUpdatedPacket($payload, $source_connection) {
        $success = true;
        try {
            // First check if the connection is made locally, and not remotely.
            // Sending Command Packet to Server is only allowed from localhost.
            $address = $source_connection->getRemoteAddress();
            $localAddr = false;
            if (!empty($address)) {
                $ip = trim(parse_url($address, PHP_URL_HOST));
                if ($ip == '127.0.0.1') {
                    $localAddr = true;
                }
            }
            if (!$localAddr) {
                $success = false;
                return 1;
            }

            // Came here means the the connection has been made locally
            // Find out the params now
            // The packet received is of the type:
            // MacAddress (6 bytes)

            // Get the first 6 bytes of mac
            $mac_bytes = $this->getNBytes($payload, 6);
            // Find out the MAC from bytes
            $mac = bin2hex($mac_bytes);

            // Find the client connection if exists
            $connection = $this->getConnectionForMac($mac);
            if (!empty($connection)) {
                // Get the Schedule State for this MAC Address Connection First
                $schedule = $this->getScheduleForDevice($mac);
                $this->updateConnectionSchedule($connection, $schedule);
                $success = true;
            } else {
                // The device is not connected, dont need to do anything in such a case:
                $success = true;
            }
        } catch(Exception $exception) {
            $success = false;
            throw $exception;
        } finally {
            if(!$success) {
                // Failure - return 0 in response
                $cmd_resp_data = pack("C", 0);
                $source_connection->end($cmd_resp_data);
            } else {
                $cmd_resp_data = pack("C", 1);
                $source_connection->end($cmd_resp_data);
            }
        }
    }

    /**
     * Sends the SET Schedule Command to the Connection
     * 
     * Sample Schedule
     * {
     *      "5" : [
     *          {
     *              "start" : "12:00",
     *              "duration" : <integer>,
     *              "when" : "Once | Monday | Tuesday | ..."
     *          }
     *      ], ...
     * }
     */
    protected function sendScheduleToDevice($connection, $devSchedule) {
        // Change to array
        $devSchedule = json_decode(json_encode($devSchedule), True);
        // Find total number of schedules to be sent
        $totalScheduleEntries = 0;
        $validScheduleEntries = [];
        // We do a validation of actual schedules to be sent, since JSON may be malformed too
        if(!empty($devSchedule) && is_array($devSchedule)) {
            foreach($devSchedule as $pinNum => $pinSchedules) {
                foreach($pinSchedules as $pinSchedule) {
                    if(
                        array_key_exists("start", $pinSchedule) &&
                        array_key_exists("duration", $pinSchedule) &&
                        array_key_exists("when", $pinSchedule) &&
                        is_string($pinSchedule["start"]) &&
                        is_numeric($pinSchedule["duration"]) && 
                        is_string($pinSchedule["when"]) &&
                        in_array($pinSchedule["when"], ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Once"])
                    ) {
                        $pinSchStartHM = $pinSchedule["start"];
                        $pinSchDur = intval($pinSchedule["duration"]);
                        $pinSchWeekDay = $pinSchedule["when"];
                        // Convert StartHM to start Hour, Minutes
                        $hourMinArr = explode(":", $pinSchStartHM);
                        // Validate "start" value
                        if(!empty($hourMinArr) && (sizeof($hourMinArr) >= 2)) {
                            $hour = $hourMinArr[0];
                            $min = $hourMinArr[1];
                            if(is_numeric($hour) && is_numeric($min)) {
                                $hour = intval($hour);
                                $min = intval($min);
                                // For now duration should not exceed 12 hours
                                if($hour >= 0 && $hour <= 23 && $min >= 0 && $min <= 59) {
                                    // 0-6 for Mon-Sun
                                    $weekDayCode = 0;
                                    switch($pinSchWeekDay) {
                                        case "Monday":
                                            $weekDayCode = 0;
                                            break;
                                        case "Tuesday":
                                            $weekDayCode = 1;
                                            break;
                                        case "Wednesday":
                                            $weekDayCode = 2;
                                            break;
                                        case "Thursday":
                                            $weekDayCode = 3;
                                                break;
                                        case "Friday":
                                            $weekDayCode = 4;
                                            break;
                                        case "Saturday":
                                            $weekDayCode = 5;
                                            break;
                                        case "Sunday":
                                           $weekDayCode = 6;
                                            break;
                                    }
                                    // Now convert the dates to UTC from IST, by applying a -5:30
                                    if(($hour < 5) or ($hour == 5 && $min < 30)) {
                                        // Weekday would be previous
                                        $weekDayCode -= 1;
                                        if($weekDayCode == -1) {
                                            // From 0 (Mon) to -1 means it was supposed to be 6 (Sun)
                                            $weekDayCode = 6;
                                        }
                                    }
                                    if($min >= 30) {
                                        $hour = $hour - 5;
                                        $min = $min - 30;
                                    } else {
                                        $hour = $hour - 6;
                                        $min = 30 + $min;
                                    }
                                    if($hour < 0) {
                                        $hour = $hour + 24;
                                    }

                                    // Send the packet!
                                    if($pinSchDur >= 0 && $pinSchDur <= 12*60*60) {
                                        $totalScheduleEntries += 1;
                                        $validScheduleEntries[] = [intval($pinNum), $weekDayCode, $hour, $min, $pinSchDur];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // Failsafe!
        // No point sending too many or no entries
        if($totalScheduleEntries < 0) {
            return;
        }
        // Restrict to sending just first 100 schedules in case more than 100 schedules are set
        if($totalScheduleEntries > 100) {
            $totalScheduleEntries = 100;
        }
        // Send the request to the IoT device
        // Add the total schedule entry into packet
        $commandSubType = self::CMD_SUBTYPE_SET_SCHEDULE;
        $packet_data = pack("nNnn", self::PKT_TYPE_CMD, time(), $commandSubType, $totalScheduleEntries);

        // Now, iterate over all the schedules, and sent it across wire
        $i = 0;
        foreach ($validScheduleEntries as $validScheduleEntry) {
            if($i < $totalScheduleEntries) {
                $pinNum = $validScheduleEntry[0];
                $weekDayCode = $validScheduleEntry[1];
                $hour = $validScheduleEntry[2];
                $min = $validScheduleEntry[3];
                $pinSchDur = $validScheduleEntry[4];

                $packet_data .= pack("nnnnn", $pinNum, $weekDayCode, $hour, $min, $pinSchDur);
            } else {
                break;
            }
            $i++;
        }

        $this->writePacket($connection, $packet_data);
        echo "[" . date('Y-m-d H:i:s') . "] Sending Schedule Packet: #Schedules[$totalScheduleEntries]" . PHP_EOL;
    }

    protected function writePacket($connection, $packet_data) {
        // Check if the version of the connection is > 2, in which case we'd also send the packet length
        list($mac, $version) = $this->getConnectionInfo($connection);
        if($version > 2) {
            // Also write the packet header before any thing can begin:
            $pkt_len = strlen($packet_data);
            $special_boundary = 26937;
            $header = pack("nn", $pkt_len, $special_boundary);
            $packet_data = $header . $packet_data;
        }
        $connection->write($packet_data);
    }
    
    /**
     * Sends the Command to the Device to Turn ON the device (if duration is 0, then it assumes indefinite open)
     */
    protected function sendTurnOnNow($connection, $device_number, $duration) {
        if($duration == 0) {
            // Send the request to the IoT device
            $commandSubType = self::CMD_SUBTYPE_ON_INDEFINITE;
            $packet_data = pack("nNnn", self::PKT_TYPE_CMD, time(), $commandSubType, $device_number);
            $this->writePacket($connection, $packet_data);
            echo "[" . date('Y-m-d H:i:s') . "] Sending Turn ON INDEFINITE Packet: [$device_number]" . PHP_EOL;
        } else {
            // Send the request to the IoT device
            $commandSubType = self::CMD_SUBTYPE_ON_PERIODIC;
            // Time period is in seconds
            $packet_data = pack("nNnnn", self::PKT_TYPE_CMD, time(), $commandSubType, $device_number, $duration);
            $this->writePacket($connection, $packet_data);
            echo "[" . date('Y-m-d H:i:s') . "] Sending Turn ON Packet: [$device_number] for Duration [$duration]" . PHP_EOL;
        }
    }

    /**
     * @param mixed $payload
     * @param ConnectionInterface $source_connection
     * @throws Exception
     */
    private function processCommandPacket($payload, $source_connection) {
        $success = true;
        try {
            // First check if the connection is made locally, and not remotely.
            // Sending Command Packet to Server is only allowed from localhost.
            $address = $source_connection->getRemoteAddress();
            $localAddr = false;
            if (!empty($address)) {
                $ip = trim(parse_url($address, PHP_URL_HOST));
                if ($ip == '127.0.0.1') {
                    $localAddr = true;
                }
            }
            if (!$localAddr) {
                $success = false;
                return 1;
            }

            // Came here means the the connection has been made locally
            // Find out the params now
            // The packet received is of the type:
            // MacAddress (6 bytes) + Cmd SubType (2 bytes) + Num Args (2 bytes) + ArgType-A (1 byte, 0-byte, 1-short, 2-long 3-string) + ArgLen-A (2 bytes) + Argument-A (arg lenA bytes)+ ...

            // Get the first 6 bytes of mac
            $mac_bytes = $this->getNBytes($payload, 6);
            // command subtype
            $cmd_st_bytes = $this->getNBytes($payload, 2);
            // Num Argument
            $num_args_bytes = $this->getNBytes($payload, 2);

            // Find out the MAC from bytes
            $mac = bin2hex($mac_bytes);
            // n        unsigned short (always 16 bit, big endian byte order)
            $commandSubType = $this->unpack_bytes($cmd_st_bytes, 'n');
            $numArgs = $this->unpack_bytes($num_args_bytes, 'n');
            // Now iterate over arguments and find out the packet
            $commandArgs = [];
            for($i=0; $i<$numArgs; $i++) {
                $arg_type_bytes = $this->getNBytes($payload, 1);
                $argType = $this->unpack_bytes($arg_type_bytes, 'C');
                $arg_len_bytes = $this->getNBytes($payload, 2);
                $argLen = $this->unpack_bytes($arg_len_bytes, 'n');
                $arg_bytes = $this->getNBytes($payload, $argLen);
                switch ($argType) {
                    case 0:
                        $arg = $this->unpack_bytes($arg_bytes, 'C');
                        break;
                    case 1:
                        $arg = $this->unpack_bytes($arg_bytes, 'n');
                        break;
                    case 2:
                        $arg = $this->unpack_bytes($arg_bytes, 'N');
                        break;
                    case 3:
                        $arg = $this->unpack_bytes($arg_bytes, 'C');
                        break;
                    case 10:
                        $arg = $arg_bytes;
                        break;
                    default:
                        $arg = NULL;
                }
                // Add the argument:
                $commandArgs[] = $arg;
            }

            // Find the client connection if exists
            $connection = $this->getConnectionForMac($mac);
            if (!empty($connection)) {
                // Send the command now
                switch ($commandSubType) {
                    case self::CMD_SUBTYPE_ON_INDEFINITE:
                        if (sizeof($commandArgs) == 1) {
                            $device_number = $commandArgs[0];
                            // Send the request to the IoT device
                            $this->sendTurnOnNow($connection, $device_number, 0 /* Duration 0 means indefinitely */);
                            // Now the job is done
                            $success = true;
                        } else {
                            $success = false;
                        }
                        break;
                    case self::CMD_SUBTYPE_OFF_INDEFINITE:
                        if (sizeof($commandArgs) == 1) {
                            $device_number = $commandArgs[0];
                            // Send the request to the IoT device
                            $packet_data = pack("nNnn", self::PKT_TYPE_CMD, time(), $commandSubType, $device_number);
                            $this->writePacket($connection, $packet_data);
                            // Now the job is done
                            $success = true;
                        } else {
                            $success = false;
                        }
                        break;
                    case self::CMD_SUBTYPE_ON_PERIODIC:
                        if (sizeof($commandArgs) == 2) {
                            $device_number = $commandArgs[0];
                            $time_period = $commandArgs[1];
                            // Send the request to the IoT device
                            $this->sendTurnOnNow($connection, $device_number, $time_period);
                            // Now the job is done
                            $success = true;
                        } else {
                            $success = false;
                        }
                        break;
                    case self::CMD_SUBTYPE_ON_LATER_PERIODIC:
                        if (sizeof($commandArgs) == 3) {
                            // Device number to send the command to
                            $device_number = $commandArgs[0];
                            // When to set the start time of the ON
                            $start_time = $commandArgs[1];
                            // Post these many seconds, turn OFF
                            $time_period = $commandArgs[2];
                            // Send the request to the IoT device
                            // Time period is in seconds
                            $packet_data = pack("nNnnNn", self::PKT_TYPE_CMD, time(), $commandSubType, $device_number, $start_time, $time_period);
                            $this->writePacket($connection, $packet_data);
                            // Now the job is done
                            $success = true;
                        } else {
                            $success = false;
                        }
                        break;

                    default:
                        // Incorrect SubType
                        $success = false;
                }
            } else {
                $success = false;
            }
        } catch(Exception $exception) {
            $success = false;
            throw $exception;
        } finally {
            if(!$success) {
                // Failure - return 0 in response
                $cmd_resp_data = pack("C", 0);
                $source_connection->end($cmd_resp_data);
            } else {
                $cmd_resp_data = pack("C", 1);
                $source_connection->end($cmd_resp_data);
            }
        }
    }

    /**
     * @param mixed $payload
     * @param ConnectionInterface $except
     * @throws Exception
     */
    private function processStatusPacket($payload, $connection) {
        // Check if the connection is valid
        $device_status_arr = [];
        // Only if connection is valid, should processing be done
        if(!empty($this->getConnectionData($connection))) {
            $macAddr = $this->getConnectionData($connection);
            $ssid = $this->getSSIDForMac($macAddr);
            // Num Devices (1 bytes) + Device A offset (1 byte) + Device A Status (1 byte) + Device A Status Extra Info (4 bytes) + + Start Time (4 bytes) + Stop Time (4 bytes)...
            $num_devices_bytes = $this->getNBytes($payload, 1);
            $numDevices = $this->unpack_bytes($num_devices_bytes, 'C');
            for ($i = 0; $i < $numDevices; $i++) {
                $device_offset_bytes = $this->getNBytes($payload, 1);
                $device_offset = $this->unpack_bytes($device_offset_bytes, 'C');
                $device_status_bytes = $this->getNBytes($payload, 1);
                $device_status = $this->unpack_bytes($device_status_bytes, 'C');
                $extra_device_status_bytes = $this->getNBytes($payload, 4);

                // Now get the Start and End Time
                $starttime_bytes = $this->getNBytes($payload, 4);
                $device_starttime = $this->unpack_bytes($starttime_bytes, 'N');
                $stoptime_bytes = $this->getNBytes($payload, 4);
                $device_stoptime = $this->unpack_bytes($stoptime_bytes, 'N');

                $device_status_arr[$device_offset] = $device_status;
            }
            // Example received Status Packet is {"0":0,"2":0} OR {"0":1,"2":0}

            try {
                // return $device_status_arr;
                $dbConn = getDBHandleForCredentials(HOST, USER, PASSWORD, "jf", True);
                // Check the entry in DB for the DevID + AccessKey
                $qryFindDevice = "SELECT * from IOT_DeviceInfo where DeviceIdentifier = ?";
                $deviceDBRow = fetchSingleRowLenient($dbConn, $qryFindDevice, "s", [strtoupper($macAddr)]);
                if (empty($deviceDBRow)) {
                    throw new TwigException("Please contact support, looks like the Device is not registered on the platform");
                }
                $currentDateTime = date('Y-m-d H:i:s', time());
                $devID = $deviceDBRow->ID;
                // Handle the status so that it starts to show up in human readable format
                // Get current status
                $devCurrStatus = $deviceDBRow->LastKnownStatus;
                $devCurrStatusObj = json_decode($devCurrStatus);
                if (json_last_error() == JSON_ERROR_NONE) {
                        // Is valid json
                } else {
                        $devCurrStatusObj = new stdClass();
                }
                $statusStr = 'UNKNOWN';
                foreach ($device_status_arr as $device_offset => $device_status) {
                    if ($device_offset == "5" || $device_offset == "4" || $device_offset == "12" || $device_offset == "14") {
                        $statusStr = ($device_status == 0) ? "OFF" : "ON";
                        $devCurrStatusObj->$device_offset = $statusStr;
                    }
                }

                $statusStr = json_encode($devCurrStatusObj);

                $setDeviceStatusQry = "UPDATE IOT_DeviceInfo set LastKnownStatus = ?, LastKnownStatusRecvTime = ?, SSID = ? WHERE ID = ?";
                updateRow($dbConn, $setDeviceStatusQry, "sssi", [$statusStr, $currentDateTime, $ssid, $devID]);
            } finally {
                $dbConn->close();
            }
            echo "[" . date('Y-m-d H:i:s') . "] Received Status Packet: " . json_encode($device_status_arr) . PHP_EOL;
            // Now send the Status ACK packet:
            // Pkt Type (2 bytes) + TimeStamp (4 bytes)
            $response = pack("nN", self::PKT_TYPE_STATUS_ACK, time());
            $this->writePacket($connection, $response);
        } else {
            $this->closeConnection($connection);
            echo "[" . date('Y-m-d H:i:s') . "] Closing connection for Exception in Status Handler" . PHP_EOL;
            throw new Exception("Status Packet received for a non Connected Device!");
        }
    }

    // Process the packet

    /**
     * @param $data
     * @param ConnectionInterface $connection
     */
    protected function processDataPacket($data, $connection) {
        try {
            echo "[" . date('Y-m-d H:i:s') . "] Got Packet! " . $data . PHP_EOL;
            if (strlen($data) == 0 || $data == "") {
                echo "[" . date('Y-m-d H:i:s') . "] Got Packet of zero Size!! Closing connection immediately" . PHP_EOL;
                $this->closeConnection($connection);
            }
            if (strlen($data) < 3) {
                throw new Exception("Got packet less than min allowed size");
            } else {
                // Parse the packet first
                $version_bytes = $this->getNBytes($data, 1);
                $version = $this->unpack_bytes($version_bytes, 'C');
                $pkt_type_bytes = $this->getNBytes($data, 2);
                $packetType = $this->unpack_bytes($pkt_type_bytes, 'n');
                echo "[" . date('Y-m-d H:i:s') . "] Version[$version] Packet Type[$packetType]" . PHP_EOL;
                if ($version == 1 || $version == 2 || $version == 3) {
                    if ($packetType == self::PKT_TYPE_KEEPALIVE) {
                        // Keepalive
                        // version (1 byte) + Packet Type(2 bytes) + MAC (6 bytes) + TimeStamp (4 bytes) + MD5-Hash of Secret.TimeStamp (128 bytes)
                        $this->processKeepAlivePacket($version, $data, $connection);
                    } else if ($packetType == self::PKT_TYPE_STATUS) {
                        // version (1 byte) + Packet Type(2 bytes) + ??
                        $this->processStatusPacket($data, $connection);
                    } else if ($packetType == self::PKT_TYPE_CMD) {
                        // This packet should come from the Server itself, and is intended to be sent to IoT device
                        // version (1 byte) + Packet Type(2 bytes) + MacAddress (6 bytes) + Cmd SubType (2 bytes) + Num Args (2 bytes)
                        // + ArgType-A (1 byte, 0-byte, 1-short, 2-long 3-string) + ArgLen-A (2 bytes) + Argument-A (arg lenA bytes)+ ...
                        $this->processCommandPacket($data, $connection);
                    } else if ($packetType == self::PKT_TYPE_UPDATE_SCHEDULE) {
                        // This means that the Schedule for the Device has been updated, 
                        // and we should update the Device's schedule state also to reflect the same
                        $this->processScheduleUpdatedPacket($data, $connection);
                    } else if ($packetType == self::PKT_TYPE_UPDATE_SCHEDULE_ACK) {
                        // This means that the Schedule set has been synced on Device
                        $this->processScheduleACKPacket($data, $connection);
                    }
                }
            }
        } catch (Exception $ex) {
            echo "[" . date('Y-m-d H:i:s') . "] Got Exception : " . $ex->getMessage()  . PHP_EOL;
            // continue?
        }
    }

}

$tz = "UTC";
date_default_timezone_set($tz);

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server('0.0.0.0:8000', $loop);
$iotServer = new IoTServer();

$socket->on('connection', function(ConnectionInterface $connection) use ($iotServer){
    $iotServer->add($connection);
});

$loop->addPeriodicTimer(30, function() use ($loop, $iotServer) {
    $iotServer->periodicHandler();
});


echo "[" . date('Y-m-d H:i:s') . "] Listening on {$socket->getAddress()}" . PHP_EOL;

try {
    $loop->run();
} catch (Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
} finally {
    $socket->close();
    echo "[" . date('Y-m-d H:i:s') . "] Closing Server!" . PHP_EOL;
}
