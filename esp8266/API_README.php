========================
========================
LOGIN related APIs
========================
========================

---------------------
A) Generate OTP for user registration
---------------------

Method : POST 
URI : /iot/generateotp

Body:
	username	:	Email ID (Only email ID allowed)

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
	}

---------------------
B) Register a User :
---------------------
Method : POST
URI : /iot/user/register

Body :
	username	:	Email ID
	name 		:	User Name
	password	:	OTP
	pwd 		:	Password

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
		"data" 	:	{
			"authorization"	:	"JWT Token",
			"username"		:	"User Name to show in menu"
		}
	}


---------------------
C) Login :
---------------------
Method : POST
URI : /iot/user/login

Body :
	username	:	Email ID
	pwd 		:	Password

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
		"data" 	:	{
			"authorization"	:	"JWT Token",
			"username"		:	"User Name to show in menu"
		}
	}


---------------------
D) Reset Password :
---------------------
Method : POST
URI : /iot/user/resetpwd

Body :
	username	:	Email ID
	password	:	OTP
	pwd 		:	Password

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
		"data" 	:	{
			"authorization"	:	"JWT Token",
			"username"		:	"User Name to show in menu"
		}
	}



========================
========================
Device Related APIs
========================
========================

---------------------
A) Register Device
---------------------

Method : POST 
URI : /iot/device/:DevIdentifier/link

URI param:
	DeviceIdentifier	:	The 12 character MACAddress of the device 

Body : 
	AccessKey		:	"A random secret string which is unique to a device"
	SSID			:	"Home SSID to which device is configured to connect to"

Header : 
	authorization	:	JWT Token returned in login/reset/register API

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
	}


-----------------------
B) Get all the devices
-----------------------

Method:  GET
URI : /iot/devices

Header : 
	authorization	:	JWT Token returned in login/reset/register API

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
		"data"	:	{
			"devices"	:	[
				{
					"id"			:	"DB ID",
                    "identifier"	:	"Macaddress",
                    "title" 		:	"Device Title",
                    "numoutputs" 	:	Integer <Number of outputs for device>,
					"pins"			:	Array of pin numbers to use in which order [5,4,12,14]
					"status"		:	Array of [ Device Index => "String of device status, when it last connected", ... ],
					"schedule"		:	Array of [ Device Index => {Schedule info as passed in /iot/schedule API}, ... ],
                    "statusrecvtime":	"Date Time last device was connected",
                    "type" 			:	"WATER_TIMER|SWITCH",
                    "model"			:	"WT-01|WT-02|etc"
				}, ...
			]
		}
	}



---------------------
C) To Turn ON an IoT device
---------------------

Method : GET 
URI : /iot/turnon/:Macaddress(/:DeviceNum(/:howLong))

URI Param
	Macaddress 	:	The MAC of the device
	DeviceNum	: 	The index of the device
	howLong		:	Seconds for which device is to be turned ON. If not specified, then Forever ON

Header : 
	authorization	:	JWT Token returned in login/reset/register API

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
	}


---------------------
D) To Turn OFF an IoT device
---------------------

Method:  GET
URI : /iot/turnoff/:MacAddress(/:DeviceNum)


URI Param
	Macaddress 	:	The MAC of the device
	DeviceNum	: 	The index of the device

Header : 
	authorization	:	JWT Token returned in login/reset/register API

Response : 
	{
		"error" 		:	true/false
		"message"		:	"Some string to show in popup"
		"showmessage"	:	true/false	(Optional)
	}

----------------------------
(E) To schedule the device
----------------------------
Method : PUT 
URI : /iot/schedule/:Macaddress/devicenum/:DeviceNum



URI Param
	Macaddress 	:	The MAC of the device
	DeviceNum	: 	The index of the device

Header : 
	authorization	:	JWT Token returned in login/reset/register API


Body : 
     * {
     *      "weekly" : {
     *          "Monday" : [
     *              {
     *                  "start" : "23:00:00",  // Assumed to be in IST
     *                  "duration" : <int> // In seconds 
     *              }, ...
     *          ],
     *          "Tuesday" : [
     *              {
     *                  "start" : "12:00:00",  // Assumed to be in IST
     *                  "duration" : <int> // In seconds 
     *              }, ...
     *          ]
     *      },
     *      "once" : [
     *          {
     *              "start" : "2019-12-21 12:00:00",  // Assumed to be in IST
     *              "duration" : <int> // In seconds 
     *          }, ...
     *      ]
     * }