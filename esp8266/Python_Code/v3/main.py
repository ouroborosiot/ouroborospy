import pushkar
import machine
import utime
import network
import os

def init_clean():
	pushkar.init_pin(0)
	pushkar.init_pin(2)
	pushkar.init_pin(4)
	pushkar.init_pin(5)
	pushkar.init_pin(12)
	pushkar.init_pin(14)
	# Turn WiFi OFF
	pushkar.turn_wifi_off()
	# Initialize Schedule
	pushkar.init_setup()

def reset_button_callback(p):
	print('Inside IRQ')
	# Disable IRQ on Pin 13 temporarily
	pin13 = machine.Pin(13, machine.Pin.IN)
	pin13.irq(trigger = 0, handler = reset_button_callback)	
	time_to_low = 5000 #ms
	pin_low = True
	while (time_to_low > 0):
		utime.sleep_ms(250)
		time_to_low = time_to_low - 250
		if pin13.value() == 1:
			pin_low = False
			break

	# Check if the value is still LOW for the Pin, and was low all the time
	if (pin13.value() == 0) and (pin_low == True) :
		print('Resetting the Device!')
		init_clean()
		# Remove the wifi.conf
		fileName = 'wifi.conf'
		if pushkar.fileExists(fileName):
			os.remove(fileName)
		# Remove the schedule.conf
		fileName = 'schedule.conf'
		if pushkar.fileExists(fileName):
			os.remove(fileName)
		# Sleep, reset
		utime.sleep_ms(1000)
		# Stop cl if
		sta_if = network.WLAN(network.STA_IF)
		sta_if.active(False)
		utime.sleep_ms(500)
		utime.sleep_ms(500)
		machine.reset()
	else:
		# Restore the IRQ
		print('Not Resetting the Device, could be momentary blip?')
		pin13.irq(trigger = machine.Pin.IRQ_FALLING, handler = reset_button_callback)

# Give a small delay
print('Starting the MAIN Process')
utime.sleep_ms(1000)
# Initialize all the pins
init_clean()
# IRQ for GPIO 13 - Reset Button
pin13 = machine.Pin(13, machine.Pin.IN)
pin13.irq(trigger = machine.Pin.IRQ_FALLING, handler = reset_button_callback)

sta_if = network.WLAN(network.STA_IF)
sta_if.active(False)
utime.sleep_ms(500)
utime.sleep_ms(500)

# Check if the home wifi network has been setup
# Check if home wifi config is valid, if so, connect to it
# If home wifi is not configured, then use the Web server all the time. 
try:
	if pushkar.get_wifi_config() is None:
		# Came here means the wifi is not configured
		# Start the web server
		print("Starting web server")
		pushkar.start_web_server()
	else:
		# In such case, we should still run the web server for first 120 seconds!
		## No more needed!
		# pushkar.start_web_server(30)
		# Now the station interface
		pushkar.do_connect()
finally:
	# If somehow came here means machine should go for reset
	try:
		init_clean()
	finally:
		machine.reset()
# If comes here by mistake
machine.reset()
