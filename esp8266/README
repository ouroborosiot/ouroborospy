=========================
References:
=========================
https://askubuntu.com/questions/805262/cannot-send-at-commands-in-minicom

=========================
Commands: 
=========================

sudo minicom -D /dev/ttyUSB0 -b 115200

Exactly as mentioned here: 

https://www.iot-experiments.com/flashing-esp8266-esp01/

-------------------
ESP 8266 - 01
-------------------
UARD RX to ESP TX
UART TX to ESP RX
UART GND to PB GND
ESP VCC to PB 3.3
ESP GND to PB GND
ESP GPIO 0 to GND
ESP CHPD to PB 3.3

No need to do anything to RST or GPIO 2

-------------
12E
------------
UARD RX to ESP TX
UART TX to ESP RX
UART GND to PB GND
ESP VCC to PB 3.3
ESP GND to PB GND
ESP GPIO 0 to GND
ESP GPIO 2 to GND
ESP CHPD to PB 3.3

Putting above for 12E together in terms of the wires:

PB                PCB                   UART                   ESP

FEMALE (GND)      MALE (GND)
FEMALE (3.3)      MALE (3.3)

--                --                    FEMALE (RX)            FEMALE (TX)
--                --                    FEMALE (TX)            FEMALE (RX)

--                MALE (GND)            FEMALE (GND)           --
--                MALE (3.3)            --                     FEMALE (Vcc)
--                MALE (GND)            --                     FEMALE (GND)
--                MALE (GND)            --                     FEMALE (GPIO 0)
--                MALE (GND)            --                     FEMALE (GPIO 2)
--                MALE (3.3)            --                     FEMALE (CHPD)


Sometimes putting GPIO0 to GND directly caused voltage fluctuation issue in GPIO 0. Putting a resistor (220) between GPIO 0 and GND worked.

Packages Installaton:
-------------
sudo apt-get install python-pip
sudo apt-get install screen
pip install esptool
pip install adafruit-ampy
-------------

Steps to Flash
-------------
sudo esptool.py --port /dev/ttyUSB0 erase_flash
sudo esptool.py --port /dev/ttyUSB0 --baud 115200 write_flash --verify --flash_size=detect -fm dout 0 ~/mp/micropython/ports/esp8266/build/firmware-combined.bin
sudo screen /dev/ttyUSB0 115200

https://cdn-learn.adafruit.com/downloads/pdf/micropython-basics-load-files-and-run-code.pdf

Once boot loaderflashing is done, just remove the GPIO 0 from Ground, put it to HIGH and restart

=========================
To run code:
=========================
Say a sample test.py file

pushkar@pushkar-X553MA:~$ cat test.py 
print('Hello world! I can count to 10:')
for i in range(1,11):
	print(i)
pushkar@pushkar-X553MA:~$

=========================
To run the above test.py:
=========================
sudo ampy --port /dev/ttyUSB0 run test.py
OR run -n test.py

=========================
To copy file from local 
computer to esp
=========================
For example to copy a test.py
from your computer to a file /foo/bar.py on the board run (note the parent foo directory
must already exist!):

ampy --port /dev/ttyUSB0 put test.py /foo/bar.py

To copy temp.py as main.py  on esp

sudo ampy --port /dev/ttyUSB0 put ~/Ouroboros/iot/esp8266/Python\ Code/v2/main.py /main.py
sudo ampy --port /dev/ttyUSB0 put ~/Ouroboros/iot/esp8266/alpha.txt /alpha.txt



To delete, use rm command

======================
======================
Cross Compile
(Done inside /home/pushkar/mp/)
======================
======================
In order to cross compile for the ESP8266, you will need to install the ESP8266 toolchain (i.e. compiler that comes with all required libraries to generate a binary file for the ESP). Yet this toolchain needs to be built locally, since a pre-build ESP-compiler does not (yet) exist. First, add all packages to your Linux distribution so it can compile the toolchain. The to be installed packages are listed on the official ESP SDK repository (here), but I’ll include them here, too:

>> sudo apt-get install make unrar-free autoconf automake libtool gcc g++ gperf \
flex bison texinfo gawk ncurses-dev libexpat-dev python-dev python python-serial \
sed git unzip bash help2man wget bzip2

For later Linux distributions you may need this too:

>> sudo apt-get install libtool-bin


Next, download the ESP toolchain code from its Git repository using:

>> git clone --recursive https://github.com/pfalcon/esp-open-sdk.git

Change director into this repository and compile the code using make:

>> cd esp-open-sdk

// Check out a code for the commit ID : 11ca71d47d30fc4c02d90e9e805f543a3147b2d3
// https://github.com/pfalcon/esp-open-sdk/commit/11ca71d47d30fc4c02d90e9e805f543a3147b2d3#diff-b67911656ef5d18c4ae36cb6741b7965
// This version of esp-open-sdk is 2.0.0. It is compatible with the micropython v1.9.3 tag 
// which we would be taking out
>> git checkout 11ca71d47d30fc4c02d90e9e805f543a3147b2d3

Confirm that the checkout is correct by viewing Makefile, check if following line is there:

VENDOR_SDK = 2.0.0

>> make

-------- NOTICED AN ISSUE IN above make in recent testing (BEGIN) ----------
// May not be there in checkout for commit id mentioned above
https://github.com/pfalcon/esp-open-sdk/issues/248

A way to fix this issue is this pull request: jcmvbkbc/crosstool-NG#53
This also fixes #307

It boils down to update the URL template in crosstool-NG/scripts/build/companion_libs/210-expat.sh from
http://downloads.sourceforge.net/project/expat/expat/${CT_EXPAT_VERSION}
to
https://github.com/libexpat/libexpat/releases/download/R_${CT_EXPAT_VERSION//[.]/_}

-------- END ----------------------------------------------------------------


Once the compilation is done, be sure to export the toolchain’s binary folder to the system path (i.e. add it to the $PATH variable). Conveniently, once the compilation finishes, your terminal should show you the code to enter. I’ve appended mine to give you an indication what it should look like:

>> export PATH=/home/pushkar/mp/esp-open-sdk/xtensa-lx106-elf/bin:$PATH


Now the toolchain is successfully installed, step our of the current directory (in my case, back to ~/Documents) and download the MicroPython repository:

>> git clone https://github.com/micropython/micropython.git


>> cd micropython

Check out the v1.9.3 tag
// https://coderwall.com/p/k2fisg/git-clone-a-repository-and-checkout-a-specified-tag

>> git fetch && git fetch --tags
>> git checkout v1.9.3

Add all external dependencies (i.e. those that still need to be downloaded):

>> git submodule update --init



Then pre-compile the ESP cross-compiler:

>> make -C mpy-cross    (From micropython folder)

And finally build MicroPython for the ESP8266:

cd ports/esp8266

Now the following step is to copy your actual python main module to the modules folder
Copy the file pushkar.py if needed as follows

>> cp ~/Ouroboros/iot/esp8266/Python_Code/v2/pushkar_Aug11.py modules/pushkar.py

Also, we need to put urequests.py file into modules folder 
 https://github.com/micropython/micropython/issues/3389
 Daily builds don't include some libraries which are part of release builds. You need to install urequests from micropython-lib.
 https://github.com/micropython/micropython-lib

>> cp ~/Ouroboros/iot/esp8266/urequests.py modules/urequests.py

>> make clean && make axtls && make

## make axtls is NOT NEEDED if we took the checkout of master, but for v1.9.3, we need axtls
-------- NOTICED an issue recently (for master checkout, not valid for 1.9.3) ----------------------------------
make axtls does not find the target if master for micropyton is checked out.
But thats how the latest documentation for esp8266 on github of micropython works (make clean && make)
-------- END --------------------------------------------------------

This will produce all binary images for the ESP in the build/ subdirectory. Since the esptool.py was installed with the esp-open-sdk, you can simply use the following command to erase the ESP8266’s ROM and flash the new MicroPython image to it:

The binary is in build/firmware-combined.bin. A latest copy is kept in Downloads (see hamburger menu).

esptool.py --port /dev/ttyUSB0 erase_flash
make PORT=/dev/ttyUSB0 deploy
Be sure to change /dev/ttyUSB0 to whatever your machine recognises the EPS as. And you’re done!




==============================
To execute the server 
==============================

sudo nohup php IoTServer.php > /tmp/iot.log 2>&1 &

==============================
To generate random bytes
==============================

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
// Echo the random string.
// Optionally, you can give it a desired string length.
echo generateRandomString(64);

===================================
TO find the WiFi SSID
===================================
import network
import ubinascii
cl_if = network.WLAN(network.STA_IF)
macaddress = ubinascii.hexlify(cl_if.config('mac'),':').decode()

===================================
IF NOTHING WORKS to copy main.py / alpha.txt
===================================
Use Python's raw strings:

ctrl-E
code=r'''(paste)'''
ctrl-D

then save that variable into the target file ( with open('target.py','w') as f: f.write(code) )

It'll transfer everything except nested triple-quoted strings, without additional backslash-escaping.

with open('main.py','w') as f: f.write(code)


=====================================
TODOs
=====================================
on closeConnection(..) in IoTServer, the server should set the device in DB as disconnected
Circuit Diagram should be put in repo, along with Cap component values